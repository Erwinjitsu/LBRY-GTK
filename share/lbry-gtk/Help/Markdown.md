This page is for documenting LBRY-GTKs markdown rendering capabilities.<br>

###### Titles:

# Title1
## Title2
### Title3
#### Title4
##### Title5
###### Title6

###### Different titles:

Title1
=

Title2
-

###### Unordered lists:

   - A
   - B
   - C

###### Ordered lists:

   3. A
   2. B
   1. C

###### Nested Unordered lists:

   - A
   - B
      - B1
      - B2
   - C

###### Nested Ordered lists:

   3. A
   2. B
      1. B1
      2. B2
   1. C

###### Nested Mixed lists:
(4 spaces needed)


   - A
   - B
       1. B1
       2. B2
          - B21
          - B22
             1. B221
             1. B222
          - B23
       1. B3
   - C

###### Bold text:

I'm **bold**.
I'm __bold__.

###### Italic text:

I'm *italic*.<br>
I'm _italic_.

###### Bold Italic text:

I'm ***bold and italic***.
I'm ___bold and italic___.

###### Code blocks:

```
Beep-boop I am a robot,
so I talk in monospace.
```

###### Quote blocks:

>Someone said.
>>Answering this.
>>>That was an answer to this.
>>
>>Then the previous continued.
>
>And then the first one.

###### Images:

Local image:

![](./Logo.png)

Online image:

![](https://codeberg.org/repo-avatars/30238-d7f099cee5b3b6d61602e72024a699a8)

###### Links:

With placeholder text:
[I am a link](https://codeberg.org/MorsMortium/LBRY-GTK)

Without placeholder text:
[](https://codeberg.org/MorsMortium/LBRY-GTK)

Image links:
[![](https://codeberg.org/repo-avatars/30238-d7f099cee5b3b6d61602e72024a699a8)](https://codeberg.org/MorsMortium/LBRY-GTK)

###### Source:

```
This page is for documenting LBRY-GTKs markdown rendering capabilities.<br>

###### Titles:

# Title1
## Title2
### Title3
#### Title4
##### Title5
###### Title6

###### Different titles:

Title1
=

Title2
-

###### Unordered lists:

   - A
   - B
   - C

###### Ordered lists:

   3. A
   2. B
   1. C

###### Nested Unordered lists:

   - A
   - B
      - B1
      - B2
   - C

###### Nested Ordered lists:

   3. A
   2. B
      1. B1
      2. B2
   1. C

###### Nested Mixed lists:
(4 spaces needed)


   - A
   - B
       1. B1
       2. B2
          - B21
          - B22
             1. B221
             1. B222
          - B23
       1. B3
   - C

###### Bold text:

I'm **bold**.
I'm __bold__.

###### Italic text:

I'm *italic*.<br>
I'm _italic_.

###### Bold Italic text:

I'm ***bold and italic***.
I'm ___bold and italic___.

###### Code blocks:

\```
Beep-boop I am a robot,
so I talk in monospace.
\```

###### Quote blocks:

>Someone said.
>>Answering this.
>>>That was an answer to this.
>>
>>Then the previous continued.
>
>And then the first one.

###### Images:

Local image:

![](./Logo.png)

Online image:

![](https://codeberg.org/repo-avatars/30238-d7f099cee5b3b6d61602e72024a699a8)

###### Links:

With placeholder text:
[I am a link](https://codeberg.org/MorsMortium/LBRY-GTK)

Without placeholder text:
[](https://codeberg.org/MorsMortium/LBRY-GTK)

Image links:
[![](https://codeberg.org/repo-avatars/30238-d7f099cee5b3b6d61602e72024a699a8)](https://codeberg.org/MorsMortium/LBRY-GTK)
```
You can go back to the main article [here](./Index.md).
