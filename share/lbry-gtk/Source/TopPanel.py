################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2023 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import gi, threading, time, json

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk, GdkPixbuf, GLib

from Source import SelectUtil, SettingsUpdate
from Source.Select import Select
from Source.PyGObjectCAPI import PyGObjectCAPI
from CRewrite import Global, Icons, Places, Popup, Preferences, Sync
from CRewrite import Url

CAPI = PyGObjectCAPI()


class TopPanel:
    def __init__(self, BackImage, NewPage, SamePage, SetMenu):
        Builder = CAPI.ToObject(Global.Builder)
        Builder.add_from_file(Places.GladeDir + "TopPanel.glade")
        Builder.connect_signals(self)
        self.SamePage = SamePage
        self.SetMenu, self.InMenu = SetMenu, False
        self.BackImage, self.NewPage = BackImage, NewPage
        self.TopPanel = Builder.get_object("TopPanel")
        self.LBRY = Builder.get_object("LBRY")
        self.Search = Builder.get_object("Search")
        self.Inbox = Builder.get_object("Inbox")
        self.New = Builder.get_object("New")
        self.NewPublication = Builder.get_object("NewPublication")
        self.NewChannel = Builder.get_object("NewChannel")
        self.Star = Builder.get_object("Star")
        self.Settings = Builder.get_object("Settings")
        self.Help = Builder.get_object("Help")
        self.Status = Builder.get_object("Status")
        self.About = Builder.get_object("About")
        self.Balance = Builder.get_object("Balance")
        self.Account = Builder.get_object("Account")
        self.CreatorAnalytics = Builder.get_object("CreatorAnalytics")
        self.Rewards = Builder.get_object("Rewards")
        self.Invites = Builder.get_object("Invites")
        self.Signing = Builder.get_object("Signing")
        self.ShowMenu = Builder.get_object("ShowMenu")
        self.MenuBox = Builder.get_object("MenuBox")
        self.MenuBar = Builder.get_object("MenuBar")
        self.Menu = Builder.get_object("Menu")
        self.NewPublicationOnCurrent = Builder.get_object(
            "NewPublicationOnCurrent"
        )
        self.NewPublicationOnNew = Builder.get_object("NewPublicationOnNew")
        self.SettingsOnCurrent = Builder.get_object("SettingsOnCurrent")
        self.SettingsOnNew = Builder.get_object("SettingsOnNew")
        self.HelpOnCurrent = Builder.get_object("HelpOnCurrent")
        self.HelpOnNew = Builder.get_object("HelpOnNew")
        self.StatusOnCurrent = Builder.get_object("StatusOnCurrent")
        self.StatusOnNew = Builder.get_object("StatusOnNew")
        self.AboutAction = Builder.get_object("AboutAction")
        self.BalanceOnCurrent = Builder.get_object("BalanceOnCurrent")
        self.BalanceOnNew = Builder.get_object("BalanceOnNew")
        self.TopMenus = [self.New, self.Star, self.Account]
        self.MenuEnds = [self.NewChannel, self.About, self.Signing]
        Widgets = [self.New, self.NewPublication, self.NewChannel, self.Star]
        Widgets.extend([self.Settings, self.Help, self.Status, self.About])
        Widgets.extend([self.Balance, self.Account, self.CreatorAnalytics])
        Widgets.extend([self.Rewards, self.Invites, self.Signing])
        Exceptions, Enters, Leaves, Activates = [], [], [], []
        for Widget in Widgets:
            Exceptions.append(Widget)
            if Widget in self.TopMenus:
                Lambda = SelectUtil.FunctionsLambda(Widget.select, self.SetMenu)
                Enters.append(Lambda)
            else:
                Enters.append(Widget.select)
            if Widget in self.TopMenus:
                Leaves.append(
                    SelectUtil.FunctionsLambda(self.MenuLeave, self.SetMenu)
                )
            elif Widget in self.MenuEnds:
                Lambda = SelectUtil.FunctionsLambda(Widget.deselect, self.Leave)
                Leaves.append(Lambda)
            elif Widget == self.Balance:
                Lambda = SelectUtil.FunctionsLambda(
                    Widget.deselect, self.MenuLeave, self.SetMenu
                )
                Leaves.append(Lambda)
            else:
                Leaves.append(Widget.deselect)
            Lambda = SelectUtil.EventLambda(Widget, self.RestartSelector)
            Activates.append(Lambda)
        self.Selector = Select(
            Widgets, Exceptions, Enters, Leaves, Activates, True
        )
        Height = self.BackImage.get_preferred_height().minimum_height

        ScaledLogo = CAPI.ToObject(Icons.Get()["LogoBig"]).scale_simple(
            Height, Height, GdkPixbuf.InterpType.BILINEAR
        )
        Gtk.IconTheme.add_builtin_icon("LBRY-GTK", -1, ScaledLogo)
        self.LBRY.set_image(
            Gtk.Image.new_from_icon_name("LBRY-GTK", Gtk.IconSize.BUTTON)
        )
        CssProvider = Gtk.CssProvider.new()
        CssProvider.load_from_data(b"* {padding: 0px;}")
        StyleContext = self.Inbox.get_style_context()
        StyleContext.add_provider(
            CssProvider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
        )

    def on_MenuBar_deactivate(self, Widget):
        if isinstance(Widget.get_parent(), Gtk.Popover):
            self.Menu.set_active(False)

    def Exit(self, IfMenu=False):
        Widget = self.Selector.Widgets[self.Selector.LastItem]
        if IfMenu:
            Widget.deselect()
        else:
            self.TopMenus[self.MenuEnds.index(Widget)].deselect()

    def Leave(self):
        LastItem = self.Selector.LastItem
        CurrentItem = self.Selector.CurrentItem
        if (
            LastItem < CurrentItem
            or LastItem == len(self.Selector.Widgets) - 1
            and CurrentItem == 0
        ):
            self.Exit()

    def MenuLeave(self):
        LastItem = self.Selector.LastItem
        CurrentItem = self.Selector.CurrentItem
        if (
            CurrentItem < LastItem
            or CurrentItem == len(self.Selector.Widgets) - 1
            and LastItem == 0
        ):
            self.Exit(True)
            Widget = self.Selector.Widgets[LastItem]
            if Widget == self.Account or CurrentItem == -1:
                return
            if Widget == self.Balance:
                Index = 1
            else:
                Index = self.TopMenus.index(Widget) - 1
                if Index == -1:
                    Index = len(self.TopMenus) - 1
            self.TopMenus[Index].select()

    def RestartSelector(self):
        self.Exit(True)
        for Widget in self.Selector.Widgets:
            Widget.deselect()
        self.MenuBar.deselect()
        self.MenuBar.cancel()
        self.SetMenu(True)
        self.Selector.Unselect(True)
        if self.Menu.get_visible() and self.Menu.get_active():
            self.Menu.set_active(False)

    def Select(self):
        self.MenuBar.deselect()
        if not self.Menu.get_visible():
            self.RestartSelector()
            self.MenuBar.select_first(False)
        self.SetMenu()
        self.InMenu = True

    def on_MenuBar_focus_out_event(self, Widget, Event):
        self.ShowMenu.activate()

    def on_ShowMenu_activate(self, Widget):
        if self.InMenu:
            self.RestartSelector()
            self.InMenu = False
        else:
            if not self.Menu.get_visible():
                self.Select()
            else:
                self.Menu.activate()
                threading.Thread(None, self.ShowMenuThread).start()

    def ShowMenuThread(self):
        while not self.Menu.get_active():
            time.sleep(0.01)
        GLib.idle_add(self.Select)

    def Click(self, Widget):
        Index = int(Widget.get_label())
        self.ShowMenu.activate()
        for Widget in self.Selector.Widgets:
            Widget.deselect()
        for MenuIndex in range(len(self.TopMenus) - 1, -1, -1):
            MenuWidget = self.TopMenus[MenuIndex]
            MenuWidgetIndex = self.Selector.Widgets.index(MenuWidget)
            if MenuWidgetIndex < Index:
                self.Selector.SelectIndex(MenuWidgetIndex)
                break
        self.Selector.SelectIndex(Index)
        return Index

    def LeftClick(self, Widget):
        Index = self.Click(Widget)
        self.ClickUpdate(Index, 0)

    def MiddleClick(self, Widget):
        Index = self.Click(Widget)
        self.ClickUpdate(Index, 1)

    def ClickUpdate(self, Index, Button):
        self.Selector.Activate(Button, Index)
        self.ShowMenu.activate()

    def on_LBRY_button_press_event(self, Widget, Event):
        Widget.event(Gdk.Event.new(Gdk.EventType.BUTTON_RELEASE))

    def on_LBRY_button_release_event(self, Widget, Event):
        self.Search.grab_focus()
        self.Search.set_text("lbry://")
        self.Search.set_position(7)

    def on_Balance_button_press_event(self, Widget, Event):
        if Global.Started:
            if Event.button == Gdk.BUTTON_PRIMARY:
                self.SamePage("Wallet", [])
            elif Event.button == Gdk.BUTTON_MIDDLE:
                self.NewPage("Wallet", [])
        else:
            Popup.Message("LBRYNet is not running.")

    def on_Inbox_button_press_event(self, Widget, Event, Button=""):
        Widget.set_active(False)
        if Global.Started:
            if Event.button == Gdk.BUTTON_PRIMARY:
                self.SamePage("Inbox", [])
            elif Event.button == Gdk.BUTTON_MIDDLE:
                self.NewPage("Inbox", [])
        else:
            Popup.Message("LBRYNet is not running.")

    def on_Parent_button_press_event(self, Widget, Event=""):
        Widget.get_parent().event(Event)

    def on_Search_key_press_event(self, Widget, Event):
        Search = Widget.get_text()
        if Gdk.keyval_name(Event.keyval) == "Return" and Search != "":
            if Global.Started:
                NewTitle = "Search: " + Search
                LBRYSettings = json.loads(Preferences.Get())
                Session = LBRYSettings["Preferences"]["Session"]
                if Search.startswith("lbry://") and not isinstance(
                    json.loads(Url.Get([Search], Session["Server"]))[0], str
                ):
                    self.SamePage("Publication", ["", Search])
                else:
                    self.SamePage(
                        "Advanced Search",
                        [NewTitle, None, None, json.dumps({"text": Search})],
                    )
            else:
                Popup.Message("LBRYNet is not running.")

    def on_About_button_press_event(self, Widget, Event):
        Builder = CAPI.ToObject(Global.Builder)
        Builder.add_from_file(Places.GladeDir + "About.glade")
        AboutWindow = Builder.get_object("AboutWindow")
        LogoBig = CAPI.ToObject(Icons.Get()["LogoBig"])
        AboutWindow.set_logo(LogoBig)
        AboutWindow.set_icon(LogoBig)
        AboutWindow.run()
        AboutWindow.destroy()

    def on_Signing_button_press_event(self, Widget, Event):
        # This deals with login process
        LBRYSettings = json.loads(Preferences.Get())
        Server = LBRYSettings["Preferences"]["Session"]["Server"]
        # This gets executed if Sign Out is clicked
        if self.Signing.get_label() == "Sign Out":
            Accounts = LBRYSettings["Preferences"]["Accounts"]
            AccountRemove = Sync.Disconnect(json.dumps(Accounts), Server)
            if AccountRemove:
                Popup.Message("There was a problem Signing Out")
                return
            LBRYSettings["Preferences"]["AuthToken"] = ""
            LBRYSettings["Preferences"]["Accounts"] = []
            Update = json.loads(
                Preferences.Set(
                    Places.ConfigDir,
                    json.dumps(LBRYSettings),
                )
            )
            if "error" in Update:
                Popup.Error(Update["error"])
            else:
                Popup.Message("Sign Out was succesful")
                self.Signing.set_label("Sign In")
            return
        # This only gets executed if Sign In is clicked
        Builder = CAPI.ToObject(Global.Builder)
        Builder.add_from_file(Places.GladeDir + "SignInForm.glade")
        SignInDialog = Builder.get_object("SignInDialog")
        LogoPlate = Builder.get_object("LogoPlate")
        EmailField = Builder.get_object("EmailField")
        PasswordField = Builder.get_object("PasswordField")
        PasswordField.set_visibility(False)
        GotIcons = Icons.Get()
        Height = CAPI.ToObject(GotIcons["LBCLabel"]).get_height() * 7
        ScaledLogoSignUp = CAPI.ToObject(GotIcons["LogoBig"]).scale_simple(
            Height, Height, GdkPixbuf.InterpType.BILINEAR
        )
        LogoPlate.set_from_pixbuf(ScaledLogoSignUp)
        LogoPlate.set_padding(20, 0)
        SignInDialog.set_icon(CAPI.ToObject(GotIcons["LogoBig"]))
        Response = SignInDialog.run()
        Email = EmailField.get_text()
        Password = PasswordField.get_text()
        SignInDialog.destroy()

        # If OK is clicked we attempt to get the account and save it
        if Response == Gtk.ResponseType.OK:
            AuthData = Sync.Connect(Email, Password, Server)
            AuthData = json.loads(AuthData)
            if "error" in AuthData:
                Popup.Error(AuthData["error"])
                return
            LBRYSettings["Preferences"]["Accounts"] = AuthData["AccountIDs"]
            LBRYSettings["Preferences"]["AuthToken"] = AuthData["AuthToken"]
            Update = json.loads(
                Preferences.Set(
                    Places.ConfigDir,
                    json.dumps(LBRYSettings),
                )
            )
            if "error" in Update:
                Popup.Error(Update["error"])
            else:
                Popup.Message("Sign In was succesful")
                self.Signing.set_label("Sign Out")

    def on_NewPublication_button_press_event(self, Widget, Event, Button=""):
        if Global.Started:
            if Event.button == Gdk.BUTTON_PRIMARY:
                self.SamePage("NewPublication", [])
            elif Event.button == Gdk.BUTTON_MIDDLE:
                self.NewPage("NewPublication", [])
        else:
            Popup.Message("LBRYNet is not running.")

    def on_Settings_button_press_event(self, Widget, Event, Button=""):
        if Event.button == Gdk.BUTTON_PRIMARY:
            self.SamePage("Settings", [])
        elif Event.button == Gdk.BUTTON_MIDDLE:
            self.NewPage("Settings", [])

    def on_Help_button_press_event(self, Widget, Event, Button=""):
        if Event.button == Gdk.BUTTON_PRIMARY:
            self.SamePage("Help", [])
        elif Event.button == Gdk.BUTTON_MIDDLE:
            self.NewPage("Help", [])

    def on_Status_button_press_event(self, Widget, Event, Button=""):
        if Event.button == Gdk.BUTTON_PRIMARY:
            self.SamePage("Status", [])
        elif Event.button == Gdk.BUTTON_MIDDLE:
            self.NewPage("Status", [])

    def NoToggleBefore(self, Widget):
        Widget.set_name(str(Widget.get_active()))

    def NoToggleAfter(self, Widget):
        Widget.set_active(Widget.get_name() == "True")

    # Not Yet Implemented

    def on_NewChannel_button_press_event(self, Widget, Event):
        Popup.Message("Feature currently not implemented")

    def on_CreatorAnalytics_button_press_event(self, Widget, Event):
        Popup.Message("Feature currently not implemented")

    def on_Rewards_button_press_event(self, Widget, Event):
        Popup.Message("Feature currently not implemented")

    def on_Invites_button_press_event(self, Widget, Event):
        Popup.Message("Feature currently not implemented")
