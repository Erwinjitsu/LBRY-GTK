################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2023 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import gi, threading, json, copy

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GLib

from Source import SelectUtil
from Source.Select import Select
from Source.PyGObjectCAPI import PyGObjectCAPI
from CRewrite import Channel, Content, DateTime, Global, Identity, Icons
from CRewrite import Language, Move, Places, Popup, Preferences, Replace, Tag

CAPI = PyGObjectCAPI()
LicenseMessage = "You have to choose a license or set License Type to Custom."
NameMessage = "Name can't contain spaces or non-ASCII characters"


class NewPublication:
    def __init__(self, Title, TabPointer):
        Builder = CAPI.ToObject(Global.Builder)
        Builder.add_from_file(Places.GladeDir + "NewPublication.glade")
        Builder.connect_signals(self)
        self.TabPointer = TabPointer
        self.Title = Title
        self.NewPublication = Builder.get_object("NewPublication")
        self.ChannelsBox = Builder.get_object("ChannelsBox")
        self.Name = Builder.get_object("Name")
        self.PTitle = Builder.get_object("Title")
        self.File = Builder.get_object("File")
        self.Description = Builder.get_object("Description")
        self.ThumbnailType = Builder.get_object("ThumbnailType")
        self.LicenseType = Builder.get_object("LicenseType")
        self.ThumbnailUrl = Builder.get_object("ThumbnailUrl")
        self.ThumbnailFile = Builder.get_object("ThumbnailFile")
        self.TagBox = Builder.get_object("TagBox")
        self.Deposit = Builder.get_object("Deposit")
        self.Price = Builder.get_object("Price")
        self.LanguageBox = Builder.get_object("LanguageBox")
        self.License = Builder.get_object("License")
        self.LicenseLabel = Builder.get_object("LicenseLabel")
        self.LicenseModel = Builder.get_object("LicenseModel")
        self.DateTimeBox = Builder.get_object("DateTimeBox")
        self.Create = Builder.get_object("Create")
        self.Clear = Builder.get_object("Clear")
        self.FileActive = Builder.get_object("FileActive")
        self.ThumbnailTypeActive = Builder.get_object("ThumbnailTypeActive")
        self.ThumbnailFileActive = Builder.get_object("ThumbnailFileActive")
        self.LicenseTypeActive = Builder.get_object("LicenseTypeActive")
        self.SelectPrevious = Builder.get_object("SelectPrevious")
        self.SelectNext = Builder.get_object("SelectNext")
        self.OpenOnCurrent = Builder.get_object("OpenOnCurrent")
        self.OpenOnNew = Builder.get_object("OpenOnNew")
        self.MoveLeft = Builder.get_object("MoveLeft")
        self.MoveRight = Builder.get_object("MoveRight")
        self.MoveUp = Builder.get_object("MoveUp")
        self.MoveDown = Builder.get_object("MoveDown")
        self.TagSelect = Builder.get_object("TagSelect")
        self.OSI = Builder.get_object("OSI")
        self.FSF = Builder.get_object("FSF")
        self.LicenseName = Builder.get_object("LicenseName")
        self.LicenseNameLabel = Builder.get_object("LicenseNameLabel")
        self.LicenseURL = Builder.get_object("LicenseURL")
        self.LicenseURLLabel = Builder.get_object("LicenseURLLabel")

        LBCLabel = CAPI.ToObject(Icons.Get()["LBCLabel"])
        self.Deposit.get_parent().get_children()[1].set_from_pixbuf(LBCLabel)
        self.Price.get_parent().get_children()[1].set_from_pixbuf(LBCLabel)

        self.Identifier = Identity.Create(False)
        self.Identities = CAPI.ToObject(self.Identifier["Identities"])
        self.ChannelsBox.pack_start(
            CAPI.ToObject(self.Identifier["IdentitiesBox"]), True, True, 0
        )

        self.DateTimer = DateTime.Create()
        DateTimeObject = CAPI.ToObject(self.DateTimer["DateTime"])
        self.DateTimeBox.pack_start(DateTimeObject, True, True, 0)

        self.Tagger = Tag.Create(True, self.TabPointer, None)
        self.TagBox.pack_start(CAPI.ToObject(self.Tagger["Tag"]), True, True, 0)

        self.Languager = Language.Create(self.TabPointer)
        LanguageObject = CAPI.ToObject(self.Languager["Language"])
        self.LanguageBox.pack_start(LanguageObject, True, True, 0)

        with open(Places.JsonDir + "Licenses.json") as Licenses:
            self.Licenses = json.load(Licenses)["licenses"]
        for License in self.Licenses:
            try:
                OSI = License["isOsiApproved"]
            except:
                OSI = False
            try:
                FSF = License["isFsfLibre"]
            except:
                FSF = False

            self.LicenseModel.append(
                [License["name"], License["seeAlso"][0], OSI, FSF]
            )

        self.DateUse = CAPI.ToObject(self.DateTimer["DateUse"])
        DateHour = CAPI.ToObject(self.DateTimer["Hour"])
        DateMinute = CAPI.ToObject(self.DateTimer["Minute"])
        DateSecond = CAPI.ToObject(self.DateTimer["Second"])
        DateReset = CAPI.ToObject(self.DateTimer["DateReset"])
        DateActive = CAPI.ToObject(self.DateTimer["DateActive"])
        DateUseActive = CAPI.ToObject(self.DateTimer["DateUseActive"])
        Widgets = [self.Identities, CAPI.ToObject(self.Identifier["Active"])]
        Widgets.extend([self.Name, self.PTitle, self.File, self.FileActive])
        Widgets.extend([self.Description, self.ThumbnailType])
        Widgets.extend([self.ThumbnailTypeActive, self.ThumbnailUrl])
        Widgets.extend([self.ThumbnailFile, self.ThumbnailFileActive])
        Widgets.extend([self.TagBox, self.Deposit, self.Price])
        Widgets.extend([self.DateTimeBox, DateActive, DateHour])
        Widgets.extend([DateMinute, DateSecond, DateReset, self.DateUse])
        Widgets.extend([DateUseActive, self.LanguageBox])
        Widgets.extend([self.LicenseType, self.LicenseTypeActive, self.License])
        Widgets.extend([self.LicenseName, self.LicenseURL, self.Create])
        Widgets.extend([self.Clear])
        Items = SelectUtil.PrepareWidgets(
            Widgets, [self.Tagger], [self.Languager], [self.DateTimer]
        )
        self.Selector = Select(*Items)

    def StopScroll(self, Widget, Event):
        Widget.emit_stop_by_name("scroll-event")

    def on_OpenOnCurrent_activate(self, Widget):
        if self.NewPublication.get_realized():
            self.Selector.Activate(0)

    def on_OpenOnNew_activate(self, Widget):
        if self.NewPublication.get_realized():
            self.Selector.Activate(1)

    def on_SelectPrevious_activate(self, Widget):
        if self.NewPublication.get_realized():
            self.Selector.Backward()

    def on_SelectNext_activate(self, Widget):
        if self.NewPublication.get_realized():
            self.Selector.Forward()

    def on_TagSelect_activate(self, Widget):
        if self.NewPublication.get_realized():
            if self.Selector.CurrentItem == 8:
                Tag.Select(self.Tagger["Pointer"])
            elif self.Selector.CurrentItem == 17:
                Tag.Select(self.Languager["TaggerPointer"])

    def on_MoveLeft_activate(self, Widget):
        if self.NewPublication.get_realized():
            if self.Selector.CurrentItem == 8:
                Move.FlowBoxLeftRight(self.Tagger["FlowBox"], -1)
            elif self.Selector.CurrentItem == 17:
                Move.FlowBoxLeftRight(self.Languager["FlowBox"], -1)

    def on_MoveRight_activate(self, Widget):
        if self.NewPublication.get_realized():
            if self.Selector.CurrentItem == 8:
                Move.FlowBoxLeftRight(self.Tagger["FlowBox"], 1)
            elif self.Selector.CurrentItem == 17:
                Move.FlowBoxLeftRight(self.Languager["FlowBox"], 1)

    def on_MoveDown_activate(self, Widget):
        if self.NewPublication.get_realized():
            if self.Selector.CurrentItem == 8:
                Move.FlowBoxUpDown(self.Tagger["FlowBox"], 1)
            elif self.Selector.CurrentItem == 17:
                Move.FlowBoxUpDown(self.Languager["FlowBox"], 1)

    def on_MoveUp_activate(self, Widget):
        if self.NewPublication.get_realized():
            if self.Selector.CurrentItem == 8:
                Move.FlowBoxUpDown(self.Tagger["FlowBox"], -1)
            elif self.Selector.CurrentItem == 17:
                Move.FlowBoxUpDown(self.Languager["FlowBox"], -1)

    def ShowNewPublication(self):
        Identity.Update(self.Identifier["Pointer"])
        self.on_Clear_button_press_event()
        self.on_ThumbnailType_changed(self.ThumbnailType)
        self.on_LicenseType_changed(self.LicenseType)

        Replace.Replace(self.TabPointer, "NewPublication")
        self.Title.set_text("New Publication")

    def on_LicenseEntry_changed(self, Widget):
        NewLicense, ID = Widget.get_text(), 0
        for License in self.Licenses:
            if NewLicense == License["name"]:
                self.License.set_active(ID)
                break
            ID += 1
        Active, OSI, FSF = self.License.get_active(), False, False
        if Active != -1:
            Path = Gtk.TreePath.new_from_indices([Active])
            Iter = self.LicenseModel.get_iter(Path)
            OSI = self.LicenseModel.get_value(Iter, 2)
            FSF = self.LicenseModel.get_value(Iter, 3)

        self.OSI.set_active(OSI)
        self.FSF.set_active(FSF)

    def NoToggleBefore(self, Widget):
        Widget.set_name(str(Widget.get_active()))

    def NoToggleAfter(self, Widget):
        Widget.set_active(Widget.get_name() == "True")

    def on_ThumbnailType_changed(self, Widget):
        if Widget.get_active() == 0:
            self.ThumbnailUrl.set_visible(False)
            self.ThumbnailFile.set_visible(True)
        else:
            self.ThumbnailUrl.set_visible(True)
            self.ThumbnailFile.set_visible(False)

    def on_LicenseType_changed(self, Widget):
        Active = Widget.get_active() == 0
        self.License.set_visible(Active)
        self.LicenseLabel.set_visible(Active)
        self.LicenseName.set_visible(not Active)
        self.LicenseNameLabel.set_visible(not Active)
        self.LicenseURL.set_visible(not Active)
        self.LicenseURLLabel.set_visible(not Active)

    def on_Clear_button_press_event(self, Widget="", Event=""):
        self.License.set_active(-1)
        ComboObject = CAPI.ToObject(self.Languager["Combo"])
        ComboObject.get_child().set_text("")
        ComboObject.set_active(-1)
        self.ThumbnailType.set_active(0)
        Tag.RemoveAll(self.Tagger["Pointer"])
        Tag.RemoveAll(self.Languager["TaggerPointer"])
        self.Name.set_text("")
        self.PTitle.set_text("")
        self.ThumbnailUrl.set_text("")
        CAPI.ToObject(self.Tagger["Entry"]).set_text("")
        self.Deposit.set_value(0.1)
        self.Price.set_value(0)
        self.File.set_filename("")
        self.ThumbnailFile.set_filename("")

        DateTime.OnDateResetButtonPressEvent(self.DateTimer["Pointer"])
        self.DateUse.set_active(False)
        Buffer = self.Description.get_buffer()
        Buffer.set_text("")

    def on_Create_button_press_event(self, Widget, Event):
        Channel = json.loads(Identity.Get(self.Identifier["Pointer"]))["Name"]
        Title = self.PTitle.get_text()
        Name, File = self.Name.get_text(), self.File.get_filename()

        Buffer = self.Description.get_buffer()
        Start, End = Buffer.get_start_iter(), Buffer.get_end_iter()

        Description = Buffer.get_text(Start, End, True)

        ThumbnailFile, ThumbnailUrl = "", ""
        if self.ThumbnailType.get_active() == 0:
            ThumbnailFile = self.ThumbnailFile.get_filename()
        else:
            ThumbnailUrl = self.ThumbnailUrl.get_text()

        Deposit = self.Deposit.get_value()
        Tags = Tag.GetTags(self.Tagger["Pointer"])
        ReleaseTime = DateTime.GetTime(self.DateTimer["Pointer"], 0)
        Price = self.Price.get_value()

        Languages = Tag.GetTags(self.Languager["TaggerPointer"])
        for Index in range(len(Languages)):
            Languages[Index] = Languages[Index].split("/")[0]

        try:
            Name.encode("ascii")
        except:
            Popup.Message(NameMessage)
            return

        if Name.find(" ") != -1:
            Popup.Message(NameMessage)
            return

        if self.LicenseType.get_active() == 0:
            Active = self.License.get_active()
            if Active == -1:
                Popup.Message(LicenseMessage)
                return
            Path = Gtk.TreePath.new_from_indices([Active])
            Iter = self.LicenseModel.get_iter(Path)
            License = self.LicenseModel.get_value(Iter, 0)
            LicenseUrl = self.LicenseModel.get_value(Iter, 1)
        else:
            License = self.LicenseName.get_text()
            LicenseUrl = self.LicenseURL.get_text()

        args = {
            "name": Name,
            "bid": str(Deposit),
            "file_path": File,
            "license": License,
            "license_url": LicenseUrl,
        }

        if Title != "":
            args["title"] = Title

        if Description != "":
            args["description"] = Description

        if Channel != "":
            args["channel_name"] = Channel

        if ThumbnailUrl != "":
            args["thumbnail_url"] = ThumbnailUrl

        if ThumbnailFile != "":
            args["thumbnail_file"] = ThumbnailFile

        if Tags != []:
            args["tags"] = Tags

        if Languages != []:
            args["languages"] = Languages

        if ReleaseTime != -1:
            args["release_time"] = ReleaseTime

        # TODO: In future we can check for the fee_address as well
        if Price > 0:
            args["fee_currency"] = "LBC"
            args["fee_amount"] = Price

        threading.Thread(None, self.UploadThread, None, [args]).start()

    def UploadThread(self, args):
        LBRYSettings = json.loads(Preferences.Get())
        Session = LBRYSettings["Preferences"]["Session"]

        # Check for if we need to make thumbnail claim first or not
        NoThumbnail = "thumbnail_url" not in args and "thumbnail_file" in args
        JsonData = Content.Publish(
            NoThumbnail, Session["Server"], str(args).replace("'", '"')
        )
        JsonData = json.loads(JsonData)

        if "error" in JsonData:
            Popup.Error(JsonData["error"])
            return
        Popup.Message("Publication successful.")
