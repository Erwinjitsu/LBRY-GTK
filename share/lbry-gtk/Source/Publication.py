################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2023 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import gi, threading, time, os, re, queue, json, math

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GLib, Gdk

from Source.FetchComment import FetchComment
from Source.PublicationControl import PublicationControl
from Source.PyGObjectCAPI import PyGObjectCAPI
from CRewrite import Channel, Document, Global, Markdown, Move, Places, Popup
from CRewrite import Preferences, Replace, Tag, Thumbnail, Url

CAPI = PyGObjectCAPI()


def GetType(List, Type):
    for Item in List:
        if isinstance(Item, Type):
            return Item


class DataTreeStore:
    def __init__(self, DataStore):
        self.DataStore = DataStore
        self.Ignored = []

    def ToStore(self, Branch, Key, Value):
        return self.DataStore.append(Branch, (str(Key), str(Value)))

    def ParseCollection(self, Branches, Blocks, Branch, Key, Value):
        if Key in self.Ignored:
            return
        if isinstance(Value, (dict, list)):
            Blocks.append(Value)
            Branches.append(self.ToStore(Branch, Key, ""))
        else:
            self.ToStore(Branch, Key, Value)

    def TypeParse(self, Data):
        Blocks = [Data]
        Branches = [None]
        while Blocks:
            Block = Blocks.pop(0)
            Branch = Branches.pop(0)
            if isinstance(Block, dict):
                for Key in Block:
                    self.ParseCollection(
                        Branches, Blocks, Branch, Key, Block[Key]
                    )
            elif isinstance(Block, list):
                for Index in range(len(Block)):
                    self.ParseCollection(
                        Branches, Blocks, Branch, Index, Block[Index]
                    )

    def NewDataset(self, Data, ShowAll):
        self.DataStore.clear()
        if not ShowAll:
            # TODO: Add more options here if required
            self.Ignored = ["description", "tags", "title", "name"]
        else:
            self.Ignored = []
        self.TypeParse(Data)


class Publication:
    def __init__(self, Title, MainSpace, AddPage, TabPointer):
        Builder = CAPI.ToObject(Global.Builder)
        self.Title = Title
        Builder.add_from_file(Places.GladeDir + "Publication.glade")
        Builder.connect_signals(self)
        self.Publication = Builder.get_object("Publication")
        self.DataView = Builder.get_object("DataView")
        self.CommentsBox = Builder.get_object("CommentsBox")
        self.ContentBox = Builder.get_object("ContentBox")
        self.ContentTitle = Builder.get_object("ContentTitle")
        self.TopBox = Builder.get_object("TopBox")
        self.BottomStack = Builder.get_object("BottomStack")
        self.ActionWidget = Builder.get_object("ActionWidget")
        self.DescriptionBox = Builder.get_object("DescriptionBox")
        self.TagBox = Builder.get_object("TagsBox")
        self.LinksBox = Builder.get_object("LinksBox")
        self.DataBox = Builder.get_object("DataBox")
        self.PublicationControlBox = Builder.get_object("PublicationControlBox")
        self.DataAction = Builder.get_object("DataAction")
        self.TagsAction = Builder.get_object("TagsAction")
        self.DescriptionAction = Builder.get_object("DescriptionAction")
        self.LinksAction = Builder.get_object("LinksAction")
        self.CommentsAction = Builder.get_object("CommentsAction")
        self.MoveLeft = Builder.get_object("MoveLeft")
        self.MoveRight = Builder.get_object("MoveRight")
        self.MoveUp = Builder.get_object("MoveUp")
        self.MoveDown = Builder.get_object("MoveDown")
        self.OpenOnCurrent = Builder.get_object("OpenOnCurrent")
        self.OpenOnNew = Builder.get_object("OpenOnNew")
        self.SelectNext = Builder.get_object("SelectNext")
        self.SelectPrevious = Builder.get_object("SelectPrevious")
        self.TagSelect = Builder.get_object("TagSelect")
        self.DataView.set_search_equal_func(self.SearchFunction)
        self.ThumbnailBox = Builder.get_object("ThumbnailBox")
        self.DataStore = Builder.get_object("DataStore")
        self.MainSpace = MainSpace
        self.TabPointer = TabPointer

        self.StackBoxes = [
            self.DescriptionBox,
            self.CommentsBox,
            self.TagBox,
            self.LinksBox,
            self.DataBox,
        ]

        self.DataStorer = DataTreeStore(self.DataStore)

        self.Markdowner = Markdown.Create(
            "",
            "",
            False,
            True,
            False,
            TabPointer,
            AddPage,
            0,
        )
        self.DescriptionBox.pack_start(
            CAPI.ToObject(self.Markdowner.Document), True, True, 0
        )

        self.Linker = Markdown.Create(
            "",
            "",
            False,
            True,
            True,
            TabPointer,
            AddPage,
            0,
        )
        self.LinksBox.pack_start(
            CAPI.ToObject(self.Linker.Document), True, True, 0
        )

        self.Tagger = Tag.Create(False, TabPointer, AddPage)
        self.TagBox.pack_start(CAPI.ToObject(self.Tagger["Tag"]), True, True, 0)

        self.Commenter = FetchComment(
            TabPointer,
            AddPage,
            self.BottomStack,
        )

        self.Documenter = Document.Create(
            AddPage,
            CAPI.AsVoidPointer(Title.__gpointer__),
            CAPI.AsVoidPointer(MainSpace.__gpointer__),
            self.TabPointer,
        )

        self.Links = []
        self.PublicationControler = PublicationControl(
            self.Tagger,
            self.Links,
            AddPage,
            self.TabPointer,
        )

        # TopBox widgets
        self.TopBox.pack_start(
            self.PublicationControler.TypeDependent, False, False, 0
        )

        self.Thumbnailer = Thumbnail.Create(0, 0, 0)
        FrameObject = CAPI.ToObject(self.Thumbnailer["Frame"])
        self.ThumbnailBox.pack_start(FrameObject, True, True, 0)
        self.TopBox.pack_start(self.ThumbnailBox, True, True, 0)

        self.TopBox.pack_start(
            self.PublicationControler.Regular, False, False, 0
        )
        self.PageNumber = 0
        self.DoUpdateSize = True
        CAPI.ToObject(Global.Window).connect(
            "configure-event",
            self.QueueHeight,
        )

        # Hide page contents for all but first one
        for Index in range(1, 5):
            self.StackBoxes[Index].hide()

        # Update BottomStack on every scroll
        self.MainSpace.connect("scroll-event", self.UpdateBottomStack)

        self.StepIncrement = None

    def QueueHeight(self, Window, Event):
        self.DoUpdateSize = True

    def UpdateBottomStack(self, Widget, Event):
        # Queue drawing on every scroll
        self.BottomStack.queue_draw()

    def on_BottomStack_draw(self, Widget, CairoOrEvent):
        # Set height of ActionWidget in a way that the page buttons are always
        # visible. Animated for no blinking

        # Define self.StepIncrement if undefined
        # If self.StepIncrement is undefined it's also reasonable to assume
        # it is the first frame, there's no scroll, and the buttons are going
        # to be rendered in the default position
        if self.StepIncrement == None:
            self.StepIncrement = (
                self.MainSpace.get_vadjustment().get_step_increment() * 1.5
            )
            return

        # Get where the notebook stars and where the top of displayed content is
        Start = self.ThumbnailBox.get_allocated_height()
        Current = math.floor(self.MainSpace.get_vadjustment().get_value())

        # Calculate desired position
        if Start < Current:
            Goal = math.floor((Current - Start))
        else:
            Goal = -1

        # Get current height, for animation, add one in case of upward
        # scrolling, to fix single pixel misalign bug
        Height = self.ActionWidget.get_size_request().height
        if Height < Current - Start:
            Height += 1

        # Calculate the difference between the current and desired height
        Delta = Goal - Height
        WeightedDelta = Delta / self.StepIncrement

        # If it's already in the desired position, skip animation
        if Delta == 0:
            self.ActionWidget.set_size_request(-1, Goal)
        else:
            # Preventing overflows (math.sinh(22)=infinity)
            if WeightedDelta > 20:
                WeightedDelta = 20
            elif WeightedDelta < -20:
                WeightedDelta = -20

            # Calculating dHeight/dFrame
            Velocity = 5 * math.sinh(WeightedDelta)

            # Set height
            if math.fabs(Velocity) > math.fabs(Delta):
                self.ActionWidget.set_size_request(-1, Goal)
            else:
                self.ActionWidget.set_size_request(-1, Height + Velocity)

    def PublicationPageDraw(self, Widget, NotUsed):
        if self.DoUpdateSize:
            Height = self.MainSpace.get_allocated_height()
            self.TopBox.set_size_request(-1, Height / 2)
            self.BottomStack.set_size_request(-1, Height / 2)
            self.DoUpdateSize = False

    def OnPageSwitch(self, BottomStack, Page, PageNumber):
        self.StackBoxes[PageNumber].show()
        self.StackBoxes[self.PageNumber].hide()
        self.PageNumber = PageNumber
        if PageNumber == 4:
            self.DataView.show()
            GLib.idle_add(self.DataView.scroll_to_point, 0, 1)

    def ShowLinks(self, Links):
        Text = ""
        del self.Links[:]
        for Link in Links[:-1]:
            self.Links.append(Link[1])
            Text += "[%s](%s)\n\n" % (Link[0], Link[1])
        self.Links.append(Links[-1][1])
        self.Linker.Text = Text
        self.Linker.Fill()

    def GetLinks(self, GotUrl):
        Links = json.loads(Url.Web(GotUrl))
        GLib.idle_add(self.ShowLinks, Links)

    def GetTitle(self, Data, PrefixBool):
        Title = ""
        try:
            if PrefixBool:
                Title = Data["signing_channel"]["value"]["title"] + " - "
        except:
            pass
        try:
            Title += Data["value"]["title"]
        except:
            Title += Data["name"][1:]
        return Title

    def SetTabText(self, PageNumber, Name, Tooltip):
        Child = self.BottomStack.get_nth_page(PageNumber)
        self.BottomStack.set_tab_label_text(Child, Name)
        self.BottomStack.get_tab_label(Child).set_tooltip_text(Tooltip)

    def ShowPublication(self, Data):
        CommentBoxes = self.CommentsBox.get_children()
        if CommentBoxes != []:
            CommentBoxes[0].destroy()

        # It is healthy to get settings now
        LBRYSettings = json.loads(Preferences.Get())
        LBRYGTKSettings = LBRYSettings["Preferences"]
        Session = LBRYGTKSettings["Session"]

        # Metadata goes to TreeStore
        self.DataStorer.NewDataset(Data, LBRYGTKSettings["MetadataShowAll"])

        Replace.Replace(self.TabPointer, "Publication")
        NewTitle = self.GetTitle(Data, True)
        self.PublicationControler.ShowPublicationControl(Data, NewTitle)
        self.ContentTitle.set_text(self.GetTitle(Data, False))
        self.Title.set_text(NewTitle)

        # Channel info
        try:
            TryData = Data["signing_channel"]
            ChannelUrl = TryData["canonical_url"]
            ChannelID = TryData["claim_id"]
            try:
                ChannelName = TryData["value"]["title"]
            except:
                ChannelName = TryData["name"]
        except:
            ChannelUrl = Data["canonical_url"]
            ChannelID = Data["claim_id"]
            try:
                ChannelName = Data["value"]["title"]
            except:
                ChannelName = Data["name"]

        # Thumbnail
        try:
            ThumbnailUrl = Data["value"]["thumbnail"]["url"]
        except:
            ThumbnailUrl = ""
        IfChannel = Data["value_type"] == "channel"

        Thumbnail.SetExtra(self.Thumbnailer["Pointer"], IfChannel, ThumbnailUrl)
        CAPI.ToObject(self.Thumbnailer["Frame"]).queue_draw()

        Args = [Data["canonical_url"]]
        threading.Thread(None, self.GetLinks, None, Args).start()

        # Description
        if "description" in Data["value"]:
            self.Markdowner.Text = str(Data["value"]["description"])
        else:
            self.Markdowner.Text = "No description"
        self.Markdowner.Fill()

        self.Publication.show_all()

        # Tags
        Tag.RemoveAll(self.Tagger["Pointer"])
        TagAmount = 0
        if "tags" in Data["value"]:
            Tag.Append(Data["value"]["tags"], self.Tagger["Pointer"])
            TagAmount = len(Data["value"]["tags"])
        if TagAmount == 0:
            # Hide tags page if no tag present
            self.BottomStack.get_nth_page(2).hide()
        else:
            self.SetTabText(
                2,
                "🏷️ (" + str(TagAmount) + ")",
                "Tags - Total of " + str(TagAmount) + " tags",
            )

        if LBRYGTKSettings["EnableComments"]:
            ChannelList = json.loads(Channel.Get(Session["Server"]))
            CommentServer = LBRYGTKSettings["CommentServer"]
            self.SetTabText(1, "💬", "Comments")
            self.BottomStack.get_nth_page(1).show()
            CommentWrapper = Gtk.Box.new(Gtk.Orientation.VERTICAL, 0)
            ClaimID = Data["claim_id"]
            self.Commenter.SingleComment(
                ClaimID,
                CommentWrapper,
                {
                    "Replies": 0,
                    "Support": 0,
                    "Channel": "",
                    "Text": "",
                    "Id": "",
                    "Url": "",
                    "Thumbnail": "",
                    "TimeStamp": 0,
                    "Value": 0,
                    "Number": 0,
                    "Title": "",
                },
                [0, 0, False, False, False],
                CommentServer,
                True,
                LBRYGTKSettings,
                ChannelList,
                ChannelUrl,
                queue.Queue(),
            )
            self.ActiveComment = CommentWrapper.get_children()[0]
            self.MakeActive(self.ActiveComment)
            self.CommentsBox.add(CommentWrapper)
            Args = [ClaimID, CommentWrapper, queue.Queue(), CommentServer]
            Args.extend([ChannelUrl, LBRYGTKSettings, ChannelName, ChannelID])
            threading.Thread(None, self.Commenter.Fetch, None, Args).start()
            Args = [self.CommentsBox, ChannelID, CommentServer]
            threading.Thread(
                None, self.Commenter.DisplayPrice, None, Args
            ).start()
        else:
            self.BottomStack.get_nth_page(1).hide()

        self.Markdown = [self.Markdowner.SelectNext]
        self.Markdown.append(self.Markdowner.SelectPrevious)
        self.Link = [self.Linker.SelectNext, self.Linker.SelectPrevious]
        self.DoUpdateSize = True
        self.PublicationPageDraw(None, None)

        self.BottomStack.set_current_page(0)

    def Fraction(self, Children, Value):
        for Child in Children:
            if isinstance(Child, Gtk.ProgressBar):
                Child.set_fraction(Value)
                break

    def MakeActive(self, Comment=False):
        if not Comment:
            return
        Children = self.ActiveComment.get_children()[0].get_children()
        self.Fraction(Children, 0)
        Children = Comment.get_children()[0].get_children()
        self.Fraction(Children, 1)
        self.ActiveComment = Comment

    def SearchFunction(self, Model, Column, Key, Iter):
        Row = Model[Iter]
        if Key.lower() in list(Row)[Column].lower():
            return False

        for Inner in Row.iterchildren():
            if Key.lower() in list(Inner)[Column].lower():
                self.DataView.expand_to_path(Row.path)
                break
        else:
            self.DataView.collapse_row(Row.path)
        return True

    def PageActivation(self, PageNumber, Widget):
        if self.BottomStack.get_nth_page(PageNumber).is_visible():
            self.BottomStack.set_current_page(PageNumber)
            Widget.grab_focus()

    def on_DescriptionAction_activate(self, Widget):
        self.PageActivation(0, CAPI.ToObject(self.Markdowner.TextBox))

    def on_CommentsAction_activate(self, Widget):
        self.PageActivation(1, self.ActiveComment)

    def on_TagsAction_activate(self, Widget):
        self.PageActivation(2, CAPI.ToObject(self.Tagger["FlowBox"]))

    def on_LinksAction_activate(self, Widget):
        self.PageActivation(3, CAPI.ToObject(self.Linker.TextBox))

    def on_DataAction_activate(self, Widget):
        self.PageActivation(4, self.DataView)
        self.DataView.get_selection().select_path(0)
        Selection = self.DataView.get_selection()
        Path = Selection.get_selected_rows()[1][0]
        self.DataView.expand_row(Path, False)

    def on_MoveLeft_activate(self, Widget):
        if not self.Publication.get_realized():
            return
        CurrentPage = self.BottomStack.get_current_page()
        if CurrentPage == 2:
            Move.FlowBoxLeftRight(self.Tagger["FlowBox"], -1)
        elif CurrentPage == 1:
            Parent = self.ActiveComment.get_parent().get_parent()
            if isinstance(Parent, Gtk.Box):
                Expander = Parent.get_parent()
                self.MakeActive(Expander.get_parent().get_parent().get_parent())
                Expander.set_expanded(False)
        elif self.DataView.get_realized():
            self.SelectDataUpdate(self.DataExitUpdate)

    def DataExitUpdate(self):
        try:
            Selection = self.DataView.get_selection()
            Path = Selection.get_selected_rows()[1][0]
            Path.up()
            Selection.select_path(Path)
            self.DataView.collapse_row(Path)
        except:
            pass

    def on_MoveRight_activate(self, Widget):
        if not self.Publication.get_realized():
            return
        CurrentPage = self.BottomStack.get_current_page()
        if CurrentPage == 2:
            Move.FlowBoxLeftRight(self.Tagger["FlowBox"], 1)
        elif CurrentPage == 1:
            Children = self.ActiveComment.get_children()[0].get_children()
            Expander = GetType(Children, Gtk.Frame).get_children()[0]
            Box = GetType(Expander.get_children(), Gtk.Box)
            SubComments = GetType(Box.get_children(), Gtk.Box).get_children()
            if len(SubComments) != 0:
                self.MakeActive(SubComments[0])
                Expander.set_expanded(True)
        elif self.DataView.get_realized():
            Selection = self.DataView.get_selection()
            Path = Selection.get_selected_rows()[1][0]
            if self.DataView.expand_row(Path, False):
                Path.append_index(0)
                Selection.select_path(Path)
            else:
                self.SelectDataUpdate()

    def GetChild(self, Change):
        Box = self.ActiveComment.get_parent()
        Children = Box.get_children()
        Position = Box.child_get_property(self.ActiveComment, "position")
        if Position + Change < 0:
            return
        for Child in Children:
            if Box.child_get_property(Child, "position") == Position + Change:
                return Child

    def MoveAction(self, Direction):
        if not self.Publication.get_realized():
            return
        CurrentPage = self.BottomStack.get_current_page()
        if CurrentPage == 2:
            Move.FlowBoxUpDown(self.Tagger["FlowBox"], Direction)
        elif CurrentPage == 1:
            self.MakeActive(self.GetChild(Direction))
        elif self.DataView.get_realized():
            DataViewPointer = CAPI.AsVoidPointer(self.DataView.__gpointer__)
            Move.ListViewUpDown(DataViewPointer, Direction)
            Path = self.DataView.get_selection().get_selected_rows()[1][0]
            self.DataView.scroll_to_cell(Path, None, False, 0, 0)

    def on_MoveDown_activate(self, Widget):
        self.MoveAction(1)

    def on_MoveUp_activate(self, Widget):
        self.MoveAction(-1)

    def Opener(self, Offset):
        if self.Publication.get_realized():
            CurrentPage = self.BottomStack.get_current_page()
            if self.TopBox.has_focus():
                self.PublicationControler.Selector.Activate(Offset)
            elif CurrentPage == 0:
                self.Markdowner.Activate(Offset)
            elif CurrentPage == 1:
                self.ActiveComment.set_name(str(-2 - Offset))
            elif CurrentPage == 2:
                Tag.OnSearchButtonPressEvent(self.Tagger["Pointer"], Offset + 1)
            elif CurrentPage == 3:
                self.Linker.Activate(Offset)
        elif CAPI.ToObject(self.Documenter.Document).get_realized():
            self.Documenter.Activate(Offset)

    def on_OpenOnCurrent_activate(self, Widget):
        self.Opener(0)

    def on_OpenOnNew_activate(self, Widget):
        self.Opener(1)

    def SelectDataUpdate(self, Function=False):
        try:
            if Function:
                self.DataView.set_cursor(Gtk.TreePath.new(), None, True)
            else:
                Path = self.DataView.get_selection().get_selected_rows()[1][0]
                Column = self.DataView.get_column(1)
                self.DataView.set_cursor(Path, Column, True)
        except:
            pass
        if Function:
            Function()

    def on_TagSelect_activate(self, Widget):
        if (
            self.Publication.get_realized()
            and self.BottomStack.get_current_page() == 2
        ):
            Tag.Select(self.Tagger["Pointer"])

    def SelectAction(self, Name, Method):
        if not self.Publication.get_realized():
            return
        if self.BottomStack.get_current_page() == 1:
            self.ActiveComment.set_name(Name)
        else:
            self.TopBox.grab_focus()
            Method()

    def on_SelectNext_activate(self, Widget):
        self.SelectAction("-0", self.PublicationControler.Selector.Forward)

    def on_SelectPrevious_activate(self, Widget):
        self.SelectAction("-1", self.PublicationControler.Selector.Backward)

    def CommentAction(self, Button, Index):
        if self.Publication.get_realized():
            if 15 < Index:
                CurrentPage = self.BottomStack.get_current_page()
                if CurrentPage == 0:
                    self.Markdown[Index - 16]()
                    return
                elif CurrentPage == 3:
                    self.Link[Index - 16]()
                    return
            self.ActiveComment.set_name("%s %s" % (str(Index), str(Button)))
        elif CAPI.ToObject(self.Documenter.Document).get_realized():
            if Index == 16:
                self.Documenter.SelectNext()
            elif Index == 17:
                self.Documenter.SelectPrevious()
