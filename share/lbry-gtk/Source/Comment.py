################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2023 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import threading, json, gi, queue, time

gi.require_version("Gtk", "3.0")
from gi.repository import GLib, Gtk

from Source import SelectUtil
from Source.Select import Select
from Source.PyGObjectCAPI import PyGObjectCAPI
from CRewrite import CommentDB, Comments, Global, Identity, Icons, Markdown
from CRewrite import Places, Popup, Support, Timestamp, Url, Preferences
from CRewrite import Profile

CAPI = PyGObjectCAPI()
Promo = "\n\nSent from [*LBRY-GTK*](https://codeberg.org/MorsMortium/LBRY-GTK)"
CssProvider = Gtk.CssProvider.new()
CssProvider.load_from_data(b"* {padding: 0px;}")


class Comment:
    def __init__(self, *Args):
        (
            self.ClaimID,
            self.Box,
            self.Row,
            Reactions,
            self.CommentServer,
            self.HiddenUI,
            self.Settings,
            self.ChannelList,
            self.PChannel,
            RowQueue,
            self.SingleComment,
            TabPointer,
            AddPage,
            self.BottomStack,
        ) = Args
        Builder = CAPI.ToObject(Global.Builder)
        Builder.add_from_file(Places.GladeDir + "Comment.glade")
        Builder.connect_signals(self)
        self.Comment = Builder.get_object("Comment")
        self.CommentTextBox = Builder.get_object("CommentTextBox")
        self.HideDislikedBox = Builder.get_object("HideDislikedBox")
        self.RepliesFrame = Builder.get_object("RepliesFrame")
        ReplySeparator = Builder.get_object("ReplySeparator")
        self.RepliesExpander = Builder.get_object("RepliesExpander")
        self.RepliesBox = Builder.get_object("RepliesBox")
        ProfileFrame = Builder.get_object("ProfileFrame")
        self.PreviewBox = Builder.get_object("PreviewBox")
        self.ReplyScrolled = Builder.get_object("ReplyScrolled")
        self.ReplyText = Builder.get_object("ReplyText")
        UnHideButton = Builder.get_object("UnHideButton")
        ProfileActive = Builder.get_object("ProfileActive")
        CommentActive = Builder.get_object("CommentActive")
        PreviewActive = Builder.get_object("PreviewActive")

        self.Identifier = Identity.Create(True)
        self.Identities = CAPI.ToObject(self.Identifier["Identities"])

        Profiler = Profile.Create(
            self.Row["Url"],
            self.Row["Thumbnail"],
            float(self.Row["Value"]),
            float(self.Row["Support"]),
            int(self.Row["Number"]),
            int(self.Row["TimeStamp"]),
            int(self.Settings["ProfileSize"]),
            self.Settings["Profile"],
            AddPage,
            self.Row["Title"],
            self.Settings["ProfileCircling"],
            TabPointer,
        )
        ProfileFrame.add(CAPI.ToObject(Profiler["Profile"]))

        self.Markdowns = [
            {"Text": self.Row["Text"], "Box": self.CommentTextBox}
        ]
        self.Markdowns.append({"Text": "", "Box": self.PreviewBox})

        for MarkdownItem in self.Markdowns:
            MarkdownItem["Markdowner"] = Markdown.Create(
                MarkdownItem["Text"],
                "",
                True,
                self.Settings["EnableMarkdown"],
                False,
                TabPointer,
                AddPage,
                0,
            )
            MarkdownItem["Box"].add(
                CAPI.ToObject(MarkdownItem["Markdowner"].Document)
            )
            MarkdownItem["Markdowner"].Fill()

        # The comment is being checked here whether it should
        # be hidden or not
        LikeBoost = max(5, Reactions[0] * 5)
        Hide = LikeBoost <= Reactions[1] and self.Settings["HideDisliked"]
        self.CommentTextBox.set_visible(not Hide)
        self.HideDislikedBox.set_visible(Hide)

        ChannelsBox2 = Builder.get_object("ChannelsBox2")
        self.Post = Builder.get_object("Post")
        self.Save = Builder.get_object("Save")
        self.Tip = Builder.get_object("Tip")
        self.TipLabel1 = Builder.get_object("TipLabel1")
        self.TipLabel2 = Builder.get_object("TipLabel2")
        self.TipLabel2.set_from_pixbuf(CAPI.ToObject(Icons.Get()["LBCLabel"]))

        self.Heart = Builder.get_object("Heart")
        self.Like = Builder.get_object("Like")
        self.Dislike = Builder.get_object("Dislike")
        self.LikeNumber = Builder.get_object("LikeNumber")
        self.DislikeNumber = Builder.get_object("DislikeNumber")
        self.Reply = Builder.get_object("Reply")
        ChannelsBox1 = Builder.get_object("ChannelsBox1")
        self.Edit = Builder.get_object("Edit")
        Delete = Builder.get_object("Delete")
        self.EditImage = Builder.get_object("EditImage")
        self.UndoImage = Builder.get_object("UndoImage")

        self.Like.set_active(Reactions[2])
        self.Dislike.set_active(Reactions[3])
        self.Heart.set_active(Reactions[4])

        for Channel in self.ChannelList:
            if Channel["Url"] == self.PChannel:
                self.Heart.set_no_show_all(False)
                self.Heart.set_sensitive(True)
            if Channel["Url"] == self.Row["Url"]:
                self.Edit.set_no_show_all(False)
                Delete.set_no_show_all(False)

        if Reactions[4]:
            self.Heart.set_no_show_all(False)

        self.LikeNumber.set_label(str(Reactions[0]))
        self.DislikeNumber.set_label(str(Reactions[1]))

        ImageActive = CAPI.ToObject(Profiler["ImageActive"])
        EventBox = CAPI.ToObject(Profiler["Thumbnailer"]["EventBox"])
        Channel = CAPI.ToObject(Profiler["Channel"])
        Publications = CAPI.ToObject(Profiler["Publications"])
        Markdowner0 = self.Markdowns[0]["Markdowner"]
        Markdowner1 = self.Markdowns[1]["Markdowner"]

        NoPaddings = [self.Heart, self.Like, self.Dislike]
        for NoPadding in NoPaddings:
            StyleContext = NoPadding.get_children()[0].get_style_context()
            StyleContext.add_provider(
                CssProvider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
            )
        self.Identities.get_cells()[0].set_padding(0, 0)

        # Thumbnail, channel, publications, unhide, commenttext, edit, delete
        # heart like dislike, reply, channels, replytext, tip, post, save
        # previewtext

        Widgets = [EventBox, ImageActive, Channel, Publications, UnHideButton]
        Widgets.extend([Markdowner0.Activate])
        Widgets.extend([CAPI.ToObject(Markdowner0.Document)])
        Widgets.extend([CommentActive, self.Edit, Delete])
        Widgets.extend([self.Heart.get_children()[0]])
        Widgets.extend([self.Like.get_children()[0]])
        Widgets.extend([self.Dislike.get_children()[0]])
        Widgets.extend([self.Reply, self.Identities])
        Widgets.extend([CAPI.ToObject(self.Identifier["Active"])])
        Widgets.extend([self.ReplyText, self.Tip, self.Post, self.Save])
        Widgets.extend([Markdowner1.Activate])
        Widgets.extend([CAPI.ToObject(Markdowner1.Document), PreviewActive])

        self.Selector = Select(*SelectUtil.PrepareWidgets(Widgets))
        self.RepliesFrame.set_margin_start(int(self.Settings["ReplyIndent"]))
        ReplySeparator.set_size_request(int(self.Settings["ReplySeparator"]), 0)

        if self.HiddenUI:
            ChannelsBox2.add(self.Identities.get_parent())
            ToShow = [self.ReplyScrolled, self.Post, self.Tip, self.TipLabel1]
            ToShow.extend([self.TipLabel2, self.PreviewBox.get_parent()])
            for Widget in ToShow:
                Widget.set_visible(True)
            ToHide = [Delete, self.Like, self.Heart, self.Edit, self.Reply]
            ToHide.extend([self.CommentTextBox, ChannelsBox1, self.Save])
            ToHide.extend([self.Dislike, ProfileFrame, self.LikeNumber])
            ToHide.extend([CommentActive, self.HideDislikedBox])
            ToHide.extend([self.DislikeNumber])
            for Widget in ToHide:
                Widget.set_no_show_all(True)
                Widget.hide()
        else:
            ChannelsBox2.set_no_show_all(True)
            ChannelsBox2.hide()
            ChannelsBox1.add(self.Identities.get_parent())

        self.Box.add(self.Comment)
        self.MarkdownCommands = [self.MarkdownDown, self.MarkdownUp]
        self.Box.show_all()
        if self.Row["Replies"] != 0:
            self.RepliesFrame.set_no_show_all(False)
            self.RepliesExpander.set_label(
                "Replies (%s)" % str(self.Row["Replies"])
            )
            self.RepliesFrame.show_all()
            RowQueue.put(self.RepliesBox)
        self.Text = ""
        self.Timer = 5

    def GetMarkdown(self):
        CurrentWidget = self.Selector.Widgets[self.Selector.CurrentItem]
        for Markdowner in self.Markdowns:
            if CurrentWidget == CAPI.ToObject(
                Markdowner["Markdowner"].Document
            ):
                return Markdowner["Markdowner"]
        return False

    def MarkdownDown(self):
        Markdowner = self.GetMarkdown()
        if Markdowner:
            Markdowner.SelectNext()

    def MarkdownUp(self):
        Markdowner = self.GetMarkdown()
        if Markdowner:
            Markdowner.SelectPrevious()

    def on_Comment_notify(self, Widget, Property):
        if Property.name != "name":
            return
        Index = Widget.get_name()
        if Index == "-0":
            self.Selector.Forward()
        elif Index == "-1":
            self.Selector.Backward()
        elif Index == "-2":
            self.Selector.Activate(0)
        elif Index == "-3":
            self.Selector.Activate(1)
        else:
            Index = Index.split()
            Button = int(Index[1])
            Index = int(Index[0])
            if 15 < Index:
                self.MarkdownCommands[Index - 16]()
                return
            self.Selector.Activate(Button, Index)

    def on_ReplyText_draw(self, Widget, Discard=""):
        self.ReplyScrolled.get_vadjustment().set_value(0)

    def ReplyTextHelper(self, Widget, Text):
        Text = self.Text
        if (
            self.Settings["PromoteLBRYGTK"]
            and self.Text != ""
            and not self.Save.get_visible()
        ):
            Text += Promo
        self.Markdowns[1]["Text"] = Text
        self.Markdowns[1]["Markdowner"].Text = self.Markdowns[1]["Text"]
        self.Markdowns[1]["Markdowner"].Fill()
        self.Box.show_all()

    def ReplyTextThread(self, Widget, Text):
        # Timer function to delay showing text until user has stopped typing
        while self.Timer < 5:
            time.sleep(0.1)
            self.Timer += 1
        GLib.idle_add(self.ReplyTextHelper, Widget, Text)

    def on_ReplyText_key_release_event(self, Widget, Discard=""):
        TextBuffer = Widget.get_buffer()
        End = TextBuffer.get_end_iter()
        Text = TextBuffer.get_text(TextBuffer.get_start_iter(), End, False)
        # Checks the buffer has changed
        if Text == self.Text:
            return
        self.Text = Text
        if self.Timer != 5:
            self.Timer = 0
            return
        self.Timer = 0
        Args = [Widget, Text]
        threading.Thread(None, self.ReplyTextThread, None, Args).start()

    def on_UnHideButton_button_press_event(self, Widget, Discard=""):
        self.HideDislikedBox.set_visible(False)
        self.CommentTextBox.set_visible(True)

    def on_Edit_button_press_event(self, Widget, Event=""):
        TextBuffer = self.ReplyText.get_buffer()
        ToSet = Widget.get_image() == self.EditImage
        if ToSet:
            TextBuffer.set_text(self.Row["Text"])
            Widget.set_image(self.UndoImage)
        else:
            TextBuffer.set_text("")
            Widget.set_image(self.EditImage)
        Widget.set_tooltip_text(["Edit", "Undo"][ToSet])
        Visible = self.ReplyScrolled.get_name() == "True"
        self.ReplyScrolled.set_visible(ToSet or Visible)
        self.PreviewBox.get_parent().set_visible(ToSet or Visible)
        self.Save.set_visible(ToSet)
        self.Post.set_visible(not ToSet and Visible)
        self.on_ReplyText_key_release_event(self.ReplyText)

    def on_Delete_button_press_event(self, Widget, Event=""):
        for Channel in self.ChannelList:
            if Channel["Url"] == self.Row["Url"]:
                GLib.idle_add(self.DeleteHelper, Widget, Channel)
                break

    def DeleteHelper(self, Widget, Channel):
        Dialog = Gtk.MessageDialog(
            CAPI.ToObject(Global.Window), buttons=Gtk.ButtonsType.OK_CANCEL
        )
        Dialog.props.text = "Are you sure you want to delete the comment?"
        Response = Dialog.run()
        Dialog.destroy()
        if Response == Gtk.ResponseType.OK:
            Args = [Widget, Channel]
            threading.Thread(None, self.DeleteThread, None, Args).start()

    def DeleteThread(self, Widget, Channel):
        LBRYSettings = json.loads(Preferences.Get())
        Session = LBRYSettings["Preferences"]["Session"]
        Data = Comments.Delete(
            Channel["Name"],
            Channel["Claim"],
            self.CommentServer,
            self.Row["Id"],
            Session["Server"],
        )
        Data = json.loads(Data)
        if "error" in Data:
            Popup.Error(Data["error"])
            return
        Args = [None, None, self.Row["Id"], None, 0, 3]
        threading.Thread(None, CommentDB.Change, None, Args).start()
        GLib.idle_add(self.DeleteUpdate, Widget)

    def DeleteUpdate(self, Widget):
        Expander = self.Box.get_parent().get_parent()
        RepliesFrame = Expander.get_parent()
        Comment = RepliesFrame.get_parent()
        self.Comment.destroy()
        if Expander.get_name() == "RepliesExpander":
            Children = len(self.Box.get_children())
            if Children != 0:
                Expander.set_label("Replies (" + str(Children) + ")")
            if Children == 0:
                RepliesFrame.set_no_show_all(True)
                RepliesFrame.hide()
        Comment.show_all()

    def React(self, Channel, Type, Remove):
        LBRYSettings = json.loads(Preferences.Get())
        Session = LBRYSettings["Preferences"]["Session"]
        Data = Comments.ReactionReact(
            Channel["Name"],
            Channel["Claim"],
            self.CommentServer,
            str(self.Row["Id"]),
            Type,
            Remove,
            Session["Server"],
        )
        Data = json.loads(Data)
        if "error" in Data:
            Popup.Error(Data["error"])
            return ""
        Data = Comments.ReactionList(
            Channel["Name"],
            Channel["Claim"],
            self.CommentServer,
            str(self.Row["Id"]),
            Session["Server"],
        )
        Data = json.loads(Data)
        if "error" in Data:
            Popup.Error(Data["error"])
            return ""
        return Data

    def on_Heart_button_press_event(self, Widget, Event=""):
        Remove = [Widget.get_active()]
        threading.Thread(None, self.HeartThread, None, Remove).start()

    def on_Parent_button_press_event(self, Widget, Event=""):
        Widget.get_parent().event(Event)

    def HeartThread(self, Remove):
        for Channel in self.ChannelList:
            if Channel["Url"] == self.PChannel:
                Data = self.React(Channel, "creator_like", Remove)
                if not isinstance(Data, str):
                    GLib.idle_add(self.LikeDislikeUpdate, Data)
                break

    def on_Dislike_button_press_event(self, Widget, Event=""):
        self.LikeDislike("dislike", Widget.get_active())

    def on_Like_button_press_event(self, Widget, Event=""):
        self.LikeDislike("like", Widget.get_active())

    def LikeDislike(self, Type, Active):
        Args = [
            json.loads(Identity.Get(self.Identifier["Pointer"])),
            Type,
            Active,
        ]
        if Args[0]["Url"] == "":
            Popup.Message(
                "You have to sign in, in Account->Sign In, to do that.",
            )
            return
        threading.Thread(None, self.LikeDislikeThread, None, Args).start()

    def LikeDislikeThread(self, Channel, Type, Remove):
        Data = self.React(Channel, Type, Remove)
        if not isinstance(Data, str):
            GLib.idle_add(self.LikeDislikeUpdate, Data)

    def LikeDislikeUpdate(self, Data):
        MyLike = Data["my_reactions"][self.Row["Id"]]["like"]
        MyDislike = Data["my_reactions"][self.Row["Id"]]["dislike"]
        OtherLike = Data["others_reactions"][self.Row["Id"]]["like"]
        OtherDislike = Data["others_reactions"][self.Row["Id"]]["dislike"]
        Heart = Data["my_reactions"][self.Row["Id"]]["creator_like"]
        Heart += Data["others_reactions"][self.Row["Id"]]["creator_like"]
        self.Like.set_active(MyLike == 1)
        self.Dislike.set_active(MyDislike == 1)
        self.LikeNumber.set_label(str(OtherLike + MyLike))
        self.DislikeNumber.set_label(str(OtherDislike + MyDislike))
        self.Heart.set_active(Heart == 1)

    def on_Reply_button_press_event(self, Widget, Event=""):
        if self.Edit.get_image() == self.EditImage:
            Visible = self.ReplyScrolled.get_visible()
            ToSet = [self.ReplyScrolled, self.Tip, self.PreviewBox.get_parent()]
            ToSet.extend([self.Post, self.TipLabel1, self.TipLabel2])
            for Widget in ToSet:
                Widget.set_visible(not Visible)
            self.ReplyScrolled.set_name(str(not Visible))

    def StopScroll(self, Widget, Event):
        Widget.emit_stop_by_name("scroll-event")

    def on_Save_button_press_event(self, Widget, Event=""):
        for Channel in self.ChannelList:
            if Channel["Url"] == self.Row["Url"]:
                TextBuffer = self.ReplyText.get_buffer()
                StartIter = TextBuffer.get_start_iter()
                EndIter = TextBuffer.get_end_iter()
                Text = TextBuffer.get_text(StartIter, EndIter, True)
                Args = [Text, Channel]
                threading.Thread(None, self.SaveThread, None, Args).start()
                break

    def SaveThread(self, Text, Channel):
        Session = self.Settings["Session"]
        SendText = repr(Text)[1:-1].replace('"', '\\"').replace("\\'", "'")
        Data = Comments.Update(
            Channel["Name"],
            Channel["Claim"],
            self.CommentServer,
            SendText,
            self.Row["Id"],
            Session["Server"],
        )
        Data = json.loads(Data)
        if "error" in Data:
            Popup.Error(Data["error"])
            return
        Args = [Text, None, self.Row["Id"], None, Data["timestamp"], 2]
        threading.Thread(None, CommentDB.Change, None, Args).start()
        self.Row["Text"] = Text
        self.Markdowns[0]["Markdowner"].Text = Text
        GLib.idle_add(self.SaveUpdate)

    def SaveUpdate(self):
        self.Markdowns[0]["Markdowner"].Fill()
        self.on_Edit_button_press_event(self.Edit)

    def on_Post_button_press_event(self, Widget, Event=""):
        TextBuffer = self.ReplyText.get_buffer()
        Start, End = TextBuffer.get_start_iter(), TextBuffer.get_end_iter()
        Text = TextBuffer.get_text(Start, End, False)
        if Text != "":
            Child = self.BottomStack.get_nth_page(1)
            Label = self.BottomStack.get_tab_label_text(Child)
            Tip = self.Tip.get_value()
            if Label != "💬":
                MinimumTip = float(Label.split("(")[1].split(" ")[0])
                if Tip < MinimumTip:
                    Popup.Message(
                        "You need to tip at least "
                        + Label.split("(")[1].split(")")[0]
                    )
                    return
            Args = [
                Text,
                json.loads(Identity.Get(self.Identifier["Pointer"])),
                Widget,
                Tip,
            ]
            if Tip != 0:
                Dialog = Gtk.MessageDialog(
                    CAPI.ToObject(Global.Window),
                    buttons=Gtk.ButtonsType.OK_CANCEL,
                )
                Message = "Are you sure you want to tip %s LBC" % str(Tip)
                Dialog.props.text = Message
                Response = Dialog.run()
                Dialog.destroy()
                if Response != Gtk.ResponseType.OK:
                    return
                else:
                    threading.Thread(None, self.TipThread, None, Args).start()
                    return
            threading.Thread(None, self.PostThread, None, Args).start()

    def TipThread(self, Text, Channel, Widget, Tip):
        Session = self.Settings["Session"]
        JsonData = json.loads(
            Support.Create(
                self.ClaimID,
                Tip,
                True,
                Channel["Name"],
                Channel["Claim"],
                Session["Server"],
            )
        )
        if "error" in JsonData:
            Popup.Error(JsonData["error"])
        else:
            self.PostThread(
                Text, Channel, Widget, Tip, JsonData["outputs"][0]["txid"]
            )

    def PostThread(self, Text, Channel, Widget, Tip=0, TipID=""):
        if self.Settings["PromoteLBRYGTK"]:
            Text += Promo
        # We need to print the raw form out for Comments.c
        SendText = repr(Text)[1:-1].replace('"', '\\"').replace("\\'", "'")
        Data = Comments.Post(
            Channel["Name"],
            Channel["Claim"],
            self.Row["Id"],
            self.ClaimID,
            self.CommentServer,
            SendText,
            TipID,
            self.Settings["Session"]["Server"],
        )
        Data = json.loads(Data)
        if "error" in Data:
            DataError = Data["error"]
            if "\n" in DataError:
                Popup.Error(DataError)
            else:
                Popup.Message(DataError)
        else:
            Args = [Text, self.ClaimID, Data["comment_id"], Channel["Claim"]]
            Args.extend([Data["timestamp"], 1])
            threading.Thread(None, CommentDB.Change, None, Args).start()
            GLib.idle_add(self.PostUpdate, Channel, Widget, Text, Data, Tip)

    def PostUpdate(self, Channel, Widget, Text, Data, Tip):
        self.ReplyText.get_buffer().set_text("")
        if self.Row["Id"] == "":
            Box = self.Box
        else:
            self.RepliesFrame.set_no_show_all(False)
            Box = self.RepliesBox
            self.RepliesExpander.set_label(
                "Replies (" + str(len(self.RepliesBox.get_children()) + 1) + ")"
            )
            self.RepliesFrame.show_all()
        Session, Amount, Number = self.Settings["Session"], 0, 0
        try:
            JsonData = json.loads(Url.Get([Channel["Url"]], Session["Server"]))[
                0
            ]
            Value = JsonData["meta"]["effective_amount"]
            Number = JsonData["meta"]["claims_in_channel"]
        except:
            pass
        Row = {
            "Replies": 0,
            "Support": Tip,
            "Channel": Channel["Name"],
            "Text": Text,
            "Id": Data["comment_id"],
            "Url": Channel["Url"],
            "Thumbnail": Channel["Thumbnail"],
            "TimeStamp": Timestamp.CurrentTime(),
            "Value": Value,
            "Number": Number,
            "Title": Channel["Title"],
        }
        self.SingleComment(
            self.ClaimID,
            Box,
            Row,
            [0, 0, False, False, False],
            self.CommentServer,
            False,
            self.Settings,
            self.ChannelList,
            self.PChannel,
            queue.Queue(),
        )
        self.on_ReplyText_key_release_event(self.ReplyText)
        if not self.HiddenUI:
            self.on_Reply_button_press_event(self.Reply)
        Box.show_all()

    def NoToggleBefore(self, Widget):
        Widget.set_name(str(Widget.get_active()))

    def NoToggleAfter(self, Widget):
        Widget.set_active(Widget.get_name() == "True")
