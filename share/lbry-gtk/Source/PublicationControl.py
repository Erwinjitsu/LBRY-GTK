################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2023 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import gi, threading, time, os, json, shutil

gi.require_version("Gtk", "3.0")
from gi.repository import GLib, Gdk, Gtk

from Source.Select import Select
from Source.PyGObjectCAPI import PyGObjectCAPI
from CRewrite import Collection, Content, Following, Global, Identity, Icons
from CRewrite import Execute, Places, Popup, Preferences, State, Support, Tag

CAPI = PyGObjectCAPI()
Types = [Gdk.BUTTON_PRIMARY, Gdk.BUTTON_MIDDLE]


class PublicationControl:
    def __init__(self, *args):
        (
            self.Tagger,
            self.Links,
            self.AddPage,
            self.TabPointer,
        ) = args
        Builder = CAPI.ToObject(Global.Builder)
        Builder.add_from_file(Places.GladeDir + "PublicationControl.glade")
        Builder.connect_signals(self)
        self.TypeDependent = Builder.get_object("TypeDependent")
        self.Regular = Builder.get_object("Regular")
        self.UnFollow = Builder.get_object("UnFollow")
        self.Download = Builder.get_object("Download")
        self.Play = Builder.get_object("Play")
        self.UnBlock = Builder.get_object("UnBlock")

        self.Buttons = self.TypeDependent.get_children()
        self.Buttons.extend(self.Regular.get_children())

        self.Selector = Select(self.Buttons, [], [], [], [])

    def GetBlock(self):
        LBRYSettings = json.loads(Preferences.Get())
        Block = self.Channel in LBRYSettings["Preferences"]["ChannelsNot"]
        GLib.idle_add(self.SetBlock, Block)

    def SetBlock(self, Block):
        if Block:
            self.UnBlock.set_label("Unblock")
        else:
            self.UnBlock.set_label("Block")

    def GetFollow(self):
        IfFollow = Following.IsFollowed(self.Channel, Places.ConfigDir)
        GLib.idle_add(self.SetFollow, IfFollow)

    def SetFollow(self, Follow):
        if Follow:
            self.UnFollow.set_label("Unfollow")
        else:
            self.UnFollow.set_label("Follow")

    def ShowPublicationControl(self, *args):
        self.Selector.Unselect(True)
        self.Data, self.Title = args
        self.ValueType = self.Data["value_type"]
        self.Url = self.Data["canonical_url"]
        self.ChannelShort = self.Data["short_url"]

        AddBuy = ""
        if "fee" in self.Data["value"]:
            AddBuy = "Buy/"
        self.Download.set_label(AddBuy + "Download")
        self.Play.set_label(AddBuy + "Play")

        try:
            self.StreamType = self.Data["value"]["stream_type"]
        except:
            self.StreamType = ""

        try:
            self.Channel = self.Data["signing_channel"]["permanent_url"]
        except:
            if self.Data["value_type"] == "channel":
                self.Channel = self.Data["permanent_url"]
            else:
                self.Channel = ""

        for Widget in self.TypeDependent.get_children():
            Show = self.ValueType in Widget.get_style_context().list_classes()
            Widget.set_visible(Show)
        threading.Thread(None, self.GetFollow).start()
        threading.Thread(None, self.GetBlock).start()

    def on_UnBlock_button_press_event(self, Widget, Event):
        if not Widget.get_realized():
            return
        threading.Thread(None, self.UnBlockHelper).start()

    def UnBlockHelper(self):
        LBRYSettings = json.loads(Preferences.Get())
        Block = self.Channel in LBRYSettings["Preferences"]["ChannelsNot"]
        Logic = self.UnBlock.get_label() != "Unblock"
        if Block and not Logic:
            LBRYSettings["Preferences"]["ChannelsNot"].remove(self.Channel)
        if not Block and Logic:
            LBRYSettings["Preferences"]["ChannelsNot"].append(self.Channel)
        Preferences.Set(Places.ConfigDir, json.dumps(LBRYSettings))
        GLib.idle_add(self.SetBlock, Logic)

    def on_UnFollow_button_press_event(self, Widget, Event):
        if not Widget.get_realized():
            return
        threading.Thread(None, self.UnFollowHelper).start()

    def UnFollowHelper(self):
        if self.UnFollow.get_label() == "Unfollow":
            if Following.Follow(self.Channel, Places.ConfigDir, False):
                GLib.idle_add(self.SetFollow, False)
        else:
            if Following.Follow(self.Channel, Places.ConfigDir, True):
                GLib.idle_add(self.SetFollow, True)

    def on_Channel_button_press_event(self, Widget, Event):
        if self.Channel == "":
            Popup.Message("This post does not have a channel.")
            return
        Args = ["", self.Channel]
        if Event.button == Types[0]:
            State.Import("Publication", Args, self.TabPointer)
        elif Event.button == Types[1]:
            # Had State.Export(self.GetPublication, Args)
            self.AddPage(".", "", ["Publication", Args])

    def Confirmation(self):
        Dialog = Gtk.MessageDialog(
            CAPI.ToObject(Global.Window), buttons=Gtk.ButtonsType.OK_CANCEL
        )
        Amount = self.Data["value"]["fee"]["amount"]
        Currency = self.Data["value"]["fee"]["currency"]
        Dialog.props.text = (
            "Are you sure you want to resolve this publication? (%s %s)"
            % (Amount, Currency)
        )
        Response = Dialog.run()
        Dialog.destroy()
        return Response == Gtk.ResponseType.OK

    def on_Play_button_press_event(self, Widget, Event):
        if not Widget.get_label().startswith("Buy") or self.Confirmation():
            threading.Thread(
                None, self.ResolveHelper, None, [Event.button]
            ).start()

    def on_Download_button_press_event(self, Widget, Event):
        if not Widget.get_realized():
            return
        if not Widget.get_label().startswith("Buy") or self.Confirmation():
            threading.Thread(None, self.ResolveHelper).start()

    def ResolveHelper(self, Button=False):
        LBRYSettings = json.loads(Preferences.Get())
        Directory = ""
        if Button and not LBRYSettings["settings"]["save_files"]:
            Directory = Places.TmpDir
        JsonData = Content.Get(
            self.Url,
            "",
            Directory,
            True,
            LBRYSettings["Preferences"]["Session"]["Server"],
        )
        JsonData = json.loads(JsonData)

        if "error" in JsonData:
            Popup.Error(JsonData["error"])
            return

        Path = JsonData["download_path"]

        if not Path:
            return

        Args = [
            Button,
            LBRYSettings["settings"]["save_files"],
            LBRYSettings["settings"]["download_dir"],
            JsonData["download_directory"],
            JsonData["download_path"],
            JsonData["stream_name"],
        ]

        threading.Thread(None, self.MoveThread, None, Args, daemon=True).start()

        if Button:
            Command = LBRYSettings["Preferences"]["PlayCommand"]
            Setting = self.StreamType.capitalize() + "Command"
            try:
                Command = LBRYSettings["Preferences"][Setting]
            except:
                pass
            if (
                self.StreamType == "document"
                and Command == "LBRY-GTK-Document"
                and Path.endswith(".md")
            ):
                GLib.idle_add(self.DocumentUpdate, Path, Button)
            else:
                if Command == "LBRY-GTK-Document":
                    Command = ""
                Execute.Open(Path, Command)

    def MoveThread(
        self, Button, SaveFiles, Location, DownloadDirectory, Path, Name
    ):
        if (not Button) or (SaveFiles and Location != DownloadDirectory):
            Counter, Size = 0, 0
            while True:
                time.sleep(0.1)
                try:
                    NewSize = os.path.getsize(Path)
                    if Size == NewSize:
                        Counter += 1
                    else:
                        Size = NewSize
                        Counter = 0
                except:
                    Counter = 0
                if Counter == 10:
                    break
            if Location[-1] != os.sep:
                Location += os.sep
            shutil.move(Path, Location + Name)

    def DocumentUpdate(self, Path, Button):
        Args = [self.Title, Path]
        if Button == Types[0]:
            State.Import("Document", Args, self.TabPointer)
        elif Button == Types[1]:
            self.AddPage(".", "", State.Export("Document", Args))

    def on_Content_button_press_event(self, Widget, Event):
        if self.ValueType == "collection":
            LBRYSettings = json.loads(Preferences.Get())
            NewTitle = "Collection: " + self.Title
            if "error" in LBRYSettings:
                Popup.Error(LBRYSettings["error"])
                return []
            ClaimIds, Page = [], 1
            while True:
                NewClaims = json.loads(
                    Collection.Resolve(
                        json.dumps({"url": self.Url}),
                        5,
                        Page,
                        LBRYSettings["Preferences"]["Session"]["Server"],
                    )
                )
                if "error" in NewClaims:
                    Popup.Error(NewClaims["error"])
                    break
                Page += 1
                for Claim in NewClaims:
                    ClaimIds.append(Claim["Claim"])
                if len(NewClaims) != 5:
                    break
            QueryOptions = {"claim_ids": ClaimIds}
        elif self.ValueType == "channel":
            NewTitle = "Publications: " + self.Title
            QueryOptions = {"channel": self.Channel}
        Terms = [NewTitle, None, None, json.dumps(QueryOptions)]
        if Event.button == Types[0]:
            State.Import("Advanced Search", Terms, self.TabPointer)
        elif Event.button == Types[1]:
            self.AddPage(".", "", State.Export("Advanced Search", Terms))

    def on_Related_button_press_event(self, Widget, Event):
        Search = self.Data["name"].replace("-", " ")
        if Search[0] == "@":
            Search = ""
        Tags = Tag.GetTags(self.Tagger["Pointer"])
        NewTitle = "Related: " + self.Title
        if Tags[0] == "I told you":
            Tags = []
        QueryOptions = {
            "text": Search,
            "any_tags": Tags,
            "order_by": [],
        }
        Terms = [NewTitle, None, None, json.dumps(QueryOptions)]
        if Event.button == Types[0]:
            State.Import("Advanced Search", Terms, self.TabPointer)
        elif Event.button == Types[1]:
            self.AddPage(".", "", State.Export("Advanced Search", Terms))

    def on_Link_button_press_event(self, Widget, Event):
        if not Widget.get_realized():
            return
        threading.Thread(None, self.LinkThread).start()

    def LinkThread(self):
        LBRYSettings = json.loads(Preferences.Get())
        LinkCommand = LBRYSettings["Preferences"]["LinkCommand"]
        LinkSite = LBRYSettings["Preferences"]["LinkSite"]
        Link = self.Links[LinkSite]
        Execute.Open(Link, LinkCommand)

    def on_Share_button_press_event(self, Widget, Event):
        if not Widget.get_realized():
            return
        threading.Thread(None, self.ShareThread).start()

    def ShareThread(self):
        LBRYSettings = json.loads(Preferences.Get())
        ShareSite = LBRYSettings["Preferences"]["ShareSite"]
        Link = self.Links[ShareSite]
        GLib.idle_add(self.ShareUpdate, Link)

    def ShareUpdate(self, Link):
        Gtk.Clipboard.set_text(
            Gtk.Clipboard.get(Gdk.SELECTION_CLIPBOARD), Link, -1
        )
        Message = "Publication link: %s copied to clipboard." % Link
        Popup.Message(Message)

    def on_RSS_button_press_event(self, Widget, Event):
        if not Widget.get_realized():
            return
        LBRYSettings = json.loads(Preferences.Get())
        LBRYGTKSettings = LBRYSettings["Preferences"]

        RSS = self.ChannelShort.replace("#", ":").split("lbry://", 1)[1]

        if LBRYGTKSettings["RSSSite"] == 0:
            Link = "https://odysee.com/$/rss/" + RSS
        elif LBRYGTKSettings["RSSSite"] == 1:
            Link = "https://lbry.bcow.xyz/" + RSS + "/rss"
        else:
            Link = "https://lbryfeed.melroy.org/channel/" + RSS

        Gtk.Clipboard.set_text(
            Gtk.Clipboard.get(Gdk.SELECTION_CLIPBOARD), Link, -1
        )

        Popup.Message("RSS link: %s copied to clipboard." % Link)

    def CreateInputWindow(self, Title):
        Adjustment = Gtk.Adjustment.new(0, 0, 1000000000000, 1, 10, 0)
        SpinButton = Gtk.SpinButton.new(Adjustment, 0, 8)
        Dialog = Gtk.MessageDialog(
            CAPI.ToObject(Global.Window),
            Gtk.DialogFlags.DESTROY_WITH_PARENT,
            buttons=Gtk.ButtonsType.OK_CANCEL,
        )
        Dialog.set_title(Title)
        ContentArea = Dialog.get_content_area()

        Identifier = Identity.Create(False)
        ContentArea.add(Gtk.Label.new("Channel"))
        ToAdd = CAPI.ToObject(Identifier["IdentitiesBox"])
        ToAdd.get_children()[1].hide()
        ContentArea.add(ToAdd)
        ContentArea.add(Gtk.Label.new("Amount"))
        Box = Gtk.Box.new(Gtk.Orientation.HORIZONTAL, 0)
        Box.add(SpinButton)
        Image = Gtk.Image.new_from_pixbuf(
            CAPI.ToObject(Icons.Get()["LBCLabel"])
        )
        Box.add(Image)
        ContentArea.add(Box)

        Dialog.show_all()
        Response = Dialog.run()

        ReturnValue = {"Value": 0}
        if Response == Gtk.ResponseType.OK:
            ReturnValue = {
                "Value": SpinButton.get_value(),
                "Channel": json.loads(Identity.Get(Identifier["Pointer"])),
            }

        Dialog.destroy()
        return ReturnValue

    def PayUp(self, Name, BoolSet):
        LBRYSettings = json.loads(Preferences.Get())
        Data = self.CreateInputWindow(Name)
        if Data["Value"] != 0:
            JsonData = json.loads(
                Support.Create(
                    self.Data["claim_id"],
                    Data["Value"],
                    BoolSet,
                    Data["Channel"]["Name"],
                    Data["Channel"]["Claim"],
                    LBRYSettings["Preferences"]["Session"]["Server"],
                )
            )
            if "error" in JsonData:
                Popup.Error(JsonData["error"])

    def on_TipBoost_button_press_event(self, Widget, Event):
        if Widget.get_realized():
            Name = Widget.get_label()
            self.PayUp(Name, Name == "Tip")
            return True

    def NoToggleBefore(self, Widget):
        Widget.set_name(str(Widget.get_active()))

    def NoToggleAfter(self, Widget):
        Widget.set_active(Widget.get_name() == "True")

    # Not Yet Implemented

    def on_Repost_button_press_event(self, Widget, Event):
        Popup.Message("Feature currently not implemented")
