################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2023 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import gi, ctypes

gi.require_version("Gtk", "3.0")


class PyGObjectFunctions(ctypes.Structure):
    _fields_ = [
        ("pygobject_register_class", ctypes.PYFUNCTYPE(ctypes.c_void_p)),
        ("pygobject_register_wrapper", ctypes.PYFUNCTYPE(ctypes.c_void_p)),
        ("pygobject_lookup_class", ctypes.PYFUNCTYPE(ctypes.c_void_p)),
        ("pygobject_new", ctypes.PYFUNCTYPE(ctypes.py_object, ctypes.c_void_p)),
    ]


class PyGObjectCAPI(object):
    def __init__(self):
        Address = self.AsVoidPointer(gi._gobject._PyGObject_API)
        self.Api = PyGObjectFunctions.from_address(Address)

    @classmethod
    def CapsuleName(CLS, Capsule):
        ctypes.pythonapi.PyCapsule_GetName.restype = ctypes.c_char_p
        ctypes.pythonapi.PyCapsule_GetName.argtypes = [ctypes.py_object]
        return ctypes.pythonapi.PyCapsule_GetName(Capsule)

    @classmethod
    def AsVoidPointer(CLS, Capsule):
        Name = CLS.CapsuleName(Capsule)
        ctypes.pythonapi.PyCapsule_GetPointer.restype = ctypes.c_void_p
        ctypes.pythonapi.PyCapsule_GetPointer.argtypes = [
            ctypes.py_object,
            ctypes.c_char_p,
        ]
        return ctypes.pythonapi.PyCapsule_GetPointer(Capsule, Name)

    def ToObject(self, Address):
        return self.Api.pygobject_new(Address)
