################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2023 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import gi, os, json, shutil, threading

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk, GLib, GdkPixbuf

from Source import SettingsUpdate
from Source.PyGObjectCAPI import PyGObjectCAPI
from CRewrite import Connect, Global, GTKExtra, Icons, Places, Popup
from CRewrite import Preferences, Replace, State, Status, Tray, Wallet

CAPI = PyGObjectCAPI()


class Startup:
    def __init__(
        self,
        Balance,
        Signing,
        Statuser,
        StatusCall,
    ):
        self.Balance, self.Signing = Balance, Signing
        self.Statuser = Statuser
        self.StatusCall = StatusCall

        # Remove temporary directory here
        shutil.rmtree(Places.TmpDir, ignore_errors=True)

        # TODO: Hey Mors, let's move this directory stuff to Places.c one day
        Dirs = [
            Places.CacheDir,
            Places.ConfigDir,
            Places.TmpDir,
            Places.StateDir,
        ]

        for Dir in Dirs:
            try:
                os.makedirs(Dir)
            except:
                pass
        threading.Thread(None, self.StartupHelper).start()

    def StartupHelper(self):
        self.StatusCall()

        LBRYSettings = json.loads(Preferences.Get())

        Session = LBRYSettings["Preferences"]["Session"]

        for Setting in ["Binary", "Server"]:
            if Session["New" + Setting] != "":
                Session[Setting] = Session["New" + Setting]

        GLib.idle_add(self.WindowSettings, Session)

        # TODO: Change so that Connect is polled every 100ms or so
        # and make sure user can change settings while lbrynet is
        # still starting up (e.g. Connect returns
        # preferences-can-be-changed state)
        Connected = False
        Attempt = 0
        while not Connected and Attempt < 5:
            if Session["Start"]:
                Connected = Connect.Create(
                    Session["Binary"],
                    Session["Timeout"],
                    Session["Server"],
                )
                Message = "LBRYNet could not be started.\n\nAttempting again.."
            else:
                Connected = Connect.CheckTimeout(
                    Session["Timeout"],
                    Session["Server"],
                )
                Message = "LBRYNet is not running."

            if not Connected and Attempt == 0:
                Popup.Message(Message)
            Attempt = Attempt + 1

        # Get settings again now since lbrynet is running
        LBRYSettings = json.loads(Preferences.Get())
        LBRYGTKSettings = LBRYSettings["Preferences"]
        Session = LBRYGTKSettings["Session"]

        # We will throw ourselves out here if error exists in preferences
        if "error" in LBRYGTKSettings:
            Popup.Error(LBRYGTKSettings["error"])
            return

        # Updates missing settings from defaults
        JsonData = Preferences.Update(
            Places.JsonDir + "DefaultSettings.json",
            json.dumps(LBRYGTKSettings),
        )
        LBRYGTKSettings = json.loads(JsonData)

        LBRYSettings["Preferences"] = LBRYGTKSettings
        Preferences.Set(Places.ConfigDir, json.dumps(LBRYSettings))

        GLib.idle_add(self.FinishStartup, Session, LBRYGTKSettings)

        Global.SetStart()

        GLib.idle_add(self.UpdateBalance, False)

        GLib.timeout_add(60 * 1000, self.UpdateBalance, True)
        GLib.timeout_add(100, self.IfDisplayed, LBRYGTKSettings)

    def IfDisplayed(self, LBRYGTKSettings):
        if CAPI.ToObject(self.Statuser["Status"]).get_name() == "Displayed":
            threading.Thread(
                None, self.LoadHome, None, [LBRYGTKSettings]
            ).start()
            return False
        return True

    def LoadHome(self, LBRYGTKSettings):
        # NOTE: Third argument is tab
        # In future use global TabsList from Tabs.c
        Data = json.loads(LBRYGTKSettings["HomeData"])
        if len(Data) == 4 and not isinstance(Data[3], str):
            Data[3] = json.dumps(Data[3])
        State.Import(
            LBRYGTKSettings["HomeFunction"],
            Data,
            0,
        )

    def FinishStartup(self, Session, LBRYGTKSettings):
        if LBRYGTKSettings["AuthToken"] == "":
            self.Signing.set_label("Sign In")
        Status.SetLBRYGTK(self.Statuser["Pointer"], True)

        # Enable Inbox
        SettingsUpdate.ChangeMeta(Session["EnableMetaService"])

    def WindowSettings(self, Session):
        Status.SetServer(self.Statuser["Pointer"], Session["Server"])

        # Get the pointer to window
        Window = CAPI.ToObject(Global.Window)

        # Minimize window
        if Session["Minimized"]:
            Window.iconify()
        else:
            Window.show_all()

        # Creating tray icon with its menu
        if Session["Tray"]:
            self.Trayer = Tray.Create(Session["TrayMinimized"])

        # Changing menu type
        SettingsUpdate.ChangeMenu(Session["MenuType"], Session["MenuIcon"])

        # Set size of window
        if Session["WindowSize"]:
            Window.resize(Session["WindowWidth"], Session["WindowHeight"])

    def UpdateBalance(self, ReturnValue):
        LBRYSettings = json.loads(Preferences.Get())
        Session = LBRYSettings["Preferences"]["Session"]
        GotBalance = Wallet.Balance(Session["Server"])
        GotBalance = json.loads(GotBalance)
        if isinstance(GotBalance, str):
            Popup.Error(GotBalance)
        else:
            Balance = float(GotBalance["total"])
            GTKExtra.NumberLabel(
                CAPI.AsVoidPointer(self.Balance.__gpointer__), Balance
            )
            self.Balance.set_tooltip_text(str(Balance))
            self.Balance.show_all()
        return ReturnValue
