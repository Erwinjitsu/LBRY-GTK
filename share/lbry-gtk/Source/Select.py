################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2023 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gdk

Types = [Gdk.BUTTON_PRIMARY, Gdk.BUTTON_MIDDLE]


class Select:
    def __init__(
        self, Widgets, Exceptions, Enters, Leaves, Activates, NoCheck=False
    ):
        self.Widgets, self.Exceptions, self.Enters = Widgets, Exceptions, Enters
        self.Leaves, self.Activates, self.NoCheck = Leaves, Activates, NoCheck
        self.CurrentItem, self.LastItem = -1, len(Widgets) - 1

    def Unselect(self, Reset=False):
        if Reset:
            self.CurrentItem, self.LastItem = -1, -1

        if self.LastItem != -1:
            if not self.Widgets[self.LastItem] in self.Exceptions:
                self.Widgets[self.LastItem].set_active(False)
            else:
                NewIndex = self.Exceptions.index(self.Widgets[self.LastItem])
                if self.Leaves[NewIndex] != "":
                    self.Leaves[NewIndex]()
        if Reset:
            self.LastItem = -1

    def SelectIndex(self, Index):
        if not self.Widgets[Index] in self.Exceptions:
            self.Widgets[Index].set_active(True)
        else:
            Function = self.Enters[self.Exceptions.index(self.Widgets[Index])]
            if Function != "":
                Function()

        self.LastItem = self.CurrentItem
        if self.LastItem == len(self.Widgets):
            self.LastItem = 0
        self.CurrentItem = Index

    def GetVisible(self, Forward, Restart=False):
        if Forward:
            List = range(len(self.Widgets))
        else:
            List = range(len(self.Widgets) - 1, -1, -1)
            if self.CurrentItem == -1:
                self.LastItem = 0
                self.CurrentItem = len(self.Widgets)
        for Index in List:
            Widget = self.Widgets[Index]
            if self.NoCheck or (Widget.get_realized() and Widget.is_visible()):
                New = Index
                if (Forward and (self.CurrentItem < Index or Restart)) or (
                    not Forward and (Index < self.CurrentItem or Restart)
                ):
                    break
        return New

    def GoDirection(self, Forward=True):
        New = self.GetVisible(Forward)
        if self.CurrentItem == New:
            New = self.GetVisible(Forward, True)
        self.SelectIndex(New)
        self.Unselect()

    def Backward(self):
        self.GoDirection(False)

    def Forward(self):
        self.GoDirection()

    def Activate(self, Button, CurrentItem=None):
        Item = self.CurrentItem
        if CurrentItem != None:
            Item = CurrentItem
        Widget = self.Widgets[Item]
        if Item != -1 and (
            self.NoCheck or (Widget.get_realized() and Widget.is_visible())
        ):
            if not Widget in self.Exceptions:
                Event = Gdk.Event.new(Gdk.EventType.BUTTON_PRESS)
                Event.button = Types[Button]
                Event.window = Widget.get_window()
                Widget.event(Event)
            else:
                Index = self.Exceptions.index(Widget)
                Function = self.Activates[Index]
                if Function != "":
                    try:
                        Function(Button)
                    except:
                        Function()
