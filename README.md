<h1 align="center">
    <img src="./share/icons/hicolor/scalable/apps/lbry-gtk.svg" width="20%" height="auto"> 
    <br>LBRY-GTK
</h1>

![](./Images/1.gif)


LBRY-GTK as the name suggests is a GTK client to the LBRY network. LBRY-GTK in its early stages was a fork of [FastLBRY-terminal](https://notabug.org/jyamihud/FastLBRY-terminal), however the code was modified a lot in order to become a full GTK GUI app. LBRY-GTK is also:
* A desktop app for doing a variety of things on the LBRY network
* Cross platform (Windows & Linux currently)
* Highly configurable
* Free and Open Source Software (FOSS)

## Installation:

### Linux:

#### Arch Linux (AUR):

##### Stable
```
git clone https://aur.archlinux.org/lbry-gtk.git
cd lbry-gtk
makepkg -i
```

##### Git
```
git clone https://aur.archlinux.org/lbry-gtk-git.git
cd lbry-gtk-git
makepkg -i
```
or use an AUR helper of your choice.

#### Debian GNU/Linux (MPR):

```
git clone https://mpr.makedeb.org/lbry-gtk.git
cd lbry-gtk
makedeb -i
```

or use an MPR helper of your choice.

#### Appimage:

Version can be found on the [Releases](https://codeberg.org/MorsMortium/LBRY-GTK/releases) page.

#### Build from source:

```
git clone https://codeberg.org/MorsMortium/LBRY-GTK
cd LBRY-GTK
make
sudo make install
```

### Windows:

Version can be found on the [Releases](https://codeberg.org/MorsMortium/LBRY-GTK/releases) page.

## Dependencies:

- Jansson
- Curl
- MD4C
- PCRE2
- GTK3 (gtk, glib, pango, harfbuzz, cairo, gdk-pixbuf, atk)
- Pygobject (while transitioning)
- ImageMagick
- LBRYNet
- SQLite3

## Contributing:

There are many features and bugfixes planned for LBRY-GTK. All of these are listed in [TODO.md](TODO.md). Any help in terms of development or suggestions of what can be added/fixed is very much appreciated. For contributing code, see [CONTRIBUTING.md](CONTRIBUTING.md).

## Other Links:

Questions and chat about the project: [Matrix](https://matrix.to/#/#LBRY-GTK:matrix.org)

Other great LBRY projects: [Awesome-LBRY](https://github.com/LBRYFoundation/Awesome-LBRY)

## List of features:

See [FEATURES.md](FEATURES.md)

## Screenshots:

![](./Images/0.png)
![](./Images/1.png)
![](./Images/2.png)
![](./Images/3.png)
![](./Images/4.png)
![](./Images/5.png)
![](./Images/6.png)
![](./Images/7.png)
![](./Images/8.png)
![](./Images/9.png)
![](./Images/10.png)
![](./Images/11.png)
