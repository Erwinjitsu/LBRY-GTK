/******************************************************************************\
* LBRY-GTK                                                                     *
* Copyright (C) 2021-2023 MorsMortium and Other Contributors                   *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU General Public License as published by         *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                *
* GNU General Public License for more details.                                 *
*                                                                              *
* You should have received a copy of the GNU General Public License            *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
\******************************************************************************/

// This file is responsible for managing the Tray widget (tray icon)

#include <Python.h>
#include <gtk/gtk.h>
#include <stdbool.h>
#include <sys/types.h>

#include "Header/GTKExtra.h"
#include "Header/Icons.h"
#include "Header/Global.h"
#include "Header/Places.h"
#include "Header/Tray.h"

TrayData *TrayCreate(bool TrayMinimized) {
	// This function is responsible for creating the Tray widget

	// Allocate memory for data, abort if NULL
	TrayData *Data = malloc(sizeof(TrayData));
	if (Data == NULL) {
		abort();
	}

	// Get glade file name, load it in builder
	char WidgetFile[strlen(PlacesGlade) + 15];
	sprintf(WidgetFile, "%sTray.glade", PlacesGlade);
	gtk_builder_add_from_file(GlobalBuilder, WidgetFile, NULL);

	// Get widgets used
	Data->Tray = (GtkWidget *) gtk_builder_get_object(GlobalBuilder, "Tray");
	Data->Menu = (GtkWidget *) gtk_builder_get_object(GlobalBuilder, "Menu");
	Data->ShowHide = (GtkWidget *) gtk_builder_get_object(GlobalBuilder,
			"ShowHide");

	// Set icon for Tray
	gtk_status_icon_set_from_pixbuf((GtkStatusIcon *) Data->Tray, IconsLogoBig);

	// Connect signals with data
	gtk_builder_connect_signals_full(GlobalBuilder, GTKExtraConnect, Data);

	// Hide window if it is set to start minimized to tray
	if (TrayMinimized) {
		gtk_menu_item_activate((GtkMenuItem *) Data->ShowHide);
	}

	return Data;
}

gboolean TrayOnShowHideActivate(G_GNUC_UNUSED GtkWidget *Widget,
	TrayData *Data) {
	// This function is responsible for showing or hiding the application window
	// depending on its current state

	if (gtk_widget_get_visible((GtkWidget *) GlobalWindow)) {
		// If it is currently shown, hide it and set label to Show
		gtk_menu_item_set_label((GtkMenuItem *) Data->ShowHide, "Show");
		gtk_widget_hide((GtkWidget *) GlobalWindow);
	} else {
		// If it is currently hidden, show it and set label to Hide
		gtk_menu_item_set_label((GtkMenuItem *) Data->ShowHide, "Hide");
		gtk_widget_show_all((GtkWidget *) GlobalWindow);
	}

	return FALSE;
}

gboolean TrayOnTrayPopupMenu(G_GNUC_UNUSED GtkWidget *Widget,
	G_GNUC_UNUSED guint Button, G_GNUC_UNUSED guint32 ActivateTime,
	TrayData *Data) {
	// This function is responsible for popping up the menu, when the tray icon
	// is clicked

	gtk_menu_popup_at_pointer((GtkMenu *) Data->Menu, NULL);

	return FALSE;
}

gboolean TrayOnQuitActivate(G_GNUC_UNUSED GtkWidget *Widget,
	G_GNUC_UNUSED TrayData *Data) {
	// This function is responsible for quitting the app, when the quit
	// menuitem is clicked

	gtk_widget_destroy((GtkWidget *) GlobalWindow);

	return FALSE;
}

// Everything under this line is removable after full C conversion
static PyObject *TrayCreatePython(PyObject *Py_UNUSED(Self), PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Fill with sent data
	long TrayMinimized;
	if (!PyArg_ParseTuple(Args, "l", &TrayMinimized)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	TrayData *Data = TrayCreate((bool) TrayMinimized);

	// Acquire lock
	PyEval_RestoreThread(Save);

	PyObject *Dict = PyDict_New();

	PyObject *Object = PyLong_FromVoidPtr((void *) Data->Tray);
	PyDict_SetItemString(Dict, "Tray", Object);
	Py_DECREF(Object);

	Object = PyLong_FromVoidPtr((void *) Data);
	PyDict_SetItemString(Dict, "Pointer", Object);
	Py_DECREF(Object);

	return Dict;
}

// Function names and other python data
static PyMethodDef TrayMethods[] = {
	{"Create", TrayCreatePython, METH_VARARGS, ""},
	{NULL, NULL, 0, NULL}
};

static struct PyModuleDef TrayModule = {
	PyModuleDef_HEAD_INIT, "Tray", "Tray module", -1, TrayMethods, 0, 0, 0, 0
};

// The function name has to end with the module name
PyMODINIT_FUNC PyInit_Tray(void) {
	return PyModule_Create(&TrayModule);
}
