/******************************************************************************\
* LBRY-GTK                                                                     *
* Copyright (C) 2021-2023 MorsMortium and Other Contributors                   *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU General Public License as published by         *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                *
* GNU General Public License for more details.                                 *
*                                                                              *
* You should have received a copy of the GNU General Public License            *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
\******************************************************************************/

// This file is responsible for starting and stopping lbrynet and for checking
// its status

#include <curl/curl.h>
#include <Python.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <jansson.h>

#include "Header/Global.h"
#include "Header/Execute.h"
#include "Header/Request.h"
#include "Header/Connect.h"

// Variables for async stopping of Start, in case of early exit
pthread_mutex_t ConnectLock;
bool ConnectExitStart = false;

size_t ConnectDiscard(void *UNUSED(Text), size_t UNUSED(
		One), size_t Size, void *UNUSED(FullText)) {
	// This function is responsible for discarding libcurl buffer text

	// Return Size signaling no error
	return Size;
}

json_t *ConnectStatus(char *Server) {
	// This function is responsible for getting the status of lbrynet and
	// and returning it as a json object

	json_error_t Error;

	// Get level 1 of status, if unsuccessful, clean up json object then
	// return default status
	json_t *StatusObject = RequestJson(Server, "{\"method\" : \"status\"}");
	if (json_object_get(StatusObject, "error") != NULL) {
		json_decref(StatusObject);
		return json_loads("{\"connection\" : false}", 0, &Error);
	}

	// Get level 2 of status, if unsuccessful, clean up json object then
	// return default status
	json_t *StartupStatus = json_object_get(StatusObject, "startup_status");
	if (StartupStatus == NULL) {
		json_decref(StatusObject);
		return json_loads("{\"connection\" : false}", 0, &Error);
	}

	// Make status object self sufficient, add Connection as true
	json_incref(StartupStatus);
	json_object_set_new(StartupStatus, "connection", json_true());

	// Clean up root json object and return real status
	json_decref(StatusObject);
	return StartupStatus;
}

void ConnectCreateExit(void) {
	// This function is responsible for stopping Start in an async manner

	// Lock mutex guarding ExitStart, set its value, then unlock mutex
	pthread_mutex_lock(&ConnectLock);
	ConnectExitStart = true;
	pthread_mutex_unlock(&ConnectLock);
}

bool ConnectStart(void) {
	// This function is responsible for initiating everything used by the file

	// Initiating curl
	curl_global_init(CURL_GLOBAL_ALL);

	// Initiating mutex guarding ExitStart
	return pthread_mutex_init(&ConnectLock, NULL) == 0;
}

void ConnectStop(void) {
	// This function is responsible for deleting everything used by the file

	// Clean up curl
	curl_global_cleanup();

	// Destroying mutex guarding ExitStart
	pthread_mutex_destroy(&ConnectLock);
}

bool ConnectCheck(char *Server) {
	// This function is responsible for checking the status of lbrynet

	// Get status data
	json_t *StatusObject = ConnectStatus(Server);

	// Counters for all and started parts of lbrynet
	int Started = 0, Keys = 0;

	// Helpers for foreach and equality check
	const char *Key;
	json_t *Value, *True = json_true();

	// Go through every key of object
	json_object_foreach(StatusObject, Key, Value) {
		// If value at key is true, increase Started
		if (json_equal(Value, True)) {
			++Started;
		}

		// Increase Keys, counting every key
		++Keys;
	}

	// Clean up status, return if lbrynet started or not
	json_decref(StatusObject);
	return Started == Keys;
}

bool ConnectCheckTimeout(float Timeout, char *Server) {
	// This function is responsible for returning whether lbrynet started, after
	// checking it once per second, for Timeout

	// Loop until Timeout is up
	for (float Slept = 0; Slept < Timeout; ++Slept) {
		// If lbrynet started exit loop
		if (ConnectCheck(Server)) {
			break;
		}

		// Lock mutex guarding ExitStart, get its value, then unlock mutex
		pthread_mutex_lock(&ConnectLock);
		bool Exit = ConnectExitStart;
		pthread_mutex_unlock(&ConnectLock);

		// If app requested exit, exit loop
		if (Exit) {
			break;
		}

		// Sleep for a second
		sleep(1);
	}

	// Return whether lbrynet started
	return ConnectCheck(Server);
}

bool ConnectCreate(char *Binary, float Timeout, char *Server) {
	// This function is responsible for starting lbrynet and returning whether
	// it started

	// If lbrynet runs return true, otherwise open it
	if (ConnectCheck(Server)) {
		return true;
	} else {
		ExecuteFile(NULL, Binary);
	}

	return ConnectCheckTimeout(Timeout, Server);
}

bool ConnectDestroy(char *Server) {
	// This function is responsible for stopping lbrynet, and returning whether
	// it ran prior to stopping it

	// Get whether lbrynet runs
	bool Started = ConnectCheck(Server);

	// Get curl object
	CURL *Curl = curl_easy_init();

	if (Curl) {
		// Set url, sent json and data writing function for request
		curl_easy_setopt(Curl, CURLOPT_URL, Server);
		curl_easy_setopt(Curl, CURLOPT_POSTFIELDS, "{\"method\" : \"stop\"}");
		curl_easy_setopt(Curl, CURLOPT_WRITEFUNCTION, ConnectDiscard);

		// Perform request and clean up curl object
		curl_easy_perform(Curl);
		curl_easy_cleanup(Curl);
	}

	// Return whether lbrynet ran before stopping it
	return Started;
}

// Everything under this line is removable after full C conversion
static PyObject *ConnectCheckPython(PyObject *Py_UNUSED(Self), PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Get arguments and pass it to the pythonless function
	char *Server = NULL;
	if (!PyArg_ParseTuple(Args, "s", &Server)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	bool Return = ConnectCheck(Server);

	// Acquire lock
	PyEval_RestoreThread(Save);

	return PyBool_FromLong(Return);
}

static PyObject *ConnectStatusPython(PyObject *Py_UNUSED(Self),
	PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Get arguments and pass it to the pythonless function
	char *Server = NULL;
	if (!PyArg_ParseTuple(Args, "s", &Server)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	json_t *StatusObject = ConnectStatus(Server);
	char *StatusString = json_dumps(StatusObject, 0);
	json_decref(StatusObject);

	// Acquire lock
	PyEval_RestoreThread(Save);

	PyObject *NewStatus = PyUnicode_FromString(StatusString);
	free(StatusString);

	return NewStatus;
}

static PyObject *ConnectDestroyPython(PyObject *Py_UNUSED(Self),
	PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Get arguments and pass it to the pythonless function
	char *Server = NULL;
	if (!PyArg_ParseTuple(Args, "s", &Server)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	bool Return = ConnectDestroy(Server);

	// Acquire lock
	PyEval_RestoreThread(Save);

	return PyBool_FromLong(Return);
}

static PyObject *ConnectCreatePython(PyObject *Py_UNUSED(Self),
	PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Get arguments and pass it to the pythonless function
	char *Server = NULL, *Binary;
	float Timeout;
	if (!PyArg_ParseTuple(Args, "sfs", &Binary, &Timeout, &Server)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	bool Return = ConnectCreate(Binary, Timeout, Server);

	// Acquire lock
	PyEval_RestoreThread(Save);

	return PyBool_FromLong(Return);
}

static PyObject *ConnectCreateExitPython(PyObject *Py_UNUSED(
		Self), PyObject *Py_UNUSED(Args)) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	ConnectCreateExit();

	// Acquire lock
	PyEval_RestoreThread(Save);

	return PyLong_FromLong(0);
}

static PyObject *ConnectCheckTimeoutPython(PyObject *Py_UNUSED(
		Self), PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Get arguments and pass it to the pythonless function
	char *Server = NULL;
	float Timeout;
	if (!PyArg_ParseTuple(Args, "fs", &Timeout, &Server)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	bool Return = ConnectCheckTimeout(Timeout, Server);

	// Acquire lock
	PyEval_RestoreThread(Save);

	return PyBool_FromLong(Return);
}

// Function names and other python data
static PyMethodDef ConnectMethods[] = {
	{"Status", ConnectStatusPython, METH_VARARGS, "Status"},
	{"Check", ConnectCheckPython, METH_VARARGS, "Check"},
	{"Destroy", ConnectDestroyPython, METH_VARARGS, "Destroy"},
	{"Create", ConnectCreatePython, METH_VARARGS, "Create"},
	{"CreateExit", ConnectCreateExitPython, METH_VARARGS, "CreateExit"},
	{"CheckTimeout", ConnectCheckTimeoutPython, METH_VARARGS, "CheckTimeout"},
	{NULL, NULL, 0, NULL}
};

static struct PyModuleDef ConnectModule = {
	PyModuleDef_HEAD_INIT, "Connect", "Connect module", -1, ConnectMethods, 0,
	0, 0, 0
};

// The function name has to end with the module name
PyMODINIT_FUNC PyInit_Connect(void) {
	return PyModule_Create(&ConnectModule);
}
