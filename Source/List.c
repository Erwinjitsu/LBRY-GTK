/******************************************************************************\
* LBRY-GTK                                                                     *
* Copyright (C) 2021-2023 MorsMortium and Other Contributors                   *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU General Public License as published by         *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                *
* GNU General Public License for more details.                                 *
*                                                                              *
* You should have received a copy of the GNU General Public License            *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
\******************************************************************************/

// List box widgets in the main list page in grid or list view

#include <Python.h>
#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <pthread.h>
#include <jansson.h>

#include "Header/Box.h"
#include "Header/Content.h"
#include "Header/DateTime.h"
#include "Header/File.h"
#include "Header/Global.h"
#include "Header/Image.h"
#include "Header/Json.h"
#include "Header/ListUtil.h"
#include "Header/NotificationDB.h"
#include "Header/Order.h"
#include "Header/Popup.h"
#include "Header/Replace.h"
#include "Header/Tabs.h"
#include "Header/Tag.h"
#include "Header/List.h"

const char *ListClaimTypes[] = { "stream", "channel", "repost", "collection" };
const char *ListTagNames[] =
{ "any_tags", "all_tags", "not_tags", "claim_ids", "channel_ids",
  "not_channel_ids", "any_languages", "all_languages", "not_languages",
  "stream_types" };

json_t *(*ListFunctions[])() =
{ ContentSearch, ListUtilWallet, FileList, NotificationDBList };

void ListCreate(GtkWidget *NoContentNotice, GtkWidget **WalletSpaceParts,
	GtkWidget *MainSpace, GtkWidget *Grid, GtkWidget *Advanced,
	GtkContainer *Spaces[], int SpacesSize, GtkEntry *Text, GtkEntry *Channel,
	GtkLabel *Title, GtkComboBox *ClaimType, GtkComboBox *DateType,
	GtkComboBox *Inequality, TagData *TagList[], int TagsSize,
	DateTimeData *DateTimer, OrderData *Orderer, PyObject *AddPage,
	TabsData *TabData) {
	// Creates list data

	// Get the list data struct
	ListData *Data;
	TabsGet(TabData, Lister, Data);

	// Data to struct

	// FunctionData is not owned by List.c
	// Remember to allocate outside List.c
	Data->FunctionData[0] = NULL;
	Data->FunctionData[1] = WalletSpaceParts;
	Data->FunctionData[2] = NULL;
	Data->FunctionData[3] = NULL;

	Data->NoContentNotice = NoContentNotice;
	Data->Json = NULL;
	Data->MainSpace = MainSpace;
	Data->ClaimCount = 0;
	Data->ThreadCount = 0;
	Data->LastPlaced = 5;
	Data->Grid = Grid;
	Data->Advanced = Advanced;
	Data->TextLabel = Text;
	Data->TitleLabel = Title;
	Data->ChannelLabel = Channel;
	Data->ClaimType = ClaimType;
	Data->DateType = DateType;
	Data->Inequality = Inequality;
	Data->DateTime = DateTimer;
	Data->Order = Orderer;
	Data->AddPage = AddPage;

	// Allocate memory for all tags and NULL
	Data->TagList = malloc((TagsSize + 1) * sizeof(TagData *));
	int Index;
	for (Index = 0; Index < TagsSize; Index++) {
		Data->TagList[Index] = TagList[Index];
	}
	Data->TagList[Index] = NULL;

	Data->Spaces = malloc(SpacesSize * sizeof(GtkContainer *));
	for (Index = 0; Index < SpacesSize; Index++) {
		Data->Spaces[Index] = Spaces[Index];
	}

	// Allocate initial space for claims
	Data->Claims = malloc(8);

	// Thread lock for threaded tasks
	pthread_mutex_init(&Data->Lock, NULL);
}

void ListFree(ListData *Data) {
	// Free all of data allocated by ListCreate

	// TODO: WalletSpaceParts could also be free'd by some
	// other function since it is allocated outside of List.c
	free(Data->FunctionData[1]);
	free(Data->TagList);
	free(Data->Claims);
	free(Data->Spaces);
}

void ListGetContent(ListData *Data) {
	// Gets more content if needed

	int ContentNumber = 1;

	if (Data->Box) {
		// List or grid view
		int (*Function)(GtkWidget *, GtkWidget *) =
			(Data->ListDisplay) ? ListUtilGridContent : ListUtilListContent;

		// Get the number of content to load per page
		ContentNumber = Function(Data->MainSpace, Data->Box->Box);
		ContentNumber /=
			(int) json_real_value(json_object_get(GlobalPreferences,
				"ContentPerLoading"));
		ContentNumber++;
	}

	// Add content number to count of loaded content
	if (Data->LoadCount < ContentNumber) {
		Data->LoadCount = ContentNumber;
	}
}

static int ListPlaceData(ListData *Lister, json_t *RowData, bool NoErrorHad,
	TabsData *TabData) {
	// Place some Data

	// If contains no data, return back to caller
	// In case when this is the first loaded page, tell user there's no content
	if (json_is_array(RowData) && json_array_size(RowData) == 0) {
		if (Lister->ListPage == 0 &&
			gtk_widget_get_parent(Lister->NoContentNotice) == NULL) {
			gtk_container_add(Lister->Spaces[Lister->ListName],
				Lister->NoContentNotice);
			gtk_widget_show_all(Lister->MainSpace);
		}
		return 0;
	}

	// Check for error in json object
	json_t *Error = json_object_get(RowData, "error");
	if (Error != NULL) {
		if (NoErrorHad) {
			PopupError(JsonString(Error, "Something went wrong"));
		}
		return -1;
	}

	// Set the grid ready for space
	if (Lister->ListPage == 0) {
		gtk_container_add(Lister->Spaces[Lister->ListName], Lister->Grid);
	}

	// Loop over each item in the array data and create boxes from that
	bool KillThread;
	size_t Index;
	json_t *Row;
	json_array_foreach(RowData, Index, Row) {
		// Check if the killthread is set
		pthread_mutex_lock(&Lister->Lock);
		KillThread = Lister->KillThread;
		pthread_mutex_unlock(&Lister->Lock);

		// Stops boxes from being loaded
		if (KillThread) {
			return -2;
		}

		// Checks if already exists on the page
		char *Claim = JsonString(json_object_get(Row, "Claim"), "");
		if (Lister->ListName == ListWalletName &&
			Lister->ListButton == ListInboxButton) {
			bool Found = false;
			for (int ID = 0; ID < Lister->ClaimCount; ID++) {
				if (strcmp(Lister->Claims[ID], Claim) == 0) {
					Found = true;
					break;
				}
			}
			if (Found) {
				continue;
			}
		}

		// More space for our claim array
		char **Temporary = realloc(Lister->Claims,
				(Lister->ClaimCount + 1) * sizeof(char *));
		if (Temporary == NULL) {
			return -2;
		}
		Lister->Claims = Temporary;
		Lister->ClaimCount++;

		// New claim for array
		char *NewClaim = malloc(strlen(Claim) + 1);
		if (NewClaim == NULL) {
			return -2;
		}
		strcpy(NewClaim, Claim);

		// Place claim to array
		Lister->Claims[Lister->ClaimCount - 1] = NewClaim;

		// Pass data to box
		BoxData *Box = BoxCreate(Lister->ListDisplay != 0,
				Row, Lister->ListName, Lister->AddPage, Lister->Grid,
				JsonInt(json_object_get(GlobalPreferences,
				"ThumbnailWidth"), 10),
				JsonInt(json_object_get(GlobalPreferences,
				"GridTitleRows"), 10),
				JsonInt(json_object_get(GlobalPreferences, "BoxPadding"), 10),
				JsonInt(json_object_get(GlobalPreferences, "ThumbnailRounding"),
				10),
				JsonInt(json_object_get(GlobalPreferences,
				"PublicationProfileCircling"), 10),
				JsonInt(json_object_get(GlobalPreferences, "ListChannelWidth"),
				10),
				JsonInt(json_object_get(GlobalPreferences,
				"PublicationProfile"), 10),
				JsonInt(json_object_get(GlobalPreferences,
				"PublicationProfileSize"), 10), TabData);

		gtk_widget_show_all(Lister->MainSpace);

		Lister->Box = Box;
	}
	ListGetContent(Lister);

	return 1;
}

gboolean ListPlaceDataHelper(gpointer Args) {
	// Helper to sort out data before actual call to ListPlaceData

	struct ListWrapper *Wrapper = (struct ListWrapper *) Args;

	// Get reference to ListData struct
	ListData *Lister;
	TabsGet(Wrapper->TabData, Lister, Lister);

	int PlacedReturn = ListPlaceData(Lister, Wrapper->RowData,
			Wrapper->NoErrorHad, Wrapper->TabData);

	// Save the response back to tab memory
	pthread_mutex_lock(&Lister->Lock);
	Lister->LastPlaced = PlacedReturn;
	pthread_mutex_unlock(&Lister->Lock);

	return G_SOURCE_REMOVE;
}

char *ListServer[] = { "Session", "Server", NULL };
void *ListThread(void *Args) {
	// Update the mainspace (in a thread)

	TabsData *TabData = (TabsData *) Args;
	ListData *Lister;
	TabsGet(TabData, Lister, Lister);

	// 10ms timer for nanosleep
	struct timespec Remaining, Request = { 0, 10000000 };

	// Only continue execution once thread count has reached 0
	// to prevent any data being placed in wrong view
	int ThreadCount;
	do{
		nanosleep(&Request, &Remaining);

		pthread_mutex_lock(&Lister->Lock);
		ThreadCount = Lister->ThreadCount;
		pthread_mutex_unlock(&Lister->Lock);
	} while (ThreadCount != 0);

	// Get the function we need for execution
	void *FunctionData = Lister->FunctionData[Lister->ListButton];
	json_t *(*Function)(void *, int, int,
		char *) = ListFunctions[Lister->ListButton];

	// Increase reference to json object for better thread safety
	if (Lister->ListButton == ListSearchButton) {
		json_incref(FunctionData);
	}

	// Increase thread count to signal about running threads
	Lister->ThreadCount++;

	char *Server = JsonStringObject(GlobalPreferences, ListServer,
			"http://localhost:5959");
	int PlacedReturn = 1;
	bool KillThread = false;

	// Take the max value here
	int MaxValue =
		(int) json_number_value(json_object_get(GlobalPreferences,
			"ContentPerLoading"));
	if (MaxValue < 1) {
		MaxValue = 1;
	}

	// Make wrapper object for some data
	struct ListWrapper *Wrapper = malloc(sizeof(struct ListWrapper));
	Wrapper->TabData = TabData;

	while (Lister->ListPage < Lister->LoadCount && PlacedReturn == 1) {
		bool NoErrorHad = true;

		json_t *RowData;
		do{
			// Fetch row data
			RowData = Function(FunctionData,
					MaxValue, Lister->ListPage + 1, Server);

			// RowData to wrapper and send it forward
			Wrapper->RowData = RowData;
			Wrapper->NoErrorHad = NoErrorHad;
			g_idle_add(ListPlaceDataHelper, Wrapper);

			// Wait for response
			do{
				nanosleep(&Request, &Remaining);

				pthread_mutex_lock(&Lister->Lock);
				PlacedReturn = Lister->LastPlaced;
				KillThread = Lister->KillThread;
				pthread_mutex_unlock(&Lister->Lock);
			} while (PlacedReturn == 5);

			// Back to lister so we can wait for response again
			Lister->LastPlaced = 5;

			json_decref(RowData);

			if (PlacedReturn == -1) {
				if (NoErrorHad) {
					PopupMessage("LBRYNet is experiencing a "
						"problem, the client is retrying to "
						"load the data.");
					NoErrorHad = false;
				}

				// Wait for a second to retry again
				Request.tv_sec = 1;
				nanosleep(&Request, &Remaining);
				Request.tv_sec = 0;
			}
		} while (PlacedReturn == -1 && KillThread == false);

		// Increase page count
		if (PlacedReturn == 1) {
			Lister->ListPage++;
		}
	}
	free(Wrapper);

	if (Lister->ListButton == ListSearchButton) {
		json_decref(FunctionData);
	}

	// Bring down the count so we know there
	// are no more threads
	pthread_mutex_lock(&Lister->Lock);
	Lister->ThreadCount--;
	Lister->Started = true;
	pthread_mutex_unlock(&Lister->Lock);

	return NULL;
}

static void ListUpdateMainSpace(TabsData *TabData, int Increase) {
	// Update the mainspace

	// Increase to list data struct
	ListData *Lister;
	TabsGet(TabData, Lister, Lister);
	Lister->LoadCount += Increase;
	Lister->Started = false;

	pthread_t Thread;
	pthread_create(&Thread, NULL, ListThread, TabData);
	pthread_detach(Thread);
}

gboolean ListGenericLoaded(gpointer Args) {
	// Generic loading

	TabsData *TabData = (TabsData *) Args;

	ListData *Lister;
	TabsGet(TabData, Lister, Lister);

	// Get if we need content
	ListGetContent(&TabData->Lister);

	if (Lister->ListPage < Lister->LoadCount) {
		ListUpdateMainSpace(TabData, 0);
	}

	return FALSE;
}

static void ListGetSpaceName(int ListName, char Buffer[]) {
	// Return list name as string for replacer

	if (ListName == ListWalletName) {
		strcpy(Buffer, "WalletList");
	} else if (ListName == ListContentName) {
		strcpy(Buffer, "ContentList");
	} else if (ListName == ListFileName) {
		strcpy(Buffer, "FileList");
	} else if (ListName == ListNotificationName) {
		strcpy(Buffer, "NotificationList");
	}
}

void ListGridRemove(GtkWidget *Widget, G_GNUC_UNUSED gpointer Unused) {
	// Destroy item in grid

	gtk_widget_destroy(Widget);
}

const char *ListEquality[] = { ">=", "<=", "=", ">", "<" };
const char *ListNoConvert[] = { "not_tags", "not_channel_ids" };
const char *ListNoConvertSane[] = { "TagsNot", "ChannelsNot" };
gboolean ListButtonUpdate(gpointer Args) {
	// Updates all the widgets in the page

	TabsData *TabData = (TabsData *) Args;
	ListData *Lister;
	TabsGet(TabData, Lister, Lister);

	// First empty any old boxes found in list/grid
	gtk_container_foreach((GtkContainer *) Lister->Grid,
		(GtkCallback) ListGridRemove, NULL);

	// Set the max children displayed per line (1000 or 1)
	gtk_flow_box_set_max_children_per_line((GtkFlowBox *) Lister->Grid,
		Lister->ListDisplay * 999 + 1);

	// Unexpand Advanced so user can see the page on new search/page change
	gtk_expander_set_expanded((GtkExpander *) Lister->Advanced, FALSE);

	OrderSet(Lister->Order,
		JsonString(json_object_get(GlobalPreferences, "Order"), ""));

	// Text to empty for labels
	gtk_label_set_text(Lister->TitleLabel, "");
	gtk_entry_set_text(Lister->TextLabel, "");
	gtk_entry_set_text(Lister->ChannelLabel, "");

	// Set the first option for all combo boxes
	gtk_combo_box_set_active(Lister->ClaimType, 0);
	gtk_combo_box_set_active(Lister->Inequality, 0);
	gtk_combo_box_set_active(Lister->DateType, 0);

	// Remove all tags present
	for (int Index = 0; Lister->TagList[Index] != NULL; Index++) {
		TagRemoveAll(Lister->TagList[Index]);
	}

	char *Time = (char *)
		json_string_value(json_object_get(Lister->Json, "timestamp"));
	if (Time != NULL && Time[0] != '-') {
		int Length, Equal = 2;
		for (int Index = 0; Index < 5; Index++) {
			Length = (Index < 2) ? 3 : 1;
			if (strncmp(Time, ListEquality[Index], Length) != 0) {
				Equal = 4 - Index;
				Time += Length;
				break;
			}
		}

		// Set correct items active
		gtk_combo_box_set_active(Lister->DateType, 2);
		gtk_combo_box_set_active(Lister->Inequality, Equal);

		// Get timestamp and set time to DateTime widget
		time_t Timestamp = atoi(Time);
		struct tm *GotTime = localtime(&Timestamp);
		DateTimeSet(Lister->DateTime, GotTime);

		gtk_toggle_button_set_active(
			(GtkToggleButton *) Lister->DateTime->DateUse, TRUE);
	} else {
		// Trigger DateTime reset button event
		DateTimeOnDateResetButtonPressEvent(NULL, NULL, Lister->DateTime);

		gtk_toggle_button_set_active(
			(GtkToggleButton *) Lister->DateTime->DateUse, FALSE);
	}

	// Make changes applicable to content view
	if (Lister->ListName == ListContentName) {
		// Check if order_by exists and update orderer if so
		json_t *DataOrder = json_object_get(Lister->Json, "order_by");
		if (DataOrder) {
			if (json_array_size(DataOrder)) {
				OrderSet(Lister->Order,
					JsonString(json_array_get(DataOrder, 0), ""));
			} else {
				OrderSet(Lister->Order, "");
			}
		} else {
			// Get currently set order from client
			char *Returned;
			OrderGet(Lister->Order, &Returned);

			// Add order_by to json data
			if (Returned) {
				json_t *NewArray = json_array();
				json_array_append_new(NewArray, json_string(Returned));

				json_object_set_new(Lister->Json, "order_by", NewArray);
				free(Returned);
			}
		}

		// Update json data with tags from GlobalPreferences
		const char *NoConvert;
		json_t *NoConvertItem;
		for (int Index = 0; Index < 2; Index++) {
			NoConvert = ListNoConvert[Index];
			NoConvertItem = json_object_get(GlobalPreferences,
					ListNoConvertSane[Index]);

			// If there are no tags present for specified key then
			// create an empty one
			if (json_object_get(Lister->Json, NoConvert) == NULL) {
				json_object_set_new(Lister->Json, NoConvert, json_array());
			}

			if (json_array_size(NoConvertItem)) {
				json_array_extend(json_object_get(Lister->Json,
					NoConvert), NoConvertItem);
			}
		}

		// Also add mature filter to tags if "child filter" is set on
		if (json_is_false(json_object_get(GlobalPreferences,
			"ShowMature"))) {
			json_array_append_new(json_object_get(Lister->Json, "not_tags"),
				json_string("mature"));
		}
	}

	// Few values from json data
	json_t *Item = json_object_get(Lister->Json, "text");
	if (Item) {
		gtk_entry_set_text(Lister->TextLabel, json_string_value(Item));
	}

	Item = json_object_get(Lister->Json, "channel");
	if (Item) {
		gtk_entry_set_text(Lister->ChannelLabel, json_string_value(Item));
	}

	Item = json_object_get(Lister->Json, "claim_type");
	if (Item) {
		const char *Compare = json_string_value(Item);
		for (int Index = 1; Index <= 4; Index++) {
			if (strcmp(Compare, ListClaimTypes[Index]) == 0) {
				gtk_combo_box_set_active(Lister->ClaimType, Index);
				break;
			}
		}
	}

	// Check tags and stuff
	const char *TagName;
	size_t ItemIndex;
	for (int Index = 0; Lister->TagList[Index] != NULL; Index++) {
		TagName = ListTagNames[Index];

		if (json_object_get(Lister->Json, TagName) == NULL) {
			continue;
		}

		json_t *Tags = json_array();

		// Check each of the NoConverts in array and add tags based on that
		// to the Tags json array
		for (int NotIndex = 0; NotIndex < 2; NotIndex++) {
			if (strcmp(ListNoConvert[NotIndex], TagName) == 0) {
				json_array_foreach(json_object_get(Lister->Json,
					TagName), ItemIndex, Item) {
					if (!JsonInArray(Item,
						json_object_get(GlobalPreferences, TagName))) {
						json_array_append(Tags, Item);
					}
				}
				break;
			} else if (NotIndex) {
				// This is for all other normal tags
				json_array_extend(Tags, json_object_get(Lister->Json, TagName));
			}
		}

		// Get all strings to an array
		const char *String;
		char *List[json_array_size(Tags) + 1];
		json_array_foreach(Tags, ItemIndex, Item) {
			String = json_string_value(Item);

			List[ItemIndex] = (char *) String;
		}
		List[ItemIndex] = NULL;

		// Append tag names to given tag
		TagAppend(List, Lister->TagList[Index]);
	}

	gtk_label_set_text(Lister->TitleLabel, Lister->Title);

	// Remove grid or nocontentnotice from mainspace
	// Either of them is added back by ListPlaceData
	GtkWidget *Parent = gtk_widget_get_parent(Lister->Grid);
	if (Parent != NULL) {
		g_object_ref(Lister->Grid);
		gtk_container_remove((GtkContainer *) Parent, Lister->Grid);
	}
	Parent = gtk_widget_get_parent(Lister->NoContentNotice);
	if (Parent != NULL) {
		g_object_ref(Lister->NoContentNotice);
		gtk_container_remove((GtkContainer *) Parent, Lister->NoContentNotice);
	}

	char ReplaceName[20];
	ListGetSpaceName(Lister->ListName, ReplaceName);
	ReplaceSet(TabData, ReplaceName);

	Lister->ListPage = 0;
	Lister->LoadCount = 1;
	if (Lister->ListButton == ListSearchButton) {
		if (Lister->FunctionData[0]) {
			json_decref(Lister->FunctionData[0]);
		}
		Lister->FunctionData[0] = json_deep_copy(Lister->Json);
		Lister->Json = NULL;
	}

	ListUpdateMainSpace(TabData, 0);

	return FALSE;
}

void ListButton(TabsData *TabData) {
	// List button function for first loading the page

	ListData *Lister;
	TabsGet(TabData, Lister, Lister);

	ImageIncreaseCount();

	// Free all claims from claim array
	for (int Index = 0; Index < Lister->ClaimCount; Index++) {
		free(Lister->Claims[Index]);
	}

	// Back to small size for claim cache
	char **Temporary = realloc(Lister->Claims, 1);
	if (Temporary == NULL) {
		return;
	}
	Lister->Claims = Temporary;
	Lister->ClaimCount = 0;

	Lister->Started = false;
	Lister->LoadCount = 0;

	// Signal KillThread to stop any boxes being made
	pthread_mutex_lock(&Lister->Lock);
	Lister->KillThread = true;
	pthread_mutex_unlock(&Lister->Lock);

	// Sleep for 10ms while waiting for threads to stop
	int ThreadCount = 1;
	struct timespec Remaining, Request = { 0, 10000000 };
	while (ThreadCount != 0) {
		nanosleep(&Request, &Remaining);

		pthread_mutex_lock(&Lister->Lock);
		ThreadCount = Lister->ThreadCount;
		pthread_mutex_unlock(&Lister->Lock);
	}

	Lister->KillThread = false;
	Lister->ListDisplay =
		JsonInt(json_object_get(GlobalPreferences, "ListDisplay"), 0);

	g_idle_add(ListButtonUpdate, TabData);
}

// Everything under this line can be removed after full rewrite
static TabsData *ListGetTabDataPython(PyObject *Module) {
	// Returns tab data pointer

	PyObject *Dict = PyModule_GetDict(Module);
	PyObject *TabPointerObject = PyDict_GetItemString(Dict, "TabPointer");

	return (TabsData *) PyLong_AsLongLong(TabPointerObject);
}

static PyObject *ListUpdateMainSpacePython(PyObject *Self, PyObject *Py_UNUSED(
		Args)) {
	// List update mainspace

	TabsData *TabData = ListGetTabDataPython(Self);

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	if (TabData->Lister.Started) {
		ListUpdateMainSpace(TabData, 1);
	}

	// Acquire lock
	PyEval_RestoreThread(Save);

	Py_RETURN_NONE;
}

static PyObject *ListGenericLoadedPython(PyObject *Self, PyObject *Py_UNUSED(
		Args)) {
	TabsData *TabData = ListGetTabDataPython(Self);

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	// TODO: This might not be the best way to deal
	// with locking loading to only one at a time
	if (TabData->Lister.Started) {
		g_idle_add(ListGenericLoaded, TabData);
	}

	// Acquire lock
	PyEval_RestoreThread(Save);

	Py_RETURN_NONE;
}

// Function names and other python data
static PyMethodDef ListModMethods[] = {
	{"GenericLoaded", (PyCFunction) ListGenericLoadedPython, METH_NOARGS,
	 "Generic load"},
	{"UpdateMainSpace", (PyCFunction) ListUpdateMainSpacePython, METH_NOARGS,
	 "Status"},
	{NULL, NULL, 0, NULL}
};

static struct PyModuleDef ListModModule = {
	PyModuleDef_HEAD_INIT, "ListMod", "List moded module", -1, ListModMethods,
	0, 0, 0, 0
};

static PyObject *ListCreatePython(PyObject *Py_UNUSED(Self), PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0
	PyObject *AddPage, *WalletSpacePartsList, *SpacesList, *Tags;
	uintptr_t NoContentNoticePointer, MainSpacePointer, GridPointer;
	uintptr_t TextPointer, TitlePointer, ChannelPointer, ClaimTypePointer;
	uintptr_t DateTypePointer, InequalityPointer, DateTimePointer;
	uintptr_t OrdererPointer, AdvancedPointer, TabPointer;
	if (!PyArg_ParseTuple(Args, "LOLLOLLLLLLLOLLOL", &NoContentNoticePointer,
		&WalletSpacePartsList, &MainSpacePointer, &GridPointer, &SpacesList,
		&TextPointer, &TitlePointer, &ChannelPointer, &ClaimTypePointer,
		&DateTypePointer,
		&InequalityPointer, &AdvancedPointer, &Tags, &DateTimePointer,
		&OrdererPointer, &AddPage, &TabPointer)) {
		return NULL;
	}

	TagData *TagList[PyList_Size(Tags)];
	int TagsSize = 0;
	while (TagsSize < PyList_Size(Tags)) {
		TagList[TagsSize] =
			(TagData *) PyLong_AsLongLong(PyList_GetItem(Tags, TagsSize));
		TagsSize++;
	}

	int IndexParts = 0;
	GtkWidget **WalletSpaceParts = malloc(6 * sizeof(GtkWidget *));
	while (IndexParts < 6) {
		WalletSpaceParts[IndexParts] = (GtkWidget *)
			PyLong_AsLongLong(PyList_GetItem(WalletSpacePartsList, IndexParts));
		IndexParts++;
	}

	int IndexSpaces = 0;
	GtkContainer *Spaces[5];
	while (IndexSpaces < 4) {
		uintptr_t SpacePointer = PyLong_AsLongLong(PyList_GetItem(SpacesList,
				IndexSpaces));
		Spaces[IndexSpaces] = (GtkContainer *) SpacePointer;
		IndexSpaces++;
	}

	ListCreate((GtkWidget *) NoContentNoticePointer, WalletSpaceParts,
		(GtkWidget *) MainSpacePointer, (GtkWidget *) GridPointer,
		(GtkWidget *) AdvancedPointer,
		Spaces, IndexSpaces, (GtkEntry *) TextPointer,
		(GtkEntry *) ChannelPointer,
		(GtkLabel *) TitlePointer, (GtkComboBox *) ClaimTypePointer,
		(GtkComboBox *) DateTypePointer, (GtkComboBox *) InequalityPointer,
		TagList, TagsSize, (DateTimeData *) DateTimePointer,
		(OrderData *) OrdererPointer, AddPage, (TabsData *) TabPointer);

	// Create module and add Pointer to it
	PyObject *Module = PyModule_Create(&ListModModule);
	PyModule_AddObject(Module, "TabPointer", PyLong_FromLongLong(TabPointer));

	return Module;
}

// Function names and other python data
static PyMethodDef ListMethods[] = {
	{"Create", ListCreatePython, METH_VARARGS, "Create"},
	{NULL, NULL, 0, NULL}
};

static struct PyModuleDef ListModule = {
	PyModuleDef_HEAD_INIT, "List", "List module", -1, ListMethods, 0, 0, 0, 0
};

// The function name has to end with the module name
PyMODINIT_FUNC PyInit_List(void) {
	return PyModule_Create(&ListModule);
}
