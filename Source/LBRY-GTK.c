/******************************************************************************\
* LBRY-GTK                                                                     *
* Copyright (C) 2021-2023 MorsMortium and Other Contributors                   *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU General Public License as published by         *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                *
* GNU General Public License for more details.                                 *
*                                                                              *
* You should have received a copy of the GNU General Public License            *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
\******************************************************************************/

// This is the main executable file

#include <Python.h>
#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <time.h>

#include "Header/OS.h"
#include "Header/Places.h"
#include "Header/Parse.h"
#include "Header/Connect.h"
#include "Header/Threads.h"
#include "Header/Markdown.h"
#include "Header/Image.h"
#include "Header/Icons.h"
#include "Header/GTKExtra.h"
#include "Header/Language.h"
#include "Header/ConnectDB.h"
#include "Header/Preferences.h"

extern PyMODINIT_FUNC PyInit_CRewrite(void);

const char *Imports[] =
{"gi", "json", "sys", "threading", "datetime", "time", "os", "queue", "re",
 "copy", "traceback", "shutil", "pathlib", "cairo", NULL };

int main(int argc, char *argv[]) {
	// Initialize python interpreter

	// Get root directory
	char *Root = OSAbsolutePath(argv[0]);

	// Seed for rand() type functions
	// TODO: Move to Tools.c or something similar
	srand((unsigned int) (time(NULL)));

	// Init GTK
	gtk_init(&argc, &argv);

	// Start functions
	ThreadsStart();
	PlacesStart(Root);
	ConnectStart();
	MarkdownStart();
	ImageStart();
	IconsStart();
	GTKExtraStart();
	LanguageStart();
	ConnectDBStart();

	char DefaultSettings[strlen(PlacesJson) + 25];
	sprintf(DefaultSettings, "%sDefaultSettings.json", PlacesJson);

	PreferencesStart(PlacesConfig, DefaultSettings);

	// Get the indexes where we find OS separator i.e. '/' or '\'
	int Indexes[] = { 0, 0 };
	for (int Index = 0; Root[Index] != '\0'; Index++) {
		if (Root[Index] == OSSeparator[0]) {
			Indexes[0] = Indexes[1];
			Indexes[1] = Index;
		}
	}

	PyImport_AppendInittab("CRewrite", &PyInit_CRewrite);
	Py_Initialize();

	// This is rather odd, one might say, but this is necessary
	// as main thread (C main) takes the ownership of GIL
	// and doesn't release it for other threads therefore
	// killing threading entirely.
	PyThreadState *Save = PyEval_SaveThread();
	PyGILState_STATE State = PyGILState_Ensure();

	// All the modules tested in an array
	PyObject *Module;
	for (int Index = 0; Imports[Index] != NULL; Index++) {
		Module = PyImport_ImportModule(Imports[Index]);
		if (Module == NULL) {
			PyErr_Print();
			Py_Finalize();
			return 1;
		}
		Py_DECREF(Module);
	}

	Root[Indexes[0]] = '\0';
	char ModuleDir[strlen(Root) + 30];

	// Get sys.path here to append project's module path to it
	PyObject *Path = PySys_GetObject("path");

	// This makes Source module directory discoverable
	sprintf(ModuleDir, "%s" OSSeparator "share" OSSeparator "lbry-gtk", Root);
	free(Root);

	PyObject *Item = PyUnicode_FromString(ModuleDir);
	PyList_Append(Path, Item);
	Py_DECREF(Item);

	// Import Main module from Source
	PyObject *Name = PyUnicode_FromString("Source.Main");
	PyObject *MainModule = PyImport_Import(Name);
	Py_DECREF(Name);
	if (MainModule == NULL) {
		PyErr_Print();
		Py_Finalize();
		return 1;
	}

	// Get Main object and execute it
	PyObject *Func = PyObject_GetAttrString(MainModule, "Main");
	PyObject *Call = PyObject_CallObject(Func, NULL);
	Py_DECREF(MainModule);
	Py_DECREF(Func);
	if (Call) {
		Py_DECREF(Call);

		// Release GIL from C main
		PyGILState_Release(State);

		// We can init Parse here now
		ParseInit();

		// TODO: Perhaps connect these elsewhere
		signal(SIGABRT, OSSIGHandler);
		signal(SIGSEGV, OSSIGHandler);

		// Gtk main started here
		gtk_main();

		PyEval_RestoreThread(Save);
	} else {
		PyErr_Print();
	}

	// Finalize Python and get out
	Py_Finalize();

	// Stop functions after GTK main
	ConnectDBStop();
	PreferencesStop();
	LanguageStop();
	GTKExtraStop();
	IconsStop();
	ImageStop();
	MarkdownStop();
	ConnectStop();
	PlacesStop();

	return 0;
}
