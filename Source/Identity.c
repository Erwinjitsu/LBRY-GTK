/******************************************************************************\
* LBRY-GTK                                                                     *
* Copyright (C) 2021-2023 MorsMortium and Other Contributors                   *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU General Public License as published by         *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                *
* GNU General Public License for more details.                                 *
*                                                                              *
* You should have received a copy of the GNU General Public License            *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
\******************************************************************************/

// Manages the Identity widget responsible for selecting active channel

#include <Python.h>
#include <gtk/gtk.h>
#include <stdbool.h>
#include <jansson.h>
#include <stdio.h>
#include <string.h>

#include "Header/Channel.h"
#include "Header/Global.h"
#include "Header/GTKExtra.h"
#include "Header/Identity.h"
#include "Header/Json.h"
#include "Header/Places.h"

IdentityData *IdentityCreate(bool Comment) {
	// Creates the Identity widget

	// Allocate memory for data, abort if NULL
	IdentityData *Data = malloc(sizeof(IdentityData));
	if (Data == NULL) {
		abort();
	}

	// Get glade file name, load it in builder
	char WidgetFile[strlen(PlacesGlade) + 15];
	sprintf(WidgetFile, "%sIdentity.glade", PlacesGlade);
	gtk_builder_add_from_file(GlobalBuilder, WidgetFile, NULL);

	// Get widgets used
	Data->Identities = (GtkWidget *) gtk_builder_get_object(GlobalBuilder,
			"Identities");
	Data->Active =
		(GtkWidget *) gtk_builder_get_object(GlobalBuilder, "Active");
	Data->IdentitiesBox = (GtkWidget *) gtk_builder_get_object(GlobalBuilder,
			"IdentitiesBox");

	Data->Comment = Comment;

	// Update identities
	IdentityUpdate(Data);

	// Connect signals with data
	gtk_builder_connect_signals_full(GlobalBuilder, GTKExtraConnect, Data);

	return Data;
}

void IdentityUpdate(IdentityData *Data) {
	// Fills widget with list of channels, selects main channel, if needed

	// Replace Identity list
	if (IdentityList != NULL) {
		json_decref(IdentityList);
	}
	json_t *Session = json_object_get(GlobalPreferences, "Session");
	const char *Server = json_string_value(json_object_get(Session, "Server"));
	IdentityList = ChannelGet((char *) Server);

	// Get channel used for commenting
	const char *DefaultChannel = NULL;
	if (Data->Comment) {
		DefaultChannel =
			json_string_value(json_object_get(GlobalPreferences,
				"CommentChannel"));
	}

	// Empty widget containing channels
	gtk_combo_box_text_remove_all((GtkComboBoxText *) Data->Identities);

	size_t Index;
	json_t *Value;

	json_array_foreach(IdentityList, Index, Value) {
		// Add each channel to combobox
		const char *Name = json_string_value(json_object_get(Value, "Name"));
		gtk_combo_box_text_append_text((GtkComboBoxText *) Data->Identities,
			Name);

		// Select commenting channel, if available
		if (DefaultChannel != NULL && strcmp(Name, DefaultChannel) == 0) {
			gtk_combo_box_set_active((GtkComboBox *) Data->Identities, Index);
		}
	}

	// If commenting channel was not available, select first
	if (gtk_combo_box_get_active((GtkComboBox *) Data->Identities) == -1) {
		gtk_combo_box_set_active((GtkComboBox *) Data->Identities, 0);
	}
}

json_t *IdentityGet(IdentityData *Data) {
	// Gets currently selected channel, or empty channel

	// Get current channel
	int Index = gtk_combo_box_get_active((GtkComboBox *) Data->Identities);
	json_t *Current;
	if (Index == -1) {
		// Create empty channel
		Current = json_object();
		json_object_set_new(Current, "Name", json_string(""));
		json_object_set_new(Current, "Title", json_string(""));
		json_object_set_new(Current, "Url", json_string(""));
		json_object_set_new(Current, "Thumbnail", json_string(""));
		json_object_set_new(Current, "Claim", json_string(""));
	} else {
		// Get current channel, increase reference for unified usage
		// (json_decref after used)
		Current = json_array_get(IdentityList, Index);
		json_incref(Current);
	}
	return Current;
}

gboolean IdentityOnIdentitiesBoxDestroy(G_GNUC_UNUSED GtkWidget *Widget,
	IdentityData *Data) {
	// Frees all data, after widget is destroyed

	free(Data);
	return FALSE;
}

// Everything under this line is removable after full C conversion
static PyObject *IdentityCreatePython(PyObject *Py_UNUSED(Self),
	PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Fill Comment, Identities with sent data
	int Comment;
	if (!PyArg_ParseTuple(Args, "i", &Comment)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	IdentityData *Data = IdentityCreate((bool) Comment);

	// Acquire lock
	PyEval_RestoreThread(Save);

	PyObject *Dict = PyDict_New();

	PyObject *Object = PyLong_FromVoidPtr((void *) Data->Identities);
	PyDict_SetItemString(Dict, "Identities", Object);
	Py_DECREF(Object);

	Object = PyLong_FromVoidPtr((void *) Data->Active);
	PyDict_SetItemString(Dict, "Active", Object);
	Py_DECREF(Object);

	Object = PyLong_FromVoidPtr((void *) Data->IdentitiesBox);
	PyDict_SetItemString(Dict, "IdentitiesBox", Object);
	Py_DECREF(Object);

	Object = PyLong_FromVoidPtr((void *) Data);
	PyDict_SetItemString(Dict, "Pointer", Object);
	Py_DECREF(Object);

	return Dict;
}

static PyObject *IdentityUpdatePython(PyObject *Py_UNUSED(Self),
	PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Fill DataPointer with sent data
	uintptr_t DataPointer;
	if (!PyArg_ParseTuple(Args, "L", &DataPointer)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	IdentityUpdate((IdentityData *) DataPointer);

	// Acquire lock
	PyEval_RestoreThread(Save);

	return PyLong_FromLong(0);
}

static PyObject *IdentityGetPython(PyObject *Py_UNUSED(Self), PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Fill DataPointer with sent data
	uintptr_t DataPointer;
	if (!PyArg_ParseTuple(Args, "L", &DataPointer)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	json_t *Identity = IdentityGet((IdentityData *) DataPointer);

	// Acquire lock
	PyEval_RestoreThread(Save);

	return JsonToString(Identity);
}

// Function names and other python data
static PyMethodDef IdentityMethods[] = {
	{"Create", IdentityCreatePython, METH_VARARGS, ""},
	{"Update", IdentityUpdatePython, METH_VARARGS, ""},
	{"Get", IdentityGetPython, METH_VARARGS, ""},
	{NULL, NULL, 0, NULL}
};

static struct PyModuleDef IdentityModule = {
	PyModuleDef_HEAD_INIT, "Identity", "Identity module", -1, IdentityMethods,
	0, 0, 0, 0
};

// The function name has to end with the module name
PyMODINIT_FUNC PyInit_Identity(void) {
	return PyModule_Create(&IdentityModule);
}
