/******************************************************************************\
* LBRY-GTK                                                                     *
* Copyright (C) 2021-2022 MorsMortium and Other Contributors                   *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU General Public License as published by         *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                *
* GNU General Public License for more details.                                 *
*                                                                              *
* You should have received a copy of the GNU General Public License            *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
\******************************************************************************/

// Here we manipulate/check the comment table in lbry-gtk database

#include <Python.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <jansson.h>
#include <sqlite3.h>

#include "Header/Channel.h"
#include "Header/Comments.h"
#include "Header/ConnectDB.h"
#include "Header/CommentDB.h"

void CommentDBChange(char *Comment, char *ClaimID, char *CommentID,
	char *ChannelID, unsigned int Timestamp, int Option) {
	// Do the necessary changes to comments table
	// Options: Save (1), Update (2), Delete (3)

	// Get the appropriate query
	char *Query;
	switch (Option) {
		case 1:
			Query = "INSERT INTO Comments(Comment, ClaimID, "
				"CommentID, ChannelID, Timestamp) "
				"VALUES(?1, ?2, ?3, ?4, ?5)";
			break;
		case 2:
			Query = "UPDATE Comments SET Comment=?1, Timestamp=?2"
				" WHERE CommentID=?3";
			break;
		case 3:
			Query = "DELETE FROM Comments WHERE CommentID=?1";
			break;
		default:
			fprintf(stderr, "Option number %d is not valid", Option);
			return;
	}

	// Lock for operation/wait for lock
	pthread_mutex_lock(&ConnectDBLock);

	// Make a connection to DB
	sqlite3 *DB = ConnectDBOpen();
	if (DB == NULL) {
		pthread_mutex_unlock(&ConnectDBLock);
		return;
	}

	// Sqlite statement
	sqlite3_stmt *Stmt;

	// Check the statement is valid and no errors were found
	int RC = sqlite3_prepare_v2(DB, Query, -1, &Stmt, 0);
	if (RC != SQLITE_OK) {
		fprintf(stderr, "Statement wrong (%d): %s\n", RC,
			sqlite3_errmsg(DB));
		sqlite3_finalize(Stmt);
		sqlite3_close_v2(DB);
		pthread_mutex_unlock(&ConnectDBLock);
		return;
	}

	// Make correct bindings based on the option
	switch (Option) {
		case 1:
			sqlite3_bind_text(Stmt, 1, Comment, strlen(Comment),
				SQLITE_STATIC);
			sqlite3_bind_text(Stmt, 2, ClaimID, strlen(ClaimID),
				SQLITE_STATIC);
			sqlite3_bind_text(Stmt, 3, CommentID, strlen(CommentID),
				SQLITE_STATIC);
			sqlite3_bind_text(Stmt, 4, ChannelID, strlen(ChannelID),
				SQLITE_STATIC);
			sqlite3_bind_int(Stmt, 5, Timestamp);
			break;
		case 2:
			sqlite3_bind_text(Stmt, 1, Comment, strlen(Comment),
				SQLITE_STATIC);
			sqlite3_bind_int(Stmt, 2, Timestamp);
			sqlite3_bind_text(Stmt, 3, CommentID, strlen(CommentID),
				SQLITE_STATIC);
			break;
		case 3:
			sqlite3_bind_text(Stmt, 1, CommentID, strlen(CommentID),
				SQLITE_STATIC);
			break;
	}

	// Executes the actual statement
	RC = sqlite3_step(Stmt);
	if (RC != SQLITE_DONE) {
		fprintf(stderr, "Something went wrong (%d): %s\n", RC,
			sqlite3_errmsg(DB));
	}

	// Destroy Stmt
	sqlite3_finalize(Stmt);

	// Close DB
	sqlite3_close_v2(DB);

	// Unlock
	pthread_mutex_unlock(&ConnectDBLock);
}

static void *CommentDBImportThread(void *Args) {
	// Get the comment history of a channel

	// Get the given arguments into a struct
	ConnectDBArgs *CInfo = Args;

	// List all comments or maximum (MaxRecords), this might take a while
	json_t *Comments = CommentsList(CInfo->ChannelName, "", "",
			CInfo->ClaimID, "", CInfo->CommentServer, 4, CInfo->MaxRecords,
			1, CInfo->Server);

	// Check for error
	if (json_object_get(Comments, "error") != NULL) {
		json_decref(Comments);
		return NULL;
	}

	// First json value needs to be an array
	if (json_is_array(json_array_get(Comments, 0)) == 0) {
		json_decref(Comments);
		return NULL;
	}

	// Lock for operation/wait for lock
	pthread_mutex_lock(&ConnectDBLock);

	// Make a connection to DB
	sqlite3 *DB = ConnectDBOpen();
	if (DB == NULL) {
		json_decref(Comments);
		pthread_mutex_unlock(&ConnectDBLock);
		return NULL;
	}

	// Statement holder
	sqlite3_stmt *Stmt;

	// Make the insertable rows
	size_t Index;
	json_t *Item;
	json_array_foreach(json_array_get(Comments, 0), Index, Item) {
		// Get values to variables
		char *Comment =
			(char *) json_string_value(json_object_get(Item, "Text"));
		char *ClaimID =
			(char *) json_string_value(json_object_get(Item, "ClaimId"));
		char *CommentID =
			(char *) json_string_value(json_object_get(Item, "Id"));
		char *ChannelID = (char *) json_string_value(json_object_get(Item,
				"Url"));
		unsigned int Timestamp =
			json_integer_value(json_object_get(Item, "TimeStamp"));

		// Cut out the id part of it
		ChannelID = strstr(ChannelID, "#");

		// ChannelID being NULL creates problems
		if (ChannelID == NULL) {
			continue;
		}

		char *Query = "REPLACE INTO Comments(Comment, ClaimID, CommentID,"
			"ChannelID, Timestamp) VALUES(?1, ?2, ?3, ?4, ?5)";

		int RC = sqlite3_prepare_v2(DB, Query, -1, &Stmt, 0);
		if (RC != SQLITE_OK) {
			fprintf(stderr, "Statement wrong (%d): %s\n", RC,
				sqlite3_errmsg(DB));
			continue;
		}

		// Bind values to prepared statement
		sqlite3_bind_text(Stmt, 1, Comment, strlen(Comment), SQLITE_STATIC);
		sqlite3_bind_text(Stmt, 2, ClaimID, strlen(ClaimID), SQLITE_STATIC);
		sqlite3_bind_text(Stmt, 3, CommentID, strlen(CommentID), SQLITE_STATIC);
		sqlite3_bind_text(Stmt, 4, ChannelID + 1, strlen(
				ChannelID), SQLITE_STATIC);
		sqlite3_bind_int(Stmt, 5, Timestamp);

		// Actually make the change
		RC = sqlite3_step(Stmt);
		if (RC != SQLITE_DONE) {
			fprintf(stderr, "Something went wrong (%d): %s\n", RC,
				sqlite3_errmsg(DB));
		}

		// Reset statement for next iteration
		sqlite3_reset(Stmt);
	}

	// Forget about all those json blocks
	json_decref(Comments);

	// Finally destroy the statement
	sqlite3_finalize(Stmt);

	// Close DB
	sqlite3_close_v2(DB);

	// Unlock
	pthread_mutex_unlock(&ConnectDBLock);

	return NULL;
}

void *CommentDBImport(int MaxRecords, char *CommentServer, char *Server) {
	// Gets the comment history from remote

	// Lock for operation/wait for lock
	pthread_mutex_lock(&ConnectDBLock);

	// Make new connection to DB
	sqlite3 *DB = ConnectDBOpen();
	if (DB == NULL) {
		pthread_mutex_unlock(&ConnectDBLock);
		return NULL;
	}

	// Statement holder
	sqlite3_stmt *Stmt;

	// Create query
	char *Query = "SELECT SettingValue FROM SettingTable WHERE"
		" SettingName='HistoryFetched'";

	// Execute query
	int RC = sqlite3_prepare_v2(DB, Query, -1, &Stmt, 0);
	if (RC != SQLITE_OK) {
		fprintf(stderr, "Statement wrong (%d): %s\n", RC,
			sqlite3_errmsg(DB));
		sqlite3_finalize(Stmt);
		sqlite3_close_v2(DB);
		pthread_mutex_unlock(&ConnectDBLock);
		return NULL;
	}

	// Get the row if present
	RC = sqlite3_step(Stmt);

	// Destroy Stmt
	sqlite3_finalize(Stmt);

	// Add if wasn't present yet
	Query = "REPLACE INTO SettingTable (SettingName, SettingValue) "
		"VALUES ('HistoryFetched', 1)";
	sqlite3_exec(DB, Query, 0, 0, NULL);

	// Close DB
	sqlite3_close_v2(DB);

	// Unlock
	pthread_mutex_unlock(&ConnectDBLock);

	// If row was present return
	if (RC == SQLITE_ROW) {
		return NULL;
	}

	// Get all user channels
	json_t *Channels = ChannelGet(Server);
	if (json_object_get(Channels, "error") != NULL) {
		return NULL;
	}

	// Get the size of channels list
	size_t ChannelsAmount = json_array_size(Channels);
	if (ChannelsAmount == 0) {
		return NULL;
	}

	// Allocate memory for thread variables
	ConnectDBArgs *CInfo = calloc(ChannelsAmount, sizeof(*CInfo));
	if (CInfo == NULL) {
		json_decref(Channels);
		return NULL;
	}

	// Loop over each object in array
	int ThreadNumber = 0;
	while (ThreadNumber < (int) ChannelsAmount) {
		// Get the channel name
		json_t *Value = json_array_get(Channels, ThreadNumber);
		char *ChannelName = (char *) json_string_value(
			json_object_get(Value, "Name"));
		char *ClaimID = (char *) json_string_value(
			json_object_get(Value, "Claim"));
		if (ChannelName == NULL || ClaimID == NULL) {
			ThreadNumber++;
			continue;
		}

		// All data to struct
		CInfo[ThreadNumber].ChannelName = ChannelName;
		CInfo[ThreadNumber].ClaimID = ClaimID;
		CInfo[ThreadNumber].Server = Server;
		CInfo[ThreadNumber].CommentServer = CommentServer;
		CInfo[ThreadNumber].MaxRecords = MaxRecords;

		// Actual thread creation
		RC = pthread_create(&CInfo[ThreadNumber].Thread, NULL,
				&CommentDBImportThread, &CInfo[ThreadNumber]);

		// Check the thread actually got created
		if (RC != 0) {
			fprintf(stderr, "Could not create thread\n");
		}
		ThreadNumber++;
	}

	// Wait for threads to do their work
	ConnectDBJoin((int) ChannelsAmount, CInfo);

	// Forget about those channels :O
	json_decref(Channels);

	// Free allocated memory
	free(CInfo);

	return NULL;
}

json_t *CommentDBList(int Page, int PageSize) {
	// Get comments from DB

	// Lock for operation/wait for lock
	pthread_mutex_lock(&ConnectDBLock);

	// Make a connection to DB
	sqlite3 *DB = ConnectDBOpen();
	if (DB == NULL) {
		pthread_mutex_unlock(&ConnectDBLock);
		return NULL;
	}

	// Sqlite statement
	sqlite3_stmt *Stmt;

	// The query
	char *Query = "SELECT rowid, Channel, Title, Timestamp, ContentType,"
		" Url, ImageUrl FROM Comments ORDER BY Timestamp DESC "
		"LIMIT ?1, OFFSET ?2";

	// Prepare our statement
	int RC = sqlite3_prepare_v2(DB, Query, -1, &Stmt, 0);
	if (RC != SQLITE_OK) {
		fprintf(stderr, "Statement wrong (%d): %s\n", RC,
			sqlite3_errmsg(DB));
		sqlite3_finalize(Stmt);
		sqlite3_close_v2(DB);
		return NULL;
	}

	// Since we are using prepare we need to bind values to placeholders (?)
	sqlite3_bind_int(Stmt, 1, PageSize);
	sqlite3_bind_int(Stmt, 2, (Page * PageSize));

	// Create json array holding our items
	json_t *Items = json_array();

	// Fetch each row into a json block
	while (sqlite3_step(Stmt) == SQLITE_ROW) {
		// New object to hold item
		json_t *Item = json_object();

		// Place values into an object
		// TODO: see what to return once this function is used
		json_object_set_new(Item, "Channel", json_string(
				(char *) sqlite3_column_text(Stmt, 1)));
		json_object_set_new(Item, "Title", json_string(
				(char *) sqlite3_column_text(Stmt, 2)));
		json_object_set_new(Item, "TimeStamp", json_integer(
				sqlite3_column_int(Stmt, 3)));
		json_object_set_new(Item, "Type", json_integer(
				sqlite3_column_int(Stmt, 4)));
		json_object_set_new(Item, "Url", json_string(
				(char *) sqlite3_column_text(Stmt, 5)));
		json_object_set_new(Item, "Thumbnail", json_string(
				(char *) sqlite3_column_text(Stmt, 6)));

		// Append the newly made item into the items array
		json_array_append_new(Items, Item);
	}

	// Destroy Stmt
	sqlite3_finalize(Stmt);

	// Close DB
	sqlite3_close_v2(DB);

	// Unlock
	pthread_mutex_unlock(&ConnectDBLock);

	// Return the json array
	return Items;
}

// Everything under this line can be removed after not used in python code
static PyObject *CommentDBChangePython(PyObject *Py_UNUSED(
		Self), PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Get arguments and pass it to the pythonless function
	char *Comment, *ClaimID, *CommentID, *ChannelID;
	unsigned int Timestamp;
	int Option;

	// We have z here for strings since those can also be None (NULL)
	if (!PyArg_ParseTuple(Args, "zzszIi", &Comment, &ClaimID, &CommentID,
		&ChannelID, &Timestamp, &Option)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	CommentDBChange(Comment, ClaimID, CommentID, ChannelID, Timestamp, Option);

	// Acquire lock
	PyEval_RestoreThread(Save);

	return PyLong_FromLong(0);
}

static PyObject *CommentDBImportPython(PyObject *Py_UNUSED(
		Self), PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Get arguments and pass it to the pythonless function
	int MaxRecords;
	char *CommentServer, *Server;
	if (!PyArg_ParseTuple(Args, "iss", &MaxRecords, &CommentServer, &Server)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	CommentDBImport(MaxRecords, CommentServer, Server);

	// Acquire lock
	PyEval_RestoreThread(Save);

	return PyLong_FromLong(0);
}

static PyObject *CommentDBListPython(PyObject *Py_UNUSED(Self),
	PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Get arguments and pass it to the pythonless function
	int Page, PageSize;
	if (!PyArg_ParseTuple(Args, "ii", &Page, &PageSize)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	json_t *JsonData = CommentDBList(Page, PageSize);

	char *ResultString = json_dumps(JsonData, 0);
	json_decref(JsonData);

	// Acquire lock
	PyEval_RestoreThread(Save);

	PyObject *ResultPython = PyUnicode_FromString(ResultString);
	free(ResultString);

	return ResultPython;
}

// Function names and other python data
static PyMethodDef CommentDBMethods[] = {
	{"Change", CommentDBChangePython, METH_VARARGS, "Check to notify"},
	{"Import", CommentDBImportPython, METH_VARARGS, "Import comments"},
	{"List", CommentDBListPython, METH_VARARGS, "List comments"},
	{NULL, NULL, 0, NULL}
};

static struct PyModuleDef CommentDBModule = {
	PyModuleDef_HEAD_INIT, "CommentDB", "CommentDB module", -1,
	CommentDBMethods, 0, 0, 0, 0
};

// The function name has to end with the module name
PyMODINIT_FUNC PyInit_CommentDB(void) {
	return PyModule_Create(&CommentDBModule);
}
