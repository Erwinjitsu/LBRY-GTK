/******************************************************************************\
* LBRY-GTK                                                                     *
* Copyright (C) 2021-2023 MorsMortium and Other Contributors                   *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU General Public License as published by         *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                *
* GNU General Public License for more details.                                 *
*                                                                              *
* You should have received a copy of the GNU General Public License            *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
\******************************************************************************/

// This file is responsible for creating message and error popups

#include <Python.h>
#include <jansson.h>
#include <stdbool.h>
#include <gtk/gtk.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "Header/Global.h"
#include "Header/Json.h"

// For getting DeveloperMode
char *PopupDeveloper[] = { "Session", "DeveloperMode", NULL };

static gboolean PopupHelper(gpointer GotData) {
	// This function is responsible for creating a popup and displaying it

	// Get text from datapointer
	char *Text = (char *) GotData;

	// Create popup widget
	GtkWidget *Dialog =
		gtk_message_dialog_new(GlobalWindow, GTK_DIALOG_DESTROY_WITH_PARENT,
			GTK_MESSAGE_INFO, GTK_BUTTONS_CLOSE, "%s", Text);

	// Get size of dialog, window
	int DialogHeight, ParentHeight, SmallDialogHeight;
	int DialogWidth, ParentWidth, SmallDialogWidth;
	gtk_widget_get_preferred_height(Dialog, &DialogHeight, NULL);
	ParentHeight = gtk_widget_get_allocated_height((GtkWidget *) GlobalWindow);
	gtk_widget_get_preferred_width(Dialog, &DialogWidth, NULL);
	ParentWidth = gtk_widget_get_allocated_width((GtkWidget *) GlobalWindow);

	// Free Text
	free(Text);

	GtkWidget *MessageArea =
		gtk_message_dialog_get_message_area((GtkMessageDialog *) Dialog);

	// Remove MessageArea from parent
	g_object_ref((GObject *) MessageArea);
	GtkWidget *Parent = gtk_widget_get_parent(MessageArea);
	gtk_container_remove((GtkContainer *) Parent, MessageArea);

	// Get size of dialog without MessageArea
	gtk_widget_get_preferred_height(Dialog, &SmallDialogHeight, NULL);
	gtk_widget_get_preferred_width(Dialog, &SmallDialogWidth, NULL);

	// add scrolled window between MessageArea and its parent
	GtkWidget *Scrolled = gtk_scrolled_window_new(NULL, NULL);
	gtk_container_add((GtkContainer *) Parent, Scrolled);
	gtk_container_add((GtkContainer *) Scrolled, MessageArea);
	g_object_unref((GObject *) MessageArea);

	// Get final sizes
	int Width = -1, Height = -1;
	if (ParentWidth < DialogWidth) {
		Width = ParentWidth - SmallDialogWidth;
	}
	if (ParentHeight < DialogHeight) {
		Height = ParentHeight - SmallDialogHeight;
	}

	// Set final sizes
	gtk_widget_set_size_request(Scrolled, Width, Height);

	// If real size was smaller than window, use that
	if (Width == -1) {
		gtk_scrolled_window_set_propagate_natural_width((GtkScrolledWindow *)
			Scrolled, TRUE);
	}
	if (Height == -1) {
		gtk_scrolled_window_set_propagate_natural_height((GtkScrolledWindow *)
			Scrolled, TRUE);
	}

	gtk_widget_show_all(Parent);

	// Get text widget of popup, make it selectable
	GList *Children = gtk_container_get_children((GtkContainer *) MessageArea);
	GtkLabel *Label = g_list_nth_data(Children, 0);
	g_list_free(Children);
	gtk_label_set_selectable(Label, true);

	// Display then destroy widget
	gtk_dialog_run((GtkDialog *) Dialog);
	gtk_widget_destroy(Dialog);

	return FALSE;
}

void PopupMessage(const char *Text) {
	// This function is responsible for starting PopupHelper in the main thread

	// Create Text, free and exit if unsuccessful
	char *Data = malloc(strlen(Text) + 1);
	if (Data == NULL) {
		return;
	}

	// Set data, call PopupHelper in main thread
	sprintf(Data, "%s", Text);
	g_idle_add(PopupHelper, Data);
}

void PopupError(const char *Text) {
	// This function is responsible for checking if the user enabled
	// DeveloperMode and forwarding data to PopupMessage, if so

	// Check DeveloperMode, free preferences
	bool DeveloperMode =
		JsonBoolObject(GlobalPreferences, PopupDeveloper, false);

	// Exit if not in developer mode, forward to PopupMessage otherwise
	if (!DeveloperMode) {
		return;
	}
	PopupMessage(Text);
}

// Everything under this line is removable after full C conversion
static PyObject *PopupMessagePython(PyObject *Py_UNUSED(Self), PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Fill Text, LongPointer
	char *Text;
	if (!PyArg_ParseTuple(Args, "s", &Text)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	PopupMessage(Text);

	// Acquire lock
	PyEval_RestoreThread(Save);

	return PyLong_FromLong(0);
}

static PyObject *PopupErrorPython(PyObject *Py_UNUSED(Self), PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Fill Text, LongPointer
	char *Text;
	if (!PyArg_ParseTuple(Args, "s", &Text)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	PopupError(Text);

	// Acquire lock
	PyEval_RestoreThread(Save);

	return PyLong_FromLong(0);
}

// Function names and other python data
static PyMethodDef PopupMethods[] = {
	{"Message", PopupMessagePython, METH_VARARGS, "Create message popup"},
	{"Error", PopupErrorPython, METH_VARARGS, "Create error popup"},
	{NULL, NULL, 0, NULL}
};

static struct PyModuleDef PopupModule = {
	PyModuleDef_HEAD_INIT, "Popup", "Popup module", -1, PopupMethods, 0, 0, 0, 0
};

// The function name has to end with the module name
PyMODINIT_FUNC PyInit_Popup(void) {
	return PyModule_Create(&PopupModule);
}
