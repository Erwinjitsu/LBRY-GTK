/******************************************************************************\
* LBRY-GTK                                                                     *
* Copyright (C) 2021-2023 MorsMortium and Other Contributors                   *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU General Public License as published by         *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                *
* GNU General Public License for more details.                                 *
*                                                                              *
* You should have received a copy of the GNU General Public License            *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
\******************************************************************************/

// This file will deal with wallet related information.

#include <stdio.h>
#include <string.h>
#include <jansson.h>
#include <Python.h>

#include "Header/Request.h"
#include "Header/Filter.h"
#include "Header/Url.h"
#include "Header/Json.h"
#include "Header/Wallet.h"

// Infos used in finding the array with items
static char *WalletInfos[] = {
	"support_info", "update_info", "abandon_info", "claim_info", "purchase_info"
};

json_t *WalletHistory(int PageSize, int Page, char *Server) {
	// This gets the transaction list

	json_error_t Error;
	json_t *Default = json_loads("{\"error\": \"no listings\"}", 0, &Error);

	// Make the request
	char JsonRequest[100];
	sprintf(JsonRequest, "{\"method\" : \"transaction_list\", \"params\" : "
		"{ \"page_size\" : %d, \"page\" : %d }}", PageSize, Page);

	// Get result
	json_t *Result = RequestJson(Server, JsonRequest);
	if (json_object_get(Result, "error") != NULL) {
		json_decref(Default);
		return Result;
	}

	// The items array is required
	json_t *Items = json_object_get(Result, "items");
	if (Items == NULL) {
		json_decref(Result);
		return Default;
	}
	json_decref(Default);

	char *Querries[PageSize];
	int Indices[PageSize];
	int ClaimIDCount = 0;

	// Each array item is checked for claim_id
	size_t Index;
	json_t *Item;
	json_array_foreach(Items, Index, Item) {
		for (int I = 0; I < (int) (sizeof(WalletInfos) / sizeof(char *)); I++) {
			// Check each info array and see if it has items in it
			json_t *Info = json_object_get(Item, WalletInfos[I]);
			if (json_array_size(Info) > 0) {
				// Gets the claim_id
				json_t *Value = json_object_get(json_array_get(Info, 0),
						"claim_id");

				// House values in arrays
				Querries[ClaimIDCount] = (char *) json_string_value(Value);
				Indices[ClaimIDCount] = (int) Index;

				ClaimIDCount++;
			}
		}
	}

	// Pretty part starts from here. Check if claim_id found
	if (ClaimIDCount) {
		// Here we get the claims
		json_t *Claims = UrlGet(Querries, ClaimIDCount, Server);
		if (json_object_get(Claims, "error") == NULL) {
			// Check each claim for same claim_id
			for (int I = 0; I < ClaimIDCount; I++) {
				// Get the value
				json_t *Value = json_array_get(Items, Indices[I]);

				// Insert into value
				json_object_set(Value, "claim", json_array_get(Claims, I));
			}
		}
		json_decref(Claims);
	}

	// Parse the resulting json object
	return FilterBalances(Result);
}

json_t *WalletBalance(char *Server) {
	// This gets the wallet balance

	// Get result and return it
	return RequestJson(Server, "{ \"method\" : \"wallet_balance\" }");
}

// Everything under this line is removable after full C conversion
static PyObject *WalletBalancePython(PyObject *Py_UNUSED(Self),
	PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Get arguments and pass it to the pythonless function
	char *Server = NULL;
	if (!PyArg_ParseTuple(Args, "s", &Server)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	json_t *JsonData = WalletBalance(Server);

	// Acquire lock
	PyEval_RestoreThread(Save);

	return JsonToString(JsonData);
}

// Function names and other python data
static PyMethodDef WalletMethods[] = {
	{"Balance", WalletBalancePython, METH_VARARGS, "Balance get"},
	{NULL, NULL, 0, NULL}
};

static struct PyModuleDef WalletModule = {
	PyModuleDef_HEAD_INIT, "Wallet", "Wallet module", -1, WalletMethods, 0, 0,
	0, 0
};

// The function name has to end with the module name
PyMODINIT_FUNC PyInit_Wallet(void) {
	return PyModule_Create(&WalletModule);
}
