/******************************************************************************\
* LBRY-GTK                                                                     *
* Copyright (C) 2021-2023 MorsMortium and Other Contributors                   *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU General Public License as published by         *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                *
* GNU General Public License for more details.                                 *
*                                                                              *
* You should have received a copy of the GNU General Public License            *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
\******************************************************************************/

// This file contains extra functions for GTK related tasks, needed by many
// other files

#include <Python.h>
#include <gtk/gtk.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "Header/GTKExtra.h"

// Symbols from library/executable
GModule *GTKExtraSymbols = NULL;

GtkCssProvider *GTKExtraCss;

char *GTKExtraClaimId[] = { "claim_id", NULL };
char *GTKExtraMinusOne = "-1";

char *GTKExtraLibrary(char *Name) {
	// This function is responsible for getting the current library path
	// TODO: Remove this and use NULL in its place after CRewrite

	// Get proc file for executable
	FILE *File = fopen("/proc/self/maps", "r");
	if (File == NULL) {
		return NULL;
	}

	// Get length of proc file
	int Length = 0;
	while (fgetc(File) != EOF) {
		++Length;
	}
	if (Length == 0) {
		return NULL;
	}

	// Get content of file, close it
	char FileContent[Length + 1];
	rewind(File);
	int GotLength = fread(FileContent, 1, Length, File);
	fclose(File);
	if (Length != GotLength) {
		return NULL;
	}
	FileContent[Length] = '\0';

	// Find first location of library name
	char *NamePlace = strstr(FileContent, Name);
	if (NamePlace == NULL) {
		return NULL;
	}
	int Index = Length - strlen(NamePlace);

	// Wind back to beginning of path
	while (Index != 0 && FileContent[Index] != ' ') {
		--Index;
	}
	if (Index == 0) {
		return NULL;
	}
	++Index;

	// Calculate length of full path, create and fill it
	char *NewlinePlace = strchr(FileContent + Index, '\n');
	if (NewlinePlace == NULL) {
		return NULL;
	}
	int NameLength = Length - Index - strlen(NewlinePlace);
	char *FoundName = malloc(NameLength + 1);
	if (FoundName != NULL) {
		strncpy(FoundName, FileContent + Index, NameLength);
		FoundName[NameLength] = '\0';
	}

	return FoundName;
}

void GTKExtraConnect(G_GNUC_UNUSED GtkBuilder *Builder, GObject *SignalObject,
	const gchar *SignalName, const gchar *HandlerName, GObject *ConnectObject,
	GConnectFlags Flags, gpointer Data) {
	// This function is responsible for connecting functions to GTK signals,
	// with extra passed data
	GCallback Function;
	GCallback *FunctionPointer = &Function;

	// TODO: Use GTKExtraSymbols = g_module_open(NULL, 0); here
	if (GTKExtraSymbols == NULL) {
		char *Name = GTKExtraLibrary("CRewrite");
		GTKExtraSymbols = g_module_open(Name, 0);
		if (Name != NULL) {
			free(Name);
		}
	}

	// Get function from symbols and name, message for missing function
	if (!g_module_symbol(GTKExtraSymbols, HandlerName,
		(gpointer *) FunctionPointer)) {
		if (Function == NULL) {
			printf("Could not find signal handler '%s'.\n", HandlerName);
			return;
		}
	}

	// Connect function to signal
	if (ConnectObject) {
		g_signal_connect_object(SignalObject, SignalName, Function,
			ConnectObject, Flags);
	} else {
		g_signal_connect_data(SignalObject, SignalName, Function,
			Data, NULL, Flags);
	}
}

gboolean GTKExtraNoToggleBefore(GtkToggleButton *Widget) {
	// This function is responsible for setting the name of a toggle
	// button from its active status
	// In pair with GTKExtraNoToggleAfter they stop toggling of toggle buttons
	if (gtk_toggle_button_get_active(Widget)) {
		gtk_widget_set_name((GtkWidget *) Widget, "1");
	} else {
		gtk_widget_set_name((GtkWidget *) Widget, "0");
	}
	return FALSE;
}

gboolean GTKExtraNoToggleAfter(GtkToggleButton *Widget) {
	// This function is responsible for setting the active status of a toggle
	// button from its name
	// In pair with GTKExtraNoToggleBefore they stop toggling of toggle buttons
	const char *Name = gtk_widget_get_name((GtkWidget *) Widget);
	gtk_toggle_button_set_active(Widget, Name[0] == '1');
	return FALSE;
}

gboolean GTKExtraStopScroll(GtkWidget *Widget) {
	// This function is responsible for not letting the widget scroll
	g_signal_stop_emission_by_name(Widget, "scroll-event");
	return FALSE;
}

void GTKExtraNumberLabel(GtkWidget *Label, double Value) {
	// Function to return some nice string balance

	// Postfixes for the text
	// Funny enough, but we use "sizes" not "amounts"
	char PostFix[] = { 0, 'K', 'M', 'G', 'T', 0 };

	// This temporarily holds the text for label
	char Text[16];
	Text[0] = '\0';

	// Some values to help with iteration
	int FirstRun = 10;
	int Index = 0;

	// If the value is negative, we need to switch it for checking
	char *Negative = "";
	if (Value < 0.00) {
		Value *= -1;
		Negative = "-";
	}

	// Loop untill we find small enough value to display nicely
	do {
		// Here we construct the actual text and stop looping
		if (Value < 1000.00) {
			int Offset = -1;
			if (Value == 0.00) {
				sprintf(Text, "0");
			} else if (Value < 0.10) {
				sprintf(Text, (Negative[0] != '\0') ? "-0.1<" : "<0.1");
			} else if ((int) Value < (10 * FirstRun)) {
				sprintf(Text, "%s%.1f%c", Negative, Value, PostFix[Index]);
				Offset = (Negative[0] != '\0') ? 1 : 0;
			}

			// Here we will either make new string or fix the last
			// if statement in previous if - else
			if (Text[0] == '\0' ||
				((Text[2 + Offset] == '0' || Text[3 + Offset] == '0') &&
				Offset != -1)) {
				sprintf(Text, "%s%d%c", Negative, (int) Value, PostFix[Index]);
			}

			// Break out
			break;
		}

		// If we couldn't contruct the text we divide by 1000
		Value = Value / 1000;

		// After first run we have a postfix, therefore we need this
		FirstRun = 1;
	} while (PostFix[++Index] != 0);

	// Set text to label by first finding out what type of widget we have
	if (GTK_IS_LABEL(Label)) {
		gtk_label_set_text((GtkLabel *) Label, Text);
	}
	if (GTK_IS_BUTTON(Label)) {
		gtk_button_set_label((GtkButton *) Label, Text);
	}
	if (GTK_IS_MENU_ITEM(Label)) {
		gtk_menu_item_set_label((GtkMenuItem *) Label, Text);
	}
}

void GTKExtraDestroy(GtkWidget *Widget, G_GNUC_UNUSED gpointer Unused) {
	// This function is responsible for destroying a widget as a GTK signal
	// handler

	gtk_widget_destroy(Widget);
}

gboolean GTKExtraOnEnterNotifyEvent(GtkWidget *Widget) {
	// This function is responsible for selecting a widget on mouse hover

	// Get child, if needed, its parent, select it
	while (!GTK_IS_FLOW_BOX_CHILD(Widget)) {
		Widget = gtk_widget_get_parent(Widget);
	}
	GtkWidget *Parent = gtk_widget_get_parent(Widget);
	gtk_flow_box_select_child((GtkFlowBox *) Parent,
		(GtkFlowBoxChild *) Widget);

	return FALSE;
}

gboolean GTKExtraOnLeaveNotifyEvent(GtkWidget *Widget) {
	// This function is responsible for unselecting a widget when mouse hover
	// stops

	// Get child, if needed, its parent, unselect everything
	while (!GTK_IS_FLOW_BOX_CHILD(Widget)) {
		Widget = gtk_widget_get_parent(Widget);
	}
	GtkWidget *Parent = gtk_widget_get_parent(Widget);
	gtk_flow_box_unselect_all((GtkFlowBox *) Parent);

	return FALSE;
}

void GTKExtraStart(void) {
	// This function is responsible for creating the 0 padding css style
	GTKExtraCss = gtk_css_provider_new();
	gtk_css_provider_load_from_data(GTKExtraCss, "* {padding: 0px;}", -1, NULL);
}

void GTKExtraStop(void) {
	// This function is responsible for destroying the 0 padding css style
	g_object_unref(GTKExtraCss);
}

// Everything under this line is removable after full C conversion
static PyObject *GTKExtraNumberLabelPython(PyObject *Py_UNUSED(
		Self), PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Fill value, LongPointer
	uintptr_t LongPointer;
	double Value;
	if (!PyArg_ParseTuple(Args, "Ld", &LongPointer, &Value)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	GtkWidget *Label = (GtkWidget *) LongPointer;
	GTKExtraNumberLabel(Label, Value);

	// Acquire lock
	PyEval_RestoreThread(Save);

	return PyLong_FromLong(0);
}

// Function names and other python data
static PyMethodDef GTKExtraMethods[] = {
	{"NumberLabel", GTKExtraNumberLabelPython, METH_VARARGS, ""},
	{NULL, NULL, 0, NULL}
};

static struct PyModuleDef GTKExtraModule = {
	PyModuleDef_HEAD_INIT, "GTKExtra", "GTKExtra module", -1, GTKExtraMethods,
	0, 0, 0, 0
};

// The function name has to end with the module name
PyMODINIT_FUNC PyInit_GTKExtra(void) {
	return PyModule_Create(&GTKExtraModule);
}
