/******************************************************************************\
* LBRY-GTK                                                                     *
* Copyright (C) 2021-2023 MorsMortium and Other Contributors                   *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU General Public License as published by         *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                *
* GNU General Public License for more details.                                 *
*                                                                              *
* You should have received a copy of the GNU General Public License            *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
\******************************************************************************/

// Widget file for sidepanel

#include <Python.h>
#include <gtk/gtk.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include "Header/Global.h"
#include "Header/Places.h"
#include "Header/Popup.h"
#include "Header/GTKExtra.h"
#include "Header/SidePanel.h"

SidePanelData *SidePanelCreate(PyObject *Mainer) {
	// Create sidepanel widget

	// Allocate memory for data, abort if NULL
	SidePanelData *Data = malloc(sizeof(SidePanelData));
	if (Data == NULL) {
		abort();
	}

	// Get glade file name, load it in builder
	char WidgetFile[strlen(PlacesGlade) + 20];
	sprintf(WidgetFile, "%sSidePanel.glade", PlacesGlade);
	gtk_builder_add_from_file(GlobalBuilder, WidgetFile, NULL);

	// Set data fields
	Data->SidePanel =
		(GtkWidget *) gtk_builder_get_object(GlobalBuilder, "SidePanel");
	Data->Revealer =
		(GtkWidget *) gtk_builder_get_object(GlobalBuilder, "Revealer");
	Data->ToggleButton =
		(GtkWidget *) gtk_builder_get_object(GlobalBuilder, "SidePanelToggle");

	Data->Mainer = Mainer;

	// Connect signals with data
	gtk_builder_connect_signals_full(GlobalBuilder, GTKExtraConnect, Data);

	return Data;
}

gboolean SidePanelToggleButtonPressEvent(G_GNUC_UNUSED GtkWidget *Widget,
	G_GNUC_UNUSED GdkEventButton *Event, GtkRevealer *Revealer) {
	// Reveals or hides the side panel once SidePanelToggle is clicked

	gtk_revealer_set_reveal_child(Revealer,
		!gtk_revealer_get_child_revealed(Revealer));

	return FALSE;
}

void SidePanelCheck(SidePanelData *Data, GtkButton *Button,
	GdkEventButton *Event, int MouseKey) {
	// Do a call to fetch specified page in new or in same tab

	// If startup isn't done yet, bail with message
	if (GlobalStarted == false) {
		PopupMessage("LBRYNet is not running.");
		return;
	}

	// Get the text from the label of button
	char *ButtonText = (char *) gtk_button_get_label(Button);

	// Make new buffer for text and check there's no spaces
	char Text[strlen(ButtonText) + 1];
	int Symbol = 0, Count = 0;
	while (ButtonText[Symbol] != '\0') {
		// Only add to new buffer if the symbol isn't space
		if (ButtonText[Symbol] != ' ') {
			Text[Count] = ButtonText[Symbol];
			Count++;
		}
		Symbol++;
	}
	Text[Count] = '\0';

	// Get the button event
	if (MouseKey == -1) {
		MouseKey = Event->button;
	}

	// TODO: Yucky python stuff, just throw this to garbage when rewritten

	// Check which callback to use
	char *Tab = (MouseKey == GDK_BUTTON_PRIMARY) ? "SamePage" : NULL;
	Tab = (MouseKey == GDK_BUTTON_MIDDLE) ? "NewPage" : Tab;

	// If neither of the callbacks is specified don't do anything
	if (Tab == NULL) {
		return;
	}

	// Allow function call from any threads
	PyGILState_STATE State = PyGILState_Ensure();

	// Add items to tuple
	PyObject *List = PyList_New(0);
	PyObject *Args = Py_BuildValue("(sO)", Text, List);
	Py_DECREF(List);

	PyObject *Function =
		PyObject_GetAttrString(Data->Mainer, Tab);
	PyObject *Call = PyObject_CallObject(Function, Args);
	if (Call) {
		Py_DECREF(Call);
	} else {
		PyErr_Print();
	}
	Py_DECREF(Args);

	// Disallow function call from any threads
	PyGILState_Release(State);
}

gboolean SidePanelOnButtonPressEvent(GtkWidget *Widget, GdkEventButton *Event,
	SidePanelData *Data) {
	// When any of the panel buttons is pressed

	// Make the call
	SidePanelCheck(Data, (GtkButton *) Widget, Event, -1);

	return FALSE;
}

gboolean SidePanelOnActivate(GtkWidget *Widget, SidePanelData *Data) {
	// When any of the panel buttons gets activated via non-mouse key

	// Make the call
	SidePanelCheck(Data, (GtkButton *) Widget, NULL, GDK_BUTTON_PRIMARY);

	return FALSE;
}

gboolean SidePanelOnDestroy(G_GNUC_UNUSED GtkWidget *Widget,
	SidePanelData *Data) {
	// Free data malloced earlier

	free(Data);

	return FALSE;
}

// Everything under this line is removable after full C conversion
static PyObject *SidePanelCreatePython(PyObject *Py_UNUSED(
		Self), PyObject *Args) {
	// Changes started flag

	PyObject *Mainer;
	if (!PyArg_ParseTuple(Args, "O", &Mainer)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	SidePanelData *Data = SidePanelCreate(Mainer);

	// Acquire lock
	PyEval_RestoreThread(Save);

	PyObject *Dict = PyDict_New();

	PyObject *Object = PyLong_FromVoidPtr((void *) Data->SidePanel);
	PyDict_SetItemString(Dict, "SidePanel", Object);
	Py_DECREF(Object);

	Object = PyLong_FromVoidPtr((void *) Data->Revealer);
	PyDict_SetItemString(Dict, "Revealer", Object);
	Py_DECREF(Object);

	Object = PyLong_FromVoidPtr((void *) Data->ToggleButton);
	PyDict_SetItemString(Dict, "SidePanelToggle", Object);
	Py_DECREF(Object);

	Object = PyLong_FromVoidPtr((void *) Data);
	PyDict_SetItemString(Dict, "Pointer", Object);
	Py_DECREF(Object);

	return Dict;
}

// Function names and other python data
static PyMethodDef SidePanelMethods[] = {
	{"Create", SidePanelCreatePython, METH_VARARGS, ""},
	{NULL, NULL, 0, NULL}
};

static struct PyModuleDef SidePanelModule = {
	PyModuleDef_HEAD_INIT, "SidePanel", "SidePanel module", -1,
	SidePanelMethods, 0, 0, 0, 0
};

// The function name has to end with the module name
PyMODINIT_FUNC PyInit_SidePanel(void) {
	return PyModule_Create(&SidePanelModule);
}
