/******************************************************************************\
* LBRY-GTK                                                                     *
* Copyright (C) 2021-2023 MorsMortium and Other Contributors                   *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU General Public License as published by         *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                *
* GNU General Public License for more details.                                 *
*                                                                              *
* You should have received a copy of the GNU General Public License            *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
\******************************************************************************/

// This file is responsible for connecting with Fill.c and creating markdown
// widget, a base for text in a page

#include <Python.h>
#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include "Header/Parse.h"
#include "Header/Places.h"
#include "Header/Global.h"
#include "Header/GTKExtra.h"
#include "Header/Fill.h"
#include "Header/Markdown.h"

GtkCssProvider *MarkdownCssProvider;
GtkStyleContext *Context;
GdkRGBA MarkdownColor;

gboolean MarkdownOnTextBoxDestroy(G_GNUC_UNUSED GtkWidget *TextBox,
	gpointer Args) {
	// Release memory used by text fields

	MarkdownData *Data = (MarkdownData *) Args;

	g_object_unref(Data->Document);

	if (Data->Text != NULL) {
		free(Data->Text);
	}
	free(Data);

	return FALSE;
}

MarkdownData *MarkdownCreate(char *Text, char *Path, bool IfComment,
	bool IfMarkdown, bool IfResolve, TabsData *TabData, PyObject *AddPage,
	GtkAdjustment *Adjustment) {
	// This function is responsible for creating the Markdown widget

	// Allocate memory for data, abort if NULL
	MarkdownData *Data = malloc(sizeof *Data);
	if (Data == NULL) {
		abort();
	}

	// Get glade file name, load it in builder
	char WidgetFile[strlen(PlacesGlade) + 15];
	sprintf(WidgetFile, "%sMarkdown.glade", PlacesGlade);
	gtk_builder_add_from_file(GlobalBuilder, WidgetFile, NULL);

	// Get widgets used
	Data->Document = (GtkWidget *) gtk_builder_get_object(GlobalBuilder,
			"Document");
	Data->TextBox = (GtkWidget *) gtk_builder_get_object(GlobalBuilder,
			"TextBox");
	Data->Scrolled = (GtkWidget *) gtk_builder_get_object(GlobalBuilder,
			"Scrolled");

	// Increase reference so Replace.c doesn't destroy this
	g_object_ref(Data->Document);

	// Get cursors for text hovering
	GdkDisplay *Display = gdk_display_get_default();
	Data->TextCursor = gdk_cursor_new_from_name(Display, "text");
	Data->PointerCursor = gdk_cursor_new_from_name(Display, "pointer");

	// Pointer to mainspace's adjustment
	Data->Adjustment = Adjustment;

	// TagIndex for currently selected tag in buffer (if any)
	Data->CurrentTagIndex = -1;

	// Some booleans
	Data->IfMarkdown = IfMarkdown;
	Data->IfUpdateHeight = false;
	Data->FirstRun = true;

	// Text to data
	Data->Text = malloc(strlen(Text) + 1);
	if (Data->Text == NULL) {
		free(Data);
		return NULL;
	}
	strcpy(Data->Text, Text);

	// Allocate memory for FillData (if needed)
	if (Data->IfMarkdown) {
		// Create the data struct and insert items to it
		FillData *TextData = FillDataAlloc(IfComment, IfResolve, Path,
				TabData, AddPage);
		if (TextData == NULL) {
			free(Data);
			return NULL;
		}

		// Fill's text data to markdown data object
		// and references parent
		Data->TextData = TextData;
		TextData->Parent = Data;
	}

	// Do other necessary changes to the widget items

	// Add style provider to textbox
	GtkStyleContext *Context = gtk_widget_get_style_context(Data->TextBox);
	gtk_style_context_add_provider(Context,
		(GtkStyleProvider *) MarkdownCssProvider,
		GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);

	// Create a link tag to the buffer
	GtkTextBuffer *Buffer = gtk_text_view_get_buffer(
		(GtkTextView *) Data->TextBox);
	gtk_text_buffer_create_tag(Buffer, "Link", "foreground-rgba",
		&MarkdownColor, "underline", PANGO_UNDERLINE_SINGLE, NULL);

	// Create quote tags to the buffer
	char Quote[10];
	strcpy(Quote, "Quote");
	for (int Level = 1; Level <= 10; Level++) {
		snprintf(Quote + 5, 3, "%d", Level);
		gtk_text_buffer_create_tag(Buffer, Quote, "left-margin",
			Level * 30, NULL);
	}

	// Connect signals with data
	gtk_builder_connect_signals_full(GlobalBuilder, GTKExtraConnect, Data);

	// Decrease signal for textviews
	g_object_connect((GObject *) Data->TextBox, "signal::destroy",
		MarkdownOnTextBoxDestroy, Data, NULL);

	return Data;
}

gboolean MarkdownQueueHeight(G_GNUC_UNUSED GtkWidget *Widget,
	G_GNUC_UNUSED GdkEventConfigure Event, gpointer Args) {
	// Queue MarkdownUpdateHeight to do the update

	bool *IfUpdateHeight = (bool *) Args;
	*IfUpdateHeight = true;

	return FALSE;
}

gboolean MarkdownUpdateHeight(G_GNUC_UNUSED GtkWidget *TextView,
	G_GNUC_UNUSED cairo_t *CR, gpointer Args) {
	// Update widget size (height) once

	MarkdownData *Data = (MarkdownData *) Args;

	if (Data->IfUpdateHeight) {
		Data->IfUpdateHeight = false;
		gtk_widget_queue_resize(Data->TextBox);
	}

	return FALSE;
}

gboolean MarkdownActivate(gpointer Args) {
	// Activates a given link from a texttag

	MarkdownData *Data = (MarkdownData *) Args;

	// Get the table storing our tags
	GtkTextBuffer *Buffer = gtk_text_view_get_buffer(
		(GtkTextView *) Data->TextBox);
	GtkTextTagTable *Table = gtk_text_buffer_get_tag_table(Buffer);

	// Get the used tags
	GtkTextTag *CurrentTag = gtk_text_tag_table_lookup(Table,
			Data->TextData->LinkTags[Data->CurrentTagIndex]);

	// Next will have double underline while last one will get single
	char *Name;
	g_object_get((GObject *) CurrentTag, "name", &Name, NULL);

	// Set the name to textbox
	gtk_widget_set_name(Data->TextBox, Name);

	GdkEvent *Event = gdk_event_new(GDK_BUTTON_RELEASE);
	Event->button.button = (Data->Add) ? GDK_BUTTON_PRIMARY : GDK_BUTTON_MIDDLE;

	// Get the start iter from buffer
	GtkTextIter Start;
	gtk_text_buffer_get_start_iter(Buffer, &Start);

	// Pass the event as the link was clicked by hand
	gtk_text_tag_event(CurrentTag, (GObject *) Data->TextBox, Event, &Start);

	return FALSE;
}

static gboolean MarkdownSelectAction(gpointer Args) {
	// Change selection i.e. change the double underlined tag in buffer

	MarkdownData *Data = (MarkdownData *) Args;

	// Get the table storing our tags
	GtkTextBuffer *Buffer = gtk_text_view_get_buffer(
		(GtkTextView *) Data->TextBox);
	GtkTextTagTable *Table = gtk_text_buffer_get_tag_table(Buffer);

	// Get the needed tags form tag table
	char *Old = Data->TextData->LinkTags[Data->CurrentTagIndex];
	char *New = Data->TextData->LinkTags[Data->CurrentTagIndex + Data->Add];
	GObject *CurrentTag = (GObject *) gtk_text_tag_table_lookup(Table, Old);
	GObject *NextTag = (GObject *) gtk_text_tag_table_lookup(Table, New);

	// Next will have double underline while last one will get single
	g_object_set(CurrentTag, "underline", PANGO_UNDERLINE_SINGLE, NULL);
	g_object_set(NextTag, "underline", PANGO_UNDERLINE_DOUBLE, NULL);

	// Add to CurrentTagIndex
	Data->CurrentTagIndex += Data->Add;

	return FALSE;
}

void MarkdownSelectNext(MarkdownData *Data) {
	// Wraps forward action for tag selection

	if (Data->CurrentTagIndex + 1 < Data->TextData->LinkTagIndexMax) {
		Data->Add = 1;
		g_idle_add((GSourceFunc) MarkdownSelectAction, Data);
	}
}

void MarkdownSelectPrevious(MarkdownData *Data) {
	// Wraps backward action for tag selection

	if (Data->CurrentTagIndex != 0) {
		Data->Add = -1;
		g_idle_add((GSourceFunc) MarkdownSelectAction, Data);
	}
}

gboolean MarkdownOnTextBoxNotifyMotionEvent(GtkWidget *Widget,
	GdkEventMotion *Event, gpointer Args) {
	// Action for pointer hovering over link tags in buffer

	MarkdownData *Data = (MarkdownData *) Args;
	GtkTextView *TextView = (GtkTextView *) Widget;

	// Get the place of pointer relative to buffer
	int PlaceX, PlaceY;
	GtkTextIter Iter;
	gtk_text_view_window_to_buffer_coords(TextView, GTK_TEXT_WINDOW_TEXT,
		Event->x, Event->y, &PlaceX, &PlaceY);
	gboolean IsText = gtk_text_view_get_iter_at_location(TextView, &Iter,
			PlaceX, PlaceY);

	GdkWindow *Window =
		gtk_text_view_get_window(TextView, GTK_TEXT_WINDOW_TEXT);

	// Get the link texttag ready
	GtkTextBuffer *Buffer = gtk_text_view_get_buffer(TextView);
	GtkTextTagTable *Table = gtk_text_buffer_get_tag_table(Buffer);
	GtkTextTag *TextTag = gtk_text_tag_table_lookup(Table, "Link");

	// Check if the iter at location is part of a tag
	if (IsText == TRUE && TextTag != NULL &&
		gtk_text_iter_has_tag(&Iter, TextTag)) {
		GObject *Tag;
		char *Name;

		// Check each tag for one that includes a link to resource
		GSList *List = gtk_text_iter_get_tags(&Iter);
		for (GSList *Tags = List; Tags != NULL; Tags = Tags->next) {
			Tag = Tags->data;
			g_object_get(Tag, "name", &Name, NULL);

			// The tag we have are: "Link", "QuoteN" and some URL
			if (Name && Name[0] != '\0' &&
				strcmp(Name, "Link") != 0 && strncmp(Name, "Quote", 5) != 0) {
				int Index = 0;
				while (Name[Index] != ' ') {
					Index++;
				}

				// Place the tooltip text here and leave
				Name[Index] = '\0';
				gtk_widget_set_tooltip_text(Widget, Name);
				break;
			}
		}
		g_slist_free(List);

		// Set cursor to pointer
		gdk_window_set_cursor(Window, Data->PointerCursor);
	} else {
		// Either not hovering text or no tag found
		gtk_widget_set_name(Widget, "");
		gtk_widget_set_tooltip_text(Widget, "");

		// Set cursor to text
		gdk_window_set_cursor(Window, Data->TextCursor);
	}

	return FALSE;
}

gboolean MarkdownOnTextBoxGrabFocus(G_GNUC_UNUSED GtkWidget *Widget,
	gpointer Args) {
	// Sets scrollplace to struct

	MarkdownData *Data = (MarkdownData *) Args;

	if (Data->Adjustment) {
		Data->ScrollPlace = gtk_adjustment_get_value(Data->Adjustment);
	}

	return FALSE;
}

gboolean MarkdownOnTextBoxFocusInEvent(G_GNUC_UNUSED GtkWidget *Widget,
	gpointer Args) {
	// Sets scrollplace to adjustment (of mainspace)

	MarkdownData *Data = (MarkdownData *) Args;

	if (Data->Adjustment) {
		gtk_adjustment_set_value(Data->Adjustment, Data->ScrollPlace);
	}

	return FALSE;
}

gboolean MarkdownFill(gpointer Args) {
	// Fill the textview widgets

	MarkdownData *Data = (MarkdownData *) Args;

	// This restores FillData once it is freed by destroy event in Fill
	FillData *TextData = Data->TextData;
	if (Data->FirstRun) {
		Data->FirstRun = false;
	} else {
		TextData = FillDataAlloc(TextData->IfComment, TextData->IfResolve,
				TextData->Path, TextData->TabData, TextData->AddPage);

		// References to parent
		TextData->Parent = Data;
	}

	// Increase reference to scrolled and overlay so those won't be destroyed
	GtkWidget *Overlay = gtk_widget_get_parent(Data->Scrolled);
	g_object_ref(Data->Scrolled);

	// Remove overlay from document
	gtk_container_remove((GtkContainer *) Overlay, Data->Scrolled);
	gtk_container_remove((GtkContainer *) Data->Document, Overlay);

	// Add a new overlay to the document
	GtkWidget *NewOverlay = gtk_overlay_new();
	gtk_container_add((GtkContainer *) NewOverlay, Data->Scrolled);
	gtk_container_add((GtkContainer *) Data->Document, NewOverlay);
	g_object_unref(Data->Scrolled);

	// If content isn't supposed to be markdown, then just add the text
	// TODO: Check how to remove Data and all allocated to data
	if (Data->IfMarkdown == false) {
		FillMarkdown((GtkTextView *) Data->TextBox, Data->Text, NULL);
	} else {
		Data->TextData = TextData;

		// Parse markdown text here and free the unparsed text
		if (Data->Text) {
			char *TempText = ParseMarkdown(Data->Text);
			if (TempText) {
				free(Data->Text);
				Data->Text = TempText;
			}
		}

		// Fill textview with text and get the associated structs
		FillMarkdown((GtkTextView *) Data->TextBox,
			Data->Text, &Data->TextData);

		// This determines the CurrentTagIndex
		// With new buffer it'll always be left to 0
		if (Data->TextData->LinkTagIndexMax - 1 < Data->CurrentTagIndex) {
			Data->CurrentTagIndex = Data->TextData->LinkTagIndexMax - 1;
		}
		if (Data->CurrentTagIndex < 0) {
			Data->CurrentTagIndex = 0;
		}

		// Set the adjustment to document if it's specified
		if (Data->Adjustment) {
			double Value = gtk_adjustment_get_lower(Data->Adjustment);
			gtk_adjustment_set_value(Data->Adjustment, Value);
		}

		// Sets TextBox to update its height once per window resize (GTK bug)
		if (Data->TextData->IfComment) {
			g_object_connect((GObject *) GlobalWindow,
				"signal::configure-event", MarkdownQueueHeight,
				&Data->IfUpdateHeight, NULL);
			g_object_connect((GObject *) Data->TextBox,
				"signal::draw", MarkdownUpdateHeight,
				Data, NULL);
		}

		// Destroy signals
		g_object_connect((GObject *) NewOverlay, "signal::destroy",
			FillOnOverlayDestroy, Data->TextData, NULL);
	}

	gtk_widget_show_all(Data->Document);

	return FALSE;
}

void *MarkdownFillThread(void *Args) {
	// Threads the filling part

	g_idle_add((GSourceFunc) MarkdownFill, Args);
	return NULL;
}

void MarkdownStart(void) {
	// Start function for Markdown
	// Makes a css provider and creates RGBA profile for text boxes

	// Get a new css provider
	GError *Error = NULL;
	MarkdownCssProvider = gtk_css_provider_new();
	gtk_css_provider_load_from_data(MarkdownCssProvider,
		"*:not(selection) {background: unset;}", 37, &Error);
	if (Error != NULL) {
		fprintf(stderr, "%s: CssProvider: %s\n", __FUNCTION__,
			Error->message);
		g_error_free(Error);
		exit(1);
	}

	// Get a new style context and set widget path to it
	GtkWidgetPath *Path = gtk_widget_path_new();
	Context = gtk_style_context_new();
	gtk_widget_path_append_type(Path, GTK_TYPE_BUTTON);
	gtk_style_context_set_path(Context, Path);

	// Get colour for from context
	gtk_style_context_get_color(Context, GTK_STATE_FLAG_LINK, &MarkdownColor);

	gtk_widget_path_unref(Path);
}

void MarkdownStop(void) {
	// Stop function for Markdown

	g_object_unref(Context);
	g_object_unref(MarkdownCssProvider);
}

// Everything under this line is removable after full C conversion
static PyObject *MarkdownFillPython(PyObject *Self, PyObject *Py_UNUSED(Args)) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	const char *Text = NULL, *Path = NULL;
	PyObject *Dict = PyModule_GetDict(Self);
	PyObject *Object = PyDict_GetItemString(Dict, "Pointer");
	uintptr_t Pointer = PyLong_AsLongLong(Object);

	Object = PyDict_GetItemString(Dict, "Text");
	if (Object) {
		Text = PyUnicode_AsUTF8(Object);
	}
	Object = PyDict_GetItemString(Dict, "Path");
	if (Object) {
		Path = PyUnicode_AsUTF8(Object);
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	MarkdownData *Data = (MarkdownData *) Pointer;
	char *NewPath;
	if (Text && strcmp(Data->Text, Text) != 0) {
		if (strlen(Data->Text) < strlen(Text)) {
			free(Data->Text);
			Data->Text = malloc(strlen(Text) + 1);
		}
		strcpy(Data->Text, Text);
	}

	if (Path && Path[0] != '\0') {
		NewPath = (char *) malloc(strlen(Path) + 1);
		if (NewPath != NULL) {
			if (Data->TextData->Path) {
				free(Data->TextData->Path);
			}
			strcpy(NewPath, Path);
			Data->TextData->Path = NewPath;
		}
	}

	pthread_t Thread;
	pthread_create(&Thread, NULL, MarkdownFillThread, Data);
	pthread_detach(Thread);

	// Acquire lock
	PyEval_RestoreThread(Save);

	Py_RETURN_NONE;
}

static PyObject *MarkdownActivatePython(PyObject *Self, PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0
	int Add;
	if (!PyArg_ParseTuple(Args, "i", &Add)) {
		return NULL;
	}

	PyObject *Dict = PyModule_GetDict(Self);
	PyObject *Object = PyDict_GetItemString(Dict, "Pointer");
	uintptr_t Pointer = PyLong_AsLongLong(Object);

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	MarkdownData *Data = (MarkdownData *) Pointer;
	Data->Add = Add;

	g_idle_add((GSourceFunc) MarkdownActivate, Data);

	// Acquire lock
	PyEval_RestoreThread(Save);

	Py_RETURN_NONE;
}

static PyObject *MarkdownSelectNextPython(PyObject *Self, PyObject *Py_UNUSED(
		Args)) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	PyObject *Dict = PyModule_GetDict(Self);
	PyObject *Object = PyDict_GetItemString(Dict, "Pointer");
	uintptr_t Pointer = PyLong_AsLongLong(Object);

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	MarkdownSelectNext((MarkdownData *) Pointer);

	// Acquire lock
	PyEval_RestoreThread(Save);

	Py_RETURN_NONE;
}

static PyObject *MarkdownSelectPreviousPython(PyObject *Self,
	PyObject *Py_UNUSED(Args)) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	PyObject *Dict = PyModule_GetDict(Self);
	PyObject *Object = PyDict_GetItemString(Dict, "Pointer");
	uintptr_t Pointer = PyLong_AsLongLong(Object);

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	MarkdownSelectPrevious((MarkdownData *) Pointer);

	// Acquire lock
	PyEval_RestoreThread(Save);

	Py_RETURN_NONE;
}

static PyMethodDef MarkdownModMethods[] = {
	{"Fill", (PyCFunction) MarkdownFillPython, METH_NOARGS, "Fill"},
	{"Activate", MarkdownActivatePython, METH_VARARGS, "Activate"},
	{"SelectNext", (PyCFunction) MarkdownSelectNextPython, METH_NOARGS,
	 "Select Next"},
	{"SelectPrevious", (PyCFunction) MarkdownSelectPreviousPython, METH_NOARGS,
	 "Select Previous"},
	{NULL, NULL, 0, NULL}
};

static struct PyModuleDef MarkdownModModule = {
	PyModuleDef_HEAD_INIT, "MarkdownMod", "Markdown moded module", -1,
	MarkdownModMethods, 0, 0, 0, 0
};

PyObject *MarkdownModuleCreatePython(MarkdownData *Data) {
	// Creates python module from given data

	PyObject *Module = PyModule_Create(&MarkdownModModule);
	PyObject *Object;

	Object = PyLong_FromVoidPtr((void *) Data);
	if (Object == NULL) {
		return Module;
	}
	PyModule_AddObject(Module, "Pointer", Object);

	Object = PyLong_FromVoidPtr((void *) Data->Document);
	if (Object == NULL) {
		return Module;
	}
	PyModule_AddObject(Module, "Document", Object);

	Object = PyLong_FromVoidPtr((void *) Data->TextBox);
	if (Object == NULL) {
		return Module;
	}
	PyModule_AddObject(Module, "TextBox", Object);

	Object = PyUnicode_FromString(Data->Text);
	if (Object == NULL) {
		return Module;
	}
	PyModule_AddObject(Module, "Text", Object);

	Object = PyUnicode_FromString("");
	if (Object == NULL) {
		return Module;
	}
	PyModule_AddObject(Module, "Path", Object);

	return Module;
}

static PyObject *MarkdownCreatePython(PyObject *Py_UNUSED(Self),
	PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0
	char *Text, *Path;
	int IfComment, IfMarkdown, IfResolve;
	PyObject *AddPage;
	uintptr_t AdjustmentPointer, TabPointer;
	if (!PyArg_ParseTuple(Args, "ssiiiLOL", &Text, &Path, &IfComment,
		&IfMarkdown, &IfResolve, &TabPointer, &AddPage, &AdjustmentPointer)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	MarkdownData *Data = MarkdownCreate(Text, Path, (bool) IfComment,
			(bool) IfMarkdown, (bool) IfResolve, (TabsData *) TabPointer,
			AddPage,
			(GtkAdjustment *) AdjustmentPointer);

	// Acquire lock
	PyEval_RestoreThread(Save);

	return MarkdownModuleCreatePython(Data);
}

// Function names and other python data
static PyMethodDef MarkdownMethods[] = {
	{"Create", MarkdownCreatePython, METH_VARARGS, "Create"},
	{NULL, NULL, 0, NULL}
};

static struct PyModuleDef MarkdownModule = {
	PyModuleDef_HEAD_INIT, "Markdown", "Markdown module", -1, MarkdownMethods,
	0, 0, 0, 0
};

// The function name has to end with the module name
PyMODINIT_FUNC PyInit_Markdown(void) {
	return PyModule_Create(&MarkdownModule);
}
