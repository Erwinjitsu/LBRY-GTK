/******************************************************************************\
* LBRY-GTK                                                                     *
* Copyright (C) 2021-2023 MorsMortium and Other Contributors                   *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU General Public License as published by         *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                *
* GNU General Public License for more details.                                 *
*                                                                              *
* You should have received a copy of the GNU General Public License            *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
\******************************************************************************/

// This file is responsible for operating system specific parts

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <signal.h>
#include <fcntl.h>
#include <unistd.h>
#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <dbghelp.h>
#else
#include <malloc.h>
#include <execinfo.h>
#endif

#include "Header/OS.h"
#include "Header/Places.h"

#ifdef _WIN32
static unsigned short OSWindowsStacktrace(HANDLE *Process, SYMBOL_INFO *Symbol,
	size_t Size, void *Traces[Size]) {
	// Initialize symbols to process and get the raw stacktrace

	*Process = GetCurrentProcess();
	SymInitialize(*Process, NULL, TRUE);

	// Add needed symbol struct members
	Symbol->MaxNameLen = MAX_SYM_NAME;
	Symbol->SizeOfStruct = sizeof(SYMBOL_INFO);

	return CaptureStackBackTrace(0, Size, Traces, NULL);
}
#endif

void OSSIGHandler(int Signal) {
	// Catches signals and prints stacktrace before exit(1)

	int bytes = -1;
	size_t Size = 100;
	void *Traces[Size];
	char LogPath[strlen(PlacesState) + 20];

	// Make the log path
	strcpy(LogPath, PlacesState);
	strcpy(LogPath + strlen(PlacesState), "Trace.log");

	// Open log path, if not just default to standard error
	int FileDescriptor = open(LogPath, O_APPEND | O_CREAT | O_WRONLY, 0644);
	if (FileDescriptor == -1) {
		FileDescriptor = STDERR_FILENO;
	}

	// Segfault and abort included, add others as needed
	if (Signal == SIGSEGV) {
		bytes = write(FileDescriptor, "\nTerminated with SIGSEGV\n", 25);
		bytes = write(STDERR_FILENO, "Segmentation fault\n", 19);
	} else if (Signal == SIGABRT) {
		bytes = write(FileDescriptor, "\nTerminated with SIGABRT\n", 25);
		bytes = write(STDERR_FILENO, "IOT Instruction\n", 16);
	}

	if (bytes > -1) {
		bytes = write(FileDescriptor, "Stacktrace:\n", 12);
#ifdef _WIN32

		// Get raw stacktrace and initialize symbols
		HANDLE Process = 0;
		char SymbolBuffer[sizeof(SYMBOL_INFO) + MAX_SYM_NAME * sizeof(TCHAR)];
		SYMBOL_INFO *Symbol = (SYMBOL_INFO *) SymbolBuffer;
		unsigned short Frames = OSWindowsStacktrace(&Process, Symbol, Size,
				Traces);

		// Print stacktrace
		char SymbolAddressBuffer[16];
		for (int Index = 0; Index < Frames; Index++) {
			SymFromAddr(Process, (DWORD64) (Traces[Index]), 0, Symbol);
			_i64toa(Symbol->Address, SymbolAddressBuffer, 16);

			// Write custom stacktrace
			bytes = write(FileDescriptor, Symbol->Name, Symbol->NameLen);
			bytes = write(FileDescriptor, " - 0x", 5);
			bytes = write(FileDescriptor, SymbolAddressBuffer,
					strlen(SymbolAddressBuffer));
			bytes = write(FileDescriptor, "\n", 1);
		}
#else
		Size = backtrace(Traces, Size);

		// Prints all the frames
		backtrace_symbols_fd(Traces, Size, FileDescriptor);
#endif
	}

	if (FileDescriptor != STDERR_FILENO) {
		close(FileDescriptor);
	}

	// Finally exit out with error
	exit(1);
}

void OSMemoryTrimmer(void) {
	// This function trims the "fat" off of the free'd memory

#ifdef __linux__
#ifdef __GNUC__
	malloc_trim(0);
#endif
#endif
}

char *OSBBacktrace(void) {
	// Gets the stacktrace

	// Create trace pointer array, with max size
	int Size = 100;
	void *Traces[Size];
	bool TraceHad = false;
	size_t TraceSize = 1;

	// Get traces and convert them to strings
#ifdef _WIN32

	// Get raw stacktrace and initialize symbols
	HANDLE Process = 0;
	char SymbolBuffer[sizeof(SYMBOL_INFO) + MAX_SYM_NAME * sizeof(TCHAR)];
	SYMBOL_INFO *Symbol = (SYMBOL_INFO *) SymbolBuffer;
	unsigned short Frames = OSWindowsStacktrace(&Process, Symbol, Size, Traces);

	// Parse stacktrace into trace strings
	char TraceStrings[Frames][300];
	int AddSize;
	for (int Index = 0; Index < Frames; Index++) {
		SymFromAddr(Process, (DWORD64) (Traces[Index]), 0, Symbol);
		AddSize = snprintf(TraceStrings[Index], 300 - 1, "%s - 0x%0llX",
				Symbol->Name, Symbol->Address);
		if (AddSize > 0) {
			TraceSize += AddSize;
		}
	}

	TraceHad = (Frames > 0);
	Size = (int) Frames;
#else

	Size = backtrace(Traces, Size);
	char **TraceStrings = backtrace_symbols(Traces, Size);

	// If there are trace strings, calculate the full size of them
	if (TraceStrings != NULL) {
		for (int Index = 1; Index < Size; ++Index) {
			TraceSize += strlen(TraceStrings[Index]) + 1;
		}
		TraceHad = true;
	}
#endif

	// Create string keeping every trace string
	char FullTrace[TraceSize];
	FullTrace[0] = '\0';

	// If there are trace strings, copy them into the unified string
	if (TraceHad) {
		for (int Index = 1; Index < Size; ++Index) {
			strcat(FullTrace, TraceStrings[Index]);
			strcat(FullTrace, "\n");
		}
#ifdef __unix__

		// Free malloced strings
		free(TraceStrings);
#endif
	}

	// Finally allocate space for return value and copy the trace over
	char *ResultTrace = malloc(TraceSize + 110);
	if (ResultTrace == NULL) {
		abort();
	}
	strcpy(ResultTrace, FullTrace);

	return ResultTrace;
}

char *OSAbsolutePath(char *Arg0) {
	// This function is responsible for getting the absolute path to binary
	// For uniformity
	char *Absolute = malloc(2);
	sprintf(Absolute, " ");

#ifdef _WIN32

	// Windows arg 0 contains absolute path, make it malloced for uniformity
	Absolute = realloc(Absolute, strlen(Arg0) + 1);
	sprintf(Absolute, "%s", Arg0);
#endif

#ifdef __unix__

	// "Unuse" Arg0 to take care of compile warnings
	(void) (Arg0);

	// Get proc file for executable
	FILE *File = fopen("/proc/self/maps", "r");

	// Get length of string reaching first line with lbry-gtk binary
	int Length = 1;

	// Letters in file for binary
	char Name[9] = {'l', 'b', 'r', 'y', '-', 'g', 't', 'k', '\n'};
	int Index = 0;

	do {
		// Read single character
		char Character = fgetc(File);

		if (Character == Name[Index]) {
			// If character is correct
			if (Index == 8) {
				// If character is last, exit
				break;
			} else {
				// If not, go to next character
				++Index;
			}
		} else {
			// If not, start over
			Index = 0;
		}
		++Length;
	} while (true);

	// Get string ending with binary, close file
	char Line[Length];
	rewind(File);
	for (int i = 0; i < Length - 1; ++i) {
		Line[i] = fgetc(File);
	}
	Line[Length - 1] = '\0';
	fclose(File);

	// Remove everything before last space (including space)
	char *Space = strrchr(Line, ' ');
	memmove(Line, Space + 1, strlen(Space));

	// Make path malloced
	Absolute = realloc(Absolute, strlen(Line) + 1);
	sprintf(Absolute, "%s", Line);
#endif

	return Absolute;
}
