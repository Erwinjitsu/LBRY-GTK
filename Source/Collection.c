/******************************************************************************\
* LBRY-GTK                                                                     *
* Copyright (C) 2021-2023 MorsMortium and Other Contributors                   *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU General Public License as published by         *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                *
* GNU General Public License for more details.                                 *
*                                                                              *
* You should have received a copy of the GNU General Public License            *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
\******************************************************************************/

// This file deals with collection resolving

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <jansson.h>
#include <Python.h>

#include "Header/Request.h"
#include "Header/Filter.h"
#include "Header/Url.h"
#include "Header/Json.h"
#include "Header/Collection.h"

json_t *CollectionResolve(json_t *Params, int PageSize, int Page,
	char *Server) {
	// This function resolves a collection

	// Check for default or empty values
	JsonClear(Params);

	// Set all the values that will always be present
	json_object_set_new(Params, "page_size", json_integer(PageSize));
	json_object_set_new(Params, "page", json_integer(Page));

	// Dump resulting parameters object
	char *ParamStr = json_dumps(Params, 0);
	json_decref(Params);

	// Make the request
	char Request[strlen(ParamStr) + 55];
	sprintf(Request, "{\"method\": \"collection_resolve\", \"params\": %s }",
		ParamStr);

	// Free dumped string
	free(ParamStr);

	// Get result
	json_t *Result = RequestJson(Server, Request);
	if (json_object_get(Result, "error") != NULL) {
		return Result;
	}

	// URL things happen here then
	UrlThumbnail(Result, Server);

	return FilterPublications(Result);
}

// Everything under this line is removable after full C conversion
static PyObject *CollectionResolvePython(PyObject *Py_UNUSED(
		Self), PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Get arguments and pass it to the pythonless function
	PyObject *Unicode;
	int PageSize, Page;
	char *Server;
	if (!PyArg_ParseTuple(Args, "Uiis", &Unicode, &PageSize, &Page, &Server)) {
		return NULL;
	}

	char *Text = (char *) PyUnicode_AsUTF8(Unicode);

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	// Convert Text into json
	json_error_t Error;
	json_t *Params = json_loads(Text, 0, &Error);

	json_t *JsonData = CollectionResolve(Params, PageSize, Page, Server);

	// Acquire lock
	PyEval_RestoreThread(Save);

	return JsonToString(JsonData);
}

// Function names and other python data
static PyMethodDef CollectionMethods[] = {
	{"Resolve", CollectionResolvePython, METH_VARARGS, "Resolve a collection"},
	{NULL, NULL, 0, NULL}
};

static struct PyModuleDef CollectionModule = {
	PyModuleDef_HEAD_INIT, "Collection", "Collection module", -1,
	CollectionMethods, 0, 0, 0, 0
};

// The function name has to end with the module name
PyMODINIT_FUNC PyInit_Collection(void) {
	return PyModule_Create(&CollectionModule);
}
