/******************************************************************************\
* LBRY-GTK                                                                     *
* Copyright (C) 2021-2023 MorsMortium and Other Contributors                   *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU General Public License as published by         *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                *
* GNU General Public License for more details.                                 *
*                                                                              *
* You should have received a copy of the GNU General Public License            *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
\******************************************************************************/

// This file is responsible for managing the Language widget

#include <Python.h>
#include <gtk/gtk.h>
#include <stdbool.h>
#include <jansson.h>
#include <stdio.h>
#include <string.h>

#include "Header/Global.h"
#include "Header/Places.h"
#include "Header/Tag.h"
#include "Header/GTKExtra.h"
#include "Header/Language.h"

// Liststore for completing languages
GtkListStore *LanguageModel;

LanguageData *LanguageCreate(TabsData *TabData) {
	// This function is responsible for creating the Language widget

	// Allocate memory for data, abort if NULL
	LanguageData *Data = malloc(sizeof(LanguageData));
	if (Data == NULL) {
		abort();
	}

	// Create Tagger
	Data->Tagger = TagCreate(true, TabData, NULL);

	// Get glade file name, load it in builder
	char WidgetFile[strlen(PlacesGlade) + 15];
	sprintf(WidgetFile, "%sLanguage.glade", PlacesGlade);
	gtk_builder_add_from_file(GlobalBuilder, WidgetFile, NULL);

	// Get widgets used
	Data->Language = (GtkWidget *) gtk_builder_get_object(GlobalBuilder,
			"Language");
	Data->Combo = (GtkWidget *) gtk_builder_get_object(GlobalBuilder, "Combo");
	Data->FlowBox = Data->Tagger->FlowBox;

	// Set Taggers LanguageEntry, add tag to Language, hide Taggers entry
	GtkWidget *Entry = (GtkWidget *) gtk_builder_get_object(GlobalBuilder,
			"Entry");
	Data->Tagger->LanguageEntry = Entry;
	gtk_box_pack_start((GtkBox *) Data->Language, Data->Tagger->Tag, true, true,
		0);
	gtk_widget_hide(Data->Tagger->Entry);

	// Add completion model for entry and combobox
	GtkEntryCompletion *Completion =
		(GtkEntryCompletion *) gtk_builder_get_object(GlobalBuilder,
			"Completion");
	gtk_entry_completion_set_model(Completion, (GtkTreeModel *) LanguageModel);
	gtk_combo_box_set_model((GtkComboBox *) Data->Combo,
		(GtkTreeModel *) LanguageModel);

	// Connect signals with data
	gtk_builder_connect_signals_full(GlobalBuilder, GTKExtraConnect, Data);

	return Data;
}

void LanguageKeybindHelper(LanguageData *Data, int Button) {
	// This function is responsible for clicking add on keybind left clicks and
	// opening the combobox on keybind middle clicks
	if (Button == 1) {
		gtk_combo_box_popup((GtkComboBox *) Data->Combo);
	} else {
		TagOnAddClicked(NULL, Data->Tagger);
	}
}

gboolean LanguageOnEntryKeyPressEvent(GtkWidget *Widget, GdkEventKey *Event,
	LanguageData *Data) {
	// This function is responsible for forwarding Language entry keypresses to
	// the tagger entry

	TagOnEntryKeyPressEvent(Widget, Event, Data->Tagger);

	// For allowing handling of other key presses
	return FALSE;
}

// Struct for LanguageEntryForeach
typedef struct LanguageEntryData {
	// Text written into Entry
	const char *Text;

	// Index of row that is the same as Text
	int Index;
} LanguageEntryData;

gboolean LanguageEntryForeach(GtkTreeModel *Model, GtkTreePath *Path,
	GtkTreeIter *Iter, gpointer GotData) {
	// This function is responsible for comparing the Entry text with a single
	// language row, and stopping the comparing of all row, if same

	LanguageEntryData *Data = (LanguageEntryData *) GotData;

	// Get row text
	char *Text;
	gtk_tree_model_get(Model, Iter, 0, &Text, -1);

	// If the two are the same, get index, and close execution
	if (strcmp(Data->Text, Text) == 0) {
		Data->Index = gtk_tree_path_get_indices(Path)[0];
		free(Text);
		return TRUE;
	}

	free(Text);

	// Continue execution
	return FALSE;
}

gboolean LanguageOnEntryChanged(GtkWidget *Widget, LanguageData *Data) {
	// This function is responsible for setting Tagger entry text, if the
	// Language entry text is the same as one of the rows in LanguageModel

	// Empty Tagger entry, get Language entry text
	gtk_entry_set_text((GtkEntry *) Data->Tagger->Entry, "");
	const char *NewLanguage = gtk_entry_get_text((GtkEntry *) Widget);

	// Create data for LanguageEntryForeach
	LanguageEntryData EntryData[1];
	EntryData->Text = NewLanguage;
	EntryData->Index = -1;

	// Check all rows in LanguageModel
	gtk_tree_model_foreach((GtkTreeModel *) LanguageModel, LanguageEntryForeach,
		EntryData);

	// If row is found
	if (EntryData->Index != -1) {
		// Choose row returned
		gtk_combo_box_set_active((GtkComboBox *) Data->Combo, EntryData->Index);

		// Get length of text, up to '/' character, create variable and fill it
		int Length = strstr(NewLanguage, "/") - NewLanguage;
		char FinalLanguage[Length + 1];
		strncpy(FinalLanguage, NewLanguage, Length);
		FinalLanguage[Length] = '\0';

		// Set Tagger entry text
		gtk_entry_set_text((GtkEntry *) Data->Tagger->Entry, FinalLanguage);
	}
	return FALSE;
}

gboolean LanguageOnLanguageDestroy(G_GNUC_UNUSED GtkWidget *Widget,
	LanguageData *Data) {
	// This function is responsible for freeing all data, after widget is
	// destroyed

	free(Data);
	return FALSE;
}

void LanguageStart(void) {
	// This function is responsible for starting everything used by the file

	// Load json containing every language
	char LanguagesFile[strlen(PlacesJson) + 15];
	sprintf(LanguagesFile, "%sLanguages.json", PlacesJson);
	json_error_t Error;
	json_t *Languages = json_load_file(LanguagesFile, 0, &Error);

	// Create list store for text completion
	LanguageModel = gtk_list_store_new(1, G_TYPE_STRING);

	size_t Index;
	json_t *JsonValue;

	// Fill LanguageModel with languages
	json_array_foreach(Languages, Index, JsonValue) {
		// Get full line in Code/Name format
		const char *Code = json_string_value(json_array_get(JsonValue, 0));
		const char *Name = json_string_value(json_array_get(JsonValue, 1));
		char Full[strlen(Code) + strlen(Name) + 2];
		sprintf(Full, "%s/%s", Code, Name);

		// Add to LanguageModel
		GtkTreeIter Iter;
		gtk_list_store_append(LanguageModel, &Iter);
		gtk_list_store_set(LanguageModel, &Iter, 0, Full, -1);
	}

	// Free json
	json_decref(Languages);
}

void LanguageStop(void) {
	// This function is responsible for stopping everything used by the file

	// Free model
	g_object_unref(LanguageModel);
}

// Everything under this line is removable after full C conversion
static PyObject *LanguageCreatePython(PyObject *Py_UNUSED(Self),
	PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	uintptr_t LongPointer = 0;
	if (!PyArg_ParseTuple(Args, "L", &LongPointer)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	LanguageData *Data = LanguageCreate((TabsData *) LongPointer);

	// Acquire lock
	PyEval_RestoreThread(Save);

	PyObject *Dict = PyDict_New();

	PyObject *Object = PyLong_FromVoidPtr((void *) Data->Language);
	PyDict_SetItemString(Dict, "Language", Object);
	Py_DECREF(Object);

	Object = PyLong_FromVoidPtr((void *) Data->Combo);
	PyDict_SetItemString(Dict, "Combo", Object);
	Py_DECREF(Object);

	Object = PyLong_FromVoidPtr((void *) Data->FlowBox);
	PyDict_SetItemString(Dict, "FlowBox", Object);
	Py_DECREF(Object);

	Object = PyLong_FromVoidPtr((void *) Data->Tagger);
	PyDict_SetItemString(Dict, "TaggerPointer", Object);
	Py_DECREF(Object);

	Object = PyLong_FromVoidPtr((void *) Data);
	PyDict_SetItemString(Dict, "Pointer", Object);
	Py_DECREF(Object);

	return Dict;
}

static PyObject *LanguageKeybindHelperPython(PyObject *Py_UNUSED(
		Self), PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Fill DataPointer, Button with sent data
	uintptr_t DataPointer, Button;
	if (!PyArg_ParseTuple(Args, "LL", &DataPointer, &Button)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	LanguageKeybindHelper((LanguageData *) DataPointer, (int) Button);

	// Acquire lock
	PyEval_RestoreThread(Save);

	return PyLong_FromLong(0);
}

// Function names and other python data
static PyMethodDef LanguageMethods[] = {
	{"Create", LanguageCreatePython, METH_VARARGS, ""},
	{"KeybindHelper", LanguageKeybindHelperPython, METH_VARARGS, ""},
	{NULL, NULL, 0, NULL}
};

static struct PyModuleDef LanguageModule = {
	PyModuleDef_HEAD_INIT, "Language", "Language module", -1, LanguageMethods,
	0, 0, 0, 0
};

// The function name has to end with the module name
PyMODINIT_FUNC PyInit_Language(void) {
	return PyModule_Create(&LanguageModule);
}
