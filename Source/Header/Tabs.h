#ifndef TABS_H_
#define TABS_H_

#include <pthread.h>

#include "List.h"
#include "Replace.h"
#include "Threads.h"
#include "State.h"
#include "Status.h"
#include "Document.h"

// Do something in-between thread locks
#define TabsLockDo(Tab, Something) \
		pthread_mutex_lock(&Tab->Lock); \
		Something; \
		pthread_mutex_unlock(&Tab->Lock);

// Gets reference to given data in tab data
// Useful for threaded work
#define TabsGet(Tab, Key, Value) \
		TabsLockDo(Tab, Value = &Tab->Key)

// Sets the GettingThread value
#define TabsExecutingSet(Tab, Value) \
		TabsLockDo(Tab, Tab->GettingThread = Value)

// TabsData struct with data specific to specific Tab
typedef struct TabsData {
	// DocumentData from Document.c
	DocumentData Documenter;

	// ListData from List.c
	ListData Lister;

	// ReplaceData from Replace.c
	ReplaceData Replacer;

	// StatusData from Status.c
	StatusData Statuser;

	// ThreadsData from Threads.c
	ThreadsData Threader;

	// StateData from State.c
	StateData *Stater;

	// Linked list navigation
	struct TabsData *Next;
	struct TabsData *Previous;

	// Boolean to tell if some thread is still doing work
	bool GettingThread;

	// Thread lock for threaded work
	pthread_mutex_t Lock;
} TabsData;

// Global array of tabs
extern TabsData *TabsList;

// Checks if thread is still being executed
bool TabsIsExecuting(TabsData *TabData);

#endif
