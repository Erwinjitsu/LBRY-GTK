#ifndef COLLECTION_H_
#define COLLECTION_H_

#include <jansson.h>

// This function resolves a collection
json_t *CollectionResolve(json_t *Params, int PageSize, int Page,
	char *Server);

#endif
