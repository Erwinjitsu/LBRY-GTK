#ifndef PLACES_H_
#define PLACES_H_

extern char *PlacesBin, *PlacesState;
extern char *PlacesRoot, *PlacesShare, *PlacesLBRYGTK, *PlacesImage, *PlacesTmp;
extern char *PlacesHelp, *PlacesJson, *PlacesGlade, *PlacesConfig, *PlacesCache;

// This function is responsible for filling every system folder used
void PlacesStart(char *GotCurrent);

// This function is responsible for freeing every string created this file
void PlacesStop(void);

#endif
