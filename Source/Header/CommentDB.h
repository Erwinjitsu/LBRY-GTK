#ifndef COMMENTDB_H_
#define COMMENTDB_H_

#include <pthread.h>
#include <jansson.h>

// Change comment table in the lbry-gtk database
void CommentDBChange(char *Comment, char *ClaimID, char *CommentID,
	char *ChannelID, unsigned int Timestamp, int Option);

// Import the history of made comments
void *CommentDBImport(int MaxRecords, char *CommentServer, char *Server);

// List all comments in the database
json_t *CommentDBList(int Page, int PageSize);

#endif
