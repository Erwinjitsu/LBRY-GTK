#ifndef FILL_H_
#define FILL_H_

#include <Python.h>
#include <gtk/gtk.h>
#include <stdbool.h>
#include <jansson.h>

#include "Tabs.h"
#include "Markdown.h"

// Items to keep in tag checking
extern const char *FillKeptTags[];

// Tables to check HTML character references
// Always wrapped in refrence start (&) and end (;) characters
extern const char *FillReferences[];
extern const char FillCharacters[];

typedef struct FillTag {
	// Name of the tag i.e. "a" or "/a"
	char *Name;

	// Option inside tag denoted by href or src
	char *Option;

	// True if end tag else false
	bool EndTag;

	// Index of the tag place in outputted string array
	int Place;

	// Index or reference to place in the same tag array
	int Index;

	// Offset caused by unicode characters and char refs
	int Offset;

	// Reference to a tag pair to this one
	struct FillTag *PairInfo;
} FillTag;

typedef struct FillData {
	// Tells whether the buffer is of comment or not
	bool IfComment;

	// To open a document with external program
	bool IfResolve;

	// Path to file location
	char *Path;

	// Calculated width/height of image
	unsigned int ImageWidth;
	unsigned int ImageHeight;

	// Links in texttags used for key navigation
	char **LinkTags;

	// The index of TextTag in buffer
	int LinkTagIndex;

	// The total amount of link tags
	int LinkTagIndexMax;

	// The total amount of data structs
	int RotateMax;

	// The total amount of html tags in TagInfo array
	int TagAmount;

	// Points to element in TagInfo array
	FillTag *TagInfo;

	// Points to first element in TagInfo array
	FillTag *TagInfos;

	// Points to first element in Data array
	struct FillData *Data;

	// Markdown.c's object that is the direct parent to this struct
	MarkdownData *Parent;

	// GObject holder, to disconnect all signals
	GObject *Object;

	// Gtk item holder used in signal or where needed
	GtkWidget *Item;

	// If we are using a thread
	bool IsThreaded;

	// Python method to add page
	PyObject *AddPage;

	// Tabs data struct
	TabsData *TabData;

	// Lock for threaded work
	pthread_mutex_t ThreadLock;
} FillData;

// Allocates memory for FillData struct
FillData *FillDataAlloc(bool IfComment, bool IfResolve, char *Path,
	TabsData *TabData, PyObject *AddPage);

// Constructor for FillData - checks the given text and fills the textbuffer
// from TextView object given to the function
void FillMarkdown(GtkTextView *TextView, char *Text, FillData **GotData);

// Constructor for FillTag - checks the given text for tags and makes a tag
// array accordingly, also filling out buffer with "clean" text
FillTag *FillTagsGet(char *Text, char *Out, int *TagAmount);

// All of the GTK callback functions used for singals and idle updates

// When buffer is destroyed, frees the allocated memory
gboolean FillOnOverlayDestroy(GtkTextView *TextView, gpointer Args);

// Draws a line in the textbuffer
gboolean FillHRDraw(GtkWidget *TextView, cairo_t *CR, gpointer Args);

// Draws a separator for quoted text
gboolean FillQuoteDraw(GtkWidget *TextView, cairo_t *CR, gpointer Args);

// On link click, checks the link and opens it the appropriate way
gboolean FillLinkClick(GtkTextTag *Tag, GtkWidget *Widget, GdkEvent *Event,
	GtkTextIter *Iter, FillData *Data);

// Updates the image and anchors it to the textbuffer
gboolean FillImageUpdate(gpointer GotData);

#endif
