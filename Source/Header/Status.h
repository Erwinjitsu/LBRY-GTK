#ifndef STATUS_H_
#define STATUS_H_

#include <gtk/gtk.h>
#include <stdbool.h>
#include <jansson.h>

#include "Replace.h"

// Struct for widget data
typedef struct StatusData {
	// Subwidgets of Status used in and outside of this file, title of page
	GtkWidget *Status, *Progress, *FlowBox, *Title;

	// Address of server
	char *Server;

	// Got status from Connect
	json_t *GotStatus;

	// Mutex guarding removal of GotStatus
	pthread_mutex_t Lock;

	// Data for replacing page
	ReplaceData *Replacer;

	// Signal for callback to stop execution and free data, status of client,
	// whether StatusUpdate runs
	bool Exit, LBRYGTK, Running;
} StatusData;

// This function is responsible for creating the Status widget
StatusData *StatusCreate(GtkWidget *Title, TabsData *TabData);

// This function is responsible for displaying the widget on the page
gboolean StatusDisplay(gpointer GotData);

#endif
