#ifndef PARSE_H_
#define PARSE_H_

#define PCRE2_CODE_UNIT_WIDTH 8
#include <pcre2.h>

extern pcre2_code *ParseRemove, *ParseNoP1, *ParseNoP2, *ParseNoP3;
extern pcre2_code *ParseMDUrl, *ParseUrl, *ParseProperty;

// This function is responsible for the full conversion of a markdown text
// into a version understandable by the client
char *ParseMarkdown(char *Markdown);

// This function is responsible for compiling every regex used in the file
void ParseInit(void);

#endif
