#ifndef IMAGE_H_
#define IMAGE_H_

#include <gtk/gtk.h>
#include <stdbool.h>
#include <pthread.h>

#include "Limit.h"

// Forbidden characters, when converting urls to filenames
extern char ImageForbidden[];

// List of image types supported by GTK
extern char **ImageTypes;
extern int ImageTypesLength;

// Limiters for conversion, animation and magickwand operations
extern LimitData *ImageConvertLimit, *ImageAnimationLimit, *ImageWandLimit;

// Empty thumbnail placeholders
extern GdkPixbuf *ImageUser, *ImageMissing;

// In which list the app is
extern int ImageListCount;

// Lock guarding ImageListCount
extern pthread_mutex_t ImageListLock;

// Struct for cross thread data
typedef struct ImageData {
	// Url, file path
	char *Url, *Path;

	// Image to set file on
	GtkImage *Image;

	// Desired max resolution
	long Width;
	long Height;

	// If the image is a channel thumbnail or not, if it needs circling/rounding
	bool Channel, Transform;

	// Real resolution of file
	int RealWidth, RealHeight;

	// Signaling if the image was tried once
	bool Retry;

	// In which list the image is needed
	int ListCount;

	// Pixbuf or animation to set Image with, loader to keep animation
	GdkPixbuf *Pixbuf;
	GdkPixbufAnimation *Animation;
	GdkPixbufLoader *Loader;
} ImageData;

// Increase ListCount
void ImageIncreaseCount(void);

// This function is responsible for binding image download and display
void *ImageGet(void *GotData);

// This function is responsible for downloading an image and returning it's
char *ImageDownload(char *Url, bool Force);

// This function is responsible for getting the resolution of an image
void ImageResolution(char *FilePath, int *Resolution);

// This function is responsible for clearing the image and then sending the
// data for download/filling
void ImageUrl(char *Url, GtkImage *Image, long Width, long Height, bool Channel,
	bool Transform);

// This function is responsible for converting a file url into an absolute
char *ImagePath(char *Url);

// This function is responsible for filling a GtkImage with either a
void *ImageFile(void *GotData);

// This function is responsible for freeing an ImageData struct instance
void ImageFree(ImageData *Data);

// Allocates memory for the used image
ImageData *ImageAlloc(GtkImage *Image, long Width, long Height, bool IsChannel,
	bool IsTransform);

// This function is responsible for initing everything used by the file
void ImageStart(void);

// This function is responsible for cleaning up everything used by the file
void ImageStop(void);

#endif
