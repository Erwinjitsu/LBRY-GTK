#ifndef CONNECTDB_H_
#define CONNECTDB_H_

#include <stdbool.h>
#include <pthread.h>
#include <sqlite3.h>

// Lock for all database actions
extern pthread_mutex_t ConnectDBLock;

// Holds the information of us running or not
extern bool ConnectDBNoDB;

// This struct is used for thread arguments
typedef struct ConnectDBArgs {
	pthread_t Thread;
	char *Server;
	char *CommentServer;
	char *ChannelUrl;
	char *ChannelName;
	char *ClaimID;
	int MaxRecords;
	unsigned int Start;
	unsigned int End;
} ConnectDBArgs;

// Make connection to lbry-gtk database
sqlite3 *ConnectDBOpen(void);

// Wait for threads to finish their work
void ConnectDBJoin(int ThreadNumber, ConnectDBArgs *CInfo);

// Init lock for DB
void ConnectDBStart(void);

// Destroy lock for DB
void ConnectDBStop(void);

#endif
