#ifndef DOCUMENT_H_
#define DOCUMENT_H_

#include <gtk/gtk.h>

#include "Markdown.h"

typedef struct DocumentData {
	char *Name;
	GtkLabel *Title;
	MarkdownData *Markdown;
} DocumentData;

// Gtk function for document
gboolean DocumentDisplay(gpointer Args);

#endif
