#ifndef FILE_H_
#define FILE_H_

#include <jansson.h>

// This function lists downloaded files
json_t *FileList(void *Unused, int PageSize, int Page, char *Server);

#endif
