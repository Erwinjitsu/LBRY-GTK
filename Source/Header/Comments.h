#ifndef COMMENTS_H_
#define COMMENTS_H_

#include <jansson.h>

// Format used by comment server
extern char *CommentsRPCFormat;

// Signature data used for signing sent comment data
typedef struct CommentsSignature {
	char Signature[130];
	char SigningTS[12];
} CommentsSignature;

// This function gets the comment related settings
json_t *CommentsSettingGet(char *ChannelID, char *CommentServer);

// This function sets a reaction for comment
json_t *CommentsReactionPost(char *ChannelName, char *ChannelID,
	char *CommentServer, char *CommentIDs,
	char *ReactionType, int Remove, char *Server);

// This lists reactions to given comment(s)
json_t *CommentsReactionList(char *ChannelName, char *ChannelID,
	char *CommentServer, char *CommentIDs,
	char *Server);

// This function lists comments
json_t *CommentsList(char *ChannelName, char *ChannelID, char *ParentID,
	char *AuthorClaimID, char *ClaimID, char *CommentServer,
	int SortBy, int PageSize, int Page, char *Server);

// This function posts a comment
json_t *CommentsPost(char *ChannelName, char *ChannelID, char *ParentID,
	char *ClaimID, char *CommentServer, unsigned char *Comment,
	char *SupportTXID, char *Server);

// This function updates previously made comment
json_t *CommentsUpdate(char *ChannelName, char *ChannelID, char *CommentServer,
	unsigned char *Comment, char *CommentID, char *Server);

// This function deletes made comment
json_t *CommentsDelete(char *ChannelName, char *ChannelID, char *CommentServer,
	char *CommentID, char *Server);

#endif
