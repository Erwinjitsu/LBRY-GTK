/******************************************************************************\
* LBRY-GTK                                                                     *
* Copyright (C) 2021-2023 MorsMortium and Other Contributors                   *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU General Public License as published by         *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                *
* GNU General Public License for more details.                                 *
*                                                                              *
* You should have received a copy of the GNU General Public License            *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
\******************************************************************************/

// This file is responsible for getting image files, from cache, or internet,
// converting them to proper formats, getting their resolution

// POSIX.1-2008 compatibility
#define _XOPEN_SOURCE 700

#include <Python.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <curl/curl.h>
#include <MagickWand/MagickWand.h>
#include <gtk/gtk.h>
#include <jansson.h>
#include <time.h>

#include "Header/Places.h"
#include "Header/Request.h"
#include "Header/Limit.h"
#include "Header/Json.h"
#include "Header/Global.h"
#include "Header/Image.h"

// Forbidden characters, when converting urls to filenames
char ImageForbidden[] = { '<', '>', ':', '"', '\\', '/', '|', '?', '*', '\0' };

// List of image types supported by GTK
char **ImageTypes;
int ImageTypesLength;

// Limiters for conversion, animation and magickwand operations
LimitData *ImageConvertLimit, *ImageAnimationLimit, *ImageWandLimit;

// Empty thumbnail placeholders
GdkPixbuf *ImageUser, *ImageMissing;

// In which list the app is
int ImageListCount;

// Lock guarding ImageListCount
pthread_mutex_t ImageListLock;

bool ImageConvert(char *FilePath) {
	// This function is responsible for converting a file into GTK compatible
	// type and returning if it was able to do it (in a different process)

	// Increase counter, if possible, exit otherwise
	if (LimitIncrease(ImageConvertLimit)) {
		return false;
	}

	char *Arguments[ImageTypesLength + 3];

	// Executable, file to convert
	char Executable[strlen(PlacesBin) + 17];
	sprintf(Executable, "%slbry-gtk-convert", PlacesBin);
	Arguments[0] = Executable;
	Arguments[1] = FilePath;

	// File types supported by GTK
	for (int Index = 0; Index < ImageTypesLength; ++Index) {
		Arguments[Index + 2] = ImageTypes[Index];
	}
	Arguments[ImageTypesLength + 2] = NULL;

	// Call executable
	char *Output;
	g_spawn_sync(NULL, Arguments, NULL, G_SPAWN_STDERR_TO_DEV_NULL, NULL, NULL,
		&Output, NULL, NULL, NULL);

	// Check what it printed, free it, decrease counter
	bool Return = Output[0] == '1';
	free(Output);
	LimitDecrease(ImageConvertLimit);

	return Return;
}

char *ImagePath(char *Url) {
	// This function is responsible for converting a file url into an absolute
	// file path

	// If Url is NULL or empty, return NULL
	if (Url == NULL || *Url == '\0') {
		return NULL;
	}

	// Write url into local variable
	char Buffer[strlen(Url) + 1];
	sprintf(Buffer, "%s", Url);

	// Go through every character in url, if it is forbidden, replace with '_'
	for (int Index = 0; Index < (int) strlen(Buffer); ++Index) {
		int ForbiddenIndex = 0;
		while (ImageForbidden[ForbiddenIndex] != '\0') {
			if (Buffer[Index] == ImageForbidden[ForbiddenIndex]) {
				Buffer[Index] = '_';
				break;
			}
			++ForbiddenIndex;
		}
	}

	// Add cache location to path
	char *FilePath = malloc(strlen(PlacesCache) + strlen(Buffer) + 1);
	if (FilePath != NULL) {
		sprintf(FilePath, "%s%s", PlacesCache, Buffer);
	}
	return FilePath;
}

char *ImageDownload(char *Url, bool Force) {
	// This function is responsible for downloading an image and returning it's
	// path on success or NULL on failure

	// Get path, return NULL
	char *FilePath = ImagePath(Url);
	if (FilePath == NULL) {
		return NULL;
	}

	if (access(FilePath, F_OK) != 0 || Force) {
		// If file does not exist or forced download (retry)

		// If download and convert succeeds, return path
		if (RequestFile(Url, FilePath) && ImageConvert(FilePath)) {
			return FilePath;
		}
	} else {
		// If file exists

		// Retry 100 times in a second, if image gets downloaded by then and is
		// a proper image, return path
		int Retries = 0;
		while (Retries < 100) {
			if (ImageConvert(FilePath)) {
				return FilePath;
			} else {
				// Sleeping for 0.01 second
				struct timespec Remaining, Request = { 0, 10000000 };
				nanosleep(&Request, &Remaining);
			}
			++Retries;
		}

		// Try to download and convert it a last time
		if (RequestFile(Url, FilePath) && ImageConvert(FilePath)) {
			return FilePath;
		}
	}

	// On failure
	free(FilePath);
	return NULL;
}

void ImageResolution(char *FilePath, int *Resolution) {
	// This function is responsible for getting the resolution of an image

	// Set default resolution
	Resolution[0] = -1;
	Resolution[1] = -1;

	// Enter lock, create wand, in case of failure exit lock and return
	if (FilePath == NULL || LimitIncrease(ImageWandLimit)) {
		return;
	}
	MagickWand *Wand = NewMagickWand();
	if (Wand == NULL) {
		LimitDecrease(ImageWandLimit);
		return;
	}

	// Get image data
	MagickBooleanType Status = MagickPingImage(Wand, FilePath);

	// Set resolution
	if (Status == MagickTrue) {
		Resolution[0] = MagickGetImageWidth(Wand);
		Resolution[1] = MagickGetImageHeight(Wand);
	}

	// Destroy wand, exit lock
	DestroyMagickWand(Wand);
	LimitDecrease(ImageWandLimit);
}

int ImageFrames(char *FilePath) {
	// This function is responsible for getting the number of frames of an
	// animation

	// Enter lock, create wand, in case of failure exit lock and return
	if (FilePath == NULL || LimitIncrease(ImageWandLimit)) {
		return 1;
	}
	MagickWand *Wand = NewMagickWand();
	if (Wand == NULL) {
		LimitDecrease(ImageWandLimit);
		return 1;
	}

	// Get image data
	MagickBooleanType Status = MagickPingImage(Wand, FilePath);

	int Frames = 1;

	// Set resolution
	if (Status == MagickTrue) {
		Frames = MagickGetNumberImages(Wand);
	}

	// Destroy wand, exit lock
	DestroyMagickWand(Wand);
	LimitDecrease(ImageWandLimit);

	return Frames;
}

void ImagePixel(int UNUSED(PointX), int PointY, int DistanceX, int CircleY,
	int RadiusSquared, int FullX, int Stride, guchar *Pixels) {
	// This function is responsible for checking if a single pixel is inside a
	// circle, and make it transparent (with simple anti-aliasing) if not

	// Check if point is outside circle
	int DistanceY = PointY - CircleY;
	int Distance = DistanceX * DistanceX + DistanceY * DistanceY -
		RadiusSquared;

	// If outside the circle calculate aliasing
	if (Distance > 0) {
		// The opacity is the square of the normalized distance from the edge of
		// the circle (8 bit resolution)
		int NormalizedDistance = (256 * Distance) / RadiusSquared;
		int Transparency = NormalizedDistance * NormalizedDistance;

		if (Transparency > 255) {
			Transparency = 255;
		}
		int OriginalOpacity = Pixels[FullX + PointY * Stride + 3];
		Pixels[FullX + PointY * Stride + 3] = OriginalOpacity *
			(255 - Transparency) / 255;
	}
}

void ImageCorner(int Width, int Height, double Percent, int Channels,
	int Stride, guchar *Pixels) {
	// This function is responsible for only checking needed pixels for both
	// circling and rounding

	// Get radius of circle from shorter side and rounding percent
	int Radius = ((Height < Width) ? Height : Width) * Percent;
	int RadiusSquared = Radius * Radius;

	// Go through every pixel of every corner of image
	// These notations assume the origin is top left

	// Left
	for (int X = 0; X < Radius; ++X) {
		int FullX = X * Channels;
		int DistanceX = X - Radius;

		// Top corner
		for (int Y = 0; Y < Radius - X; ++Y) {
			ImagePixel(X, Y, DistanceX, Radius, RadiusSquared, FullX, Stride,
				Pixels);
		}

		// Bottom corner
		for (int Y = Height - 1; Height - Radius + X <= Y; --Y) {
			ImagePixel(X, Y, DistanceX, Height - Radius - 1, RadiusSquared,
				FullX, Stride, Pixels);
		}
	}

	// Right
	for (int X = Width - 1; Width - Radius <= X; --X) {
		int FullX = X * Channels;
		int DistanceX = X - (Width - Radius - 1);

		// Top corner
		for (int Y = 0; Y < Radius - (Width - X); ++Y) {
			ImagePixel(X, Y, DistanceX, Radius, RadiusSquared, FullX, Stride,
				Pixels);
		}

		// Bottom corner
		for (int Y = Height - 1; Height - Radius + (Width - X) <= Y; --Y) {
			ImagePixel(X, Y, DistanceX, Height - Radius - 1, RadiusSquared,
				FullX, Stride, Pixels);
		}
	}
}

GdkPixbuf *ImageSquare(GdkPixbuf *Pixbuf, int Width, int Height, bool UnRef) {
	// This function is responsible for resizing a pixbuf to a wanted size, with
	// cutting of extra, on wrong aspect ratios

	// Get smaller dimension, create pixbuf with it
	int Size = ((Height < Width) ? Height : Width);
	GdkPixbuf *SquarePixbuf =
		gdk_pixbuf_new(GDK_COLORSPACE_RGB, TRUE, 8, Size, Size);

	// Copy original into it
	gdk_pixbuf_copy_area(Pixbuf, (Width - Size) / 2, (Height - Size) / 2, Size,
		Size, SquarePixbuf, 0, 0);

	// Delete original if needed
	if (UnRef) {
		g_object_unref(Pixbuf);
	}
	return SquarePixbuf;
}

GdkPixbuf *ImageContent(GdkPixbuf *Pixbuf) {
	// This function is responsible for cutting an image to content/removing
	// no alpha strips from all sides

	// If pixbuf does not have alpha channel, it can not have 0 alpha strips
	if (!gdk_pixbuf_get_has_alpha(Pixbuf)) {
		return Pixbuf;
	}

	// Get format and raw data
	int Channels = gdk_pixbuf_get_n_channels(Pixbuf);
	int Stride = gdk_pixbuf_get_rowstride(Pixbuf);
	guchar *Pixels = gdk_pixbuf_get_pixels(Pixbuf);

	// Get original dimensions
	int StartX = 0, EndX = gdk_pixbuf_get_width(Pixbuf);
	int StartY = 0, EndY = gdk_pixbuf_get_height(Pixbuf);
	int Width = EndX, Height = EndY;

	// Scan through lines, until first not zero alpha pixel found
	// These notations assume the origin is top left

	// Left to right
	bool Exit = false;
	for (int IndexX = 0; IndexX < EndX; ++IndexX) {
		for (int IndexY = 0; IndexY < EndY; ++IndexY) {
			if (Pixels[IndexX * Channels + IndexY * Stride + 3] != 0) {
				Exit = true;
				break;
			}
		}
		if (Exit) {
			break;
		}
		StartX = IndexX + 1;
	}

	// Right to left
	Exit = false;
	for (int IndexX = EndX - 1; 0 <= IndexX; --IndexX) {
		for (int IndexY = 0; IndexY < EndY; ++IndexY) {
			if (Pixels[IndexX * Channels + IndexY * Stride + 3] != 0) {
				Exit = true;
				break;
			}
		}
		if (Exit) {
			break;
		}
		EndX = IndexX;
	}

	// Top to bottom
	Exit = false;
	for (int IndexY = 0; IndexY < EndY; ++IndexY) {
		for (int IndexX = StartX; IndexX < EndX; ++IndexX) {
			if (Pixels[IndexX * Channels + IndexY * Stride + 3] != 0) {
				Exit = true;
				break;
			}
		}
		if (Exit) {
			break;
		}
		StartY = IndexY + 1;
	}

	// Bottom to top
	Exit = false;
	for (int IndexY = EndY - 1; 0 <= IndexY; --IndexY) {
		for (int IndexX = StartX; IndexX < EndX; ++IndexX) {
			if (Pixels[IndexX * Channels + IndexY * Stride + 3] != 0) {
				Exit = true;
				break;
			}
		}
		if (Exit) {
			break;
		}
		EndY = IndexY;
	}

	if (StartX != 0 || StartY != 0 || EndX != Width || EndY != Height) {
		// If some dimension changed

		// Get final size
		Width = EndX - StartX;
		Height = EndY - StartY;

		// Create and empty new pixbuf of correct size
		GdkPixbuf *NewPixbuf = gdk_pixbuf_new(GDK_COLORSPACE_RGB, true, 8,
				Width, Height);
		gdk_pixbuf_fill(NewPixbuf,  0x00000000);

		// Copy from old pixbuf, free it, return new
		gdk_pixbuf_copy_area(Pixbuf, StartX, StartY, Width, Height, NewPixbuf,
			0, 0);
		g_object_unref(Pixbuf);
		return NewPixbuf;
	}

	return Pixbuf;
}

GdkPixbuf *ImageTransform(GdkPixbuf *Pixbuf, int Width, int Height, bool Type,
	bool UnRef) {
	// This function is responsible for circling or rounding a pixbuf

	GdkPixbuf *AlphaPixbuf = Pixbuf;

	// Add alpha to pixbuf, free original, if needed
	if (!gdk_pixbuf_get_has_alpha(Pixbuf)) {
		AlphaPixbuf = gdk_pixbuf_add_alpha(Pixbuf, FALSE, 0, 0, 0);
		if (UnRef) {
			g_object_unref(Pixbuf);
		}
	}

	// Get format and raw data
	int Channels = gdk_pixbuf_get_n_channels(AlphaPixbuf);
	int Stride = gdk_pixbuf_get_rowstride(AlphaPixbuf);
	guchar *Pixels = gdk_pixbuf_get_pixels(AlphaPixbuf);

	if (Type) {
		// Circling
		ImageCorner(Width, Height, 0.5, Channels, Stride, Pixels);
	} else {
		// Rounding
		ImageCorner(Width, Height, 0.1, Channels, Stride, Pixels);
	}

	return AlphaPixbuf;
}

void ImageSizes(int Width, int Height, int Size, int *Result) {
	// This function is responsible for calculating minimum rectangle resolution
	// from which to cut out a square

	// Get minimum resolution for square
	int Minimum = (Height < Width) ? Height : Width;

	// Get ratio between size and minimum
	float Ratio = (float) Size / (float) Minimum;

	// Calculate final resolution
	Result[0] = (int) round(Ratio * Width);
	Result[1] = (int) round(Ratio * Height);
}

float ImageRatio(int Width, int Height, int RealWidth, int RealHeight) {
	// This function is responsible for getting ration for bounding box

	// Get ratios between real sizes and wanted
	float WidthRatio = (float) Width / (float) RealWidth;
	float HeightRatio = (float) Height / (float) RealHeight;

	// Return smaller
	return (WidthRatio < HeightRatio) ? WidthRatio : HeightRatio;
}

GdkPixbuf *ImagePixbuf(int Width, int Height, int WantedWidth, int WantedHeight,
	bool Channel, bool Transform, bool Path, void *Data) {
	// This function is responsible for fully preparing a pixbuf, either from a
	// filepath or from a pre existing pixbuf

	// Load pixbuf
	GdkPixbuf *Pixbuf = NULL;
	if (Path) {
		// If Data is a path load pixbuf from that
		Pixbuf = gdk_pixbuf_new_from_file((char *) Data, NULL);

		// If loading fails, return null
		if (!GDK_IS_PIXBUF(Pixbuf)) {
			return NULL;
		}

		// Cut to content
		Pixbuf = ImageContent(Pixbuf);

		// Recalculate width and height
		Width = gdk_pixbuf_get_width(Pixbuf);
		Height = gdk_pixbuf_get_height(Pixbuf);
	} else {
		// If data is a pixbuf, use it
		Pixbuf = (GdkPixbuf *) Data;
	}

	// If it is channel, change wanted size to minimum square
	if (Channel && Width != Height) {
		// Get minimum size
		int Size = (WantedWidth < WantedHeight) ? WantedWidth : WantedHeight;

		// Calculate big rectangle size (minimum size to cut square)
		int Result[2];
		ImageSizes(Width, Height, Size, Result);
		WantedWidth = Result[0];
		WantedHeight = Result[1];
	}

	// Get bounding box ratio, scale both sides with it
	float Ratio = ImageRatio(WantedWidth, WantedHeight, Width, Height);
	WantedWidth = Width * Ratio;
	WantedHeight = Height * Ratio;

	// Scale to correct size, while keeping aspect ratio
	GdkPixbuf *NewPixbuf = gdk_pixbuf_scale_simple(Pixbuf, WantedWidth,
			WantedHeight, GDK_INTERP_BILINEAR);

	// Replace original, only delete if path (not animation frame)
	if (Path) {
		g_object_unref(Pixbuf);
	}
	Pixbuf = NewPixbuf;

	// Make it square if needed
	if (Channel && Width != Height) {
		Pixbuf = ImageSquare(Pixbuf, WantedWidth, WantedHeight, true);
	}

	if (Pixbuf == NULL) {
		return NULL;
	}

	// If it needs circling or rounding, do it
	if (Transform) {
		// Reget size
		Width = gdk_pixbuf_get_width(Pixbuf);
		Height = gdk_pixbuf_get_height(Pixbuf);
		Pixbuf = ImageTransform(Pixbuf, Width, Height, Channel, true);
	}

	return Pixbuf;
}

void ImageFree(ImageData *Data) {
	// This function is responsible for freeing an ImageData struct instance
	if (Data->Url != NULL) {
		free(Data->Url);
	}
	if (Data->Path != NULL) {
		free(Data->Path);
	}
	free(Data);
}

ImageData *ImageAlloc(GtkImage *Image, long Width, long Height, bool IsChannel,
	bool IsTransform) {
	// Allocates memory for the used image

	ImageData *Data = calloc(1, sizeof(ImageData));
	if (Data == NULL) {
		return NULL;
	}

	// Set some ready values
	Data->Image = Image;
	Data->Width = Width;
	Data->Height = Height;
	Data->Channel = IsChannel;
	Data->Transform = IsTransform;

	// Lock and set list count
	pthread_mutex_lock(&ImageListLock);
	Data->ListCount = ImageListCount;
	pthread_mutex_unlock(&ImageListLock);

	return Data;
}

static gboolean ImageSetAnimation(gpointer GotData) {
	// This function is responsible for creating animation from preloaded data

	ImageData *Data = (ImageData *) GotData;

	// Check if list got replaced, exit if so
	pthread_mutex_lock(&ImageListLock);
	bool Replaced = Data->ListCount != ImageListCount;
	pthread_mutex_unlock(&ImageListLock);
	if (Replaced) {
		ImageFree(Data);
		return FALSE;
	}

	// Set image with animation
	if (GTK_IS_IMAGE(Data->Image)) {
		gtk_image_set_from_animation(Data->Image, Data->Animation);
	}

	if (Data->Loader != NULL) {
		g_object_unref(Data->Loader);
	} else {
		g_object_unref(Data->Animation);
	}

	ImageFree(Data);

	return FALSE;
}

void ImageGetAnimation(void *GotData) {
	// This function is responsible for getting animation data, in a thread
	ImageData *Data = (ImageData *) GotData;

	// Increase counter, if possible, exit otherwise
	if (LimitIncrease(ImageAnimationLimit)) {
		ImageFree(Data);
		return;
	}

	// Calculate either minimum rectangle for square or minimum size
	// (animation can not load at size)
	if (Data->Channel) {
		// Calculate final width and height
		int Size = (Data->Width < Data->Height) ? Data->Width : Data->Height;

		// Calculate big rectangle size (minimum size to cut square)
		int Result[2];
		ImageSizes(Data->RealWidth, Data->RealHeight, Size, Result);
		Data->Width = Result[0];
		Data->Height = Result[1];
	} else {
		// Get ratio
		float Ratio = ImageRatio(Data->Width, Data->Height, Data->RealWidth,
				Data->RealHeight);

		// Scale both sides with it
		Data->Width = Data->RealWidth * Ratio;
		Data->Height = Data->RealHeight * Ratio;
	}

	// Open image
	FILE *File = fopen(Data->Path, "r");
	if (File != NULL) {
		// Get length of file
		fseek(File, 0, SEEK_END);
		int Length = ftell(File);
		rewind(File);

		char *Buffer = malloc(Length + 1);
		if (Buffer == NULL) {
			fclose(File);
			ImageFree(Data);
		} else {
			// Get data from file, close it
			int Read = fread(Buffer, 1, Length, File);
			Buffer[Length] = '\0';
			fclose(File);
			if (Read != Length) {
				return;
			}

			// Create loader (supports animations), set resolution
			GdkPixbufLoader *Loader = gdk_pixbuf_loader_new();
			gdk_pixbuf_loader_set_size(Loader, Data->Width, Data->Height);

			// Write data into Loader, close it
			gdk_pixbuf_loader_write(Loader, (unsigned char *) Buffer, Length,
				NULL);
			gdk_pixbuf_loader_close(Loader, NULL);

			// Get animation
			GdkPixbufAnimation *Animation = gdk_pixbuf_loader_get_animation(
				Loader);

			if (Animation == NULL) {
				//Faulty animation

				if (!Data->Retry) {
					// Retry once
					Data->Retry = true;
				} else {
					// Set placeholder
					free(Data->Url);
					Data->Url = NULL;
				}

				// Reget animation or placeholder, decrease counter
				ImageGet(Data);
				g_object_unref(Loader);
				LimitDecrease(ImageAnimationLimit);
				return;
			}

			// Check if pixbuf needs resizing
			bool Resize = Data->Width != Data->RealWidth
				|| Data->Height != Data->RealHeight;

			// If either resizing or transforming needed we have to make a new
			// animation
			if (Resize || Data->Transform) {
				GTimeVal Time;

				// Get iterator for going through animation, fixed delay between
				// frames
				Time.tv_sec = 0;
				Time.tv_usec = 0;
				GdkPixbufAnimationIter *Iter =
					gdk_pixbuf_animation_get_iter(Animation, &Time);
				int Delay = gdk_pixbuf_animation_iter_get_delay_time(Iter);

				// Revert size after original animation is loaded with proper
				// size and
				// resize check happened, if it is a channel thumbnail
				if (Data->Channel) {
					int Minimum =
						(Data->Width <
						Data->Height) ? Data->Width : Data->Height;
					Data->Width = Minimum;
					Data->Height = Minimum;
				}

				// Create animation with correct dimensions, set it to loop
				GdkPixbufSimpleAnim *NewAnimation =
					gdk_pixbuf_simple_anim_new(Data->Width, Data->Height,
						1000.00 / Delay);
				gdk_pixbuf_simple_anim_set_loop(NewAnimation, TRUE);

				// Get number of frames in animation
				int Frames = ImageFrames(Data->Path);

				for (int Index = 0; Index < Frames; ++Index) {
					// Go through every frame

					// Get pixbuf
					GdkPixbuf *Pixbuf = gdk_pixbuf_animation_iter_get_pixbuf(
						Iter);

					// Fully process pixbuf (scale/circling/rounding)
					Pixbuf =
						ImagePixbuf(Data->RealWidth, Data->RealHeight,
							Data->Width,
							Data->Height, Data->Channel, Data->Transform, false,
							Pixbuf);

					// If not faulty, add to animation
					if (Pixbuf != NULL) {
						gdk_pixbuf_simple_anim_add_frame(NewAnimation, Pixbuf);
					}

					// Go forward in animation
					g_time_val_add(&Time, Delay * 1000);
					Delay = gdk_pixbuf_animation_iter_get_delay_time(Iter);
					gdk_pixbuf_animation_iter_advance(Iter, &Time);
				}

				Animation = (GdkPixbufAnimation *) NewAnimation;
				g_object_unref(Iter);
				g_object_unref(Loader);
				Data->Loader = NULL;
			} else {
				Data->Loader = Loader;
			}

			Data->Animation = Animation;

			g_idle_add_full(G_PRIORITY_LOW, ImageSetAnimation, Data, NULL);
		}
	} else {
		ImageFree(Data);
	}

	// Decrease counter
	LimitDecrease(ImageAnimationLimit);
}

static gboolean ImageSetPixbuf(gpointer GotData) {
	// Set an image with a pixbuf, if possible, free data

	ImageData *Data = GotData;

	// Check if list got replaced, exit if so
	pthread_mutex_lock(&ImageListLock);
	bool Replaced = Data->ListCount != ImageListCount;
	pthread_mutex_unlock(&ImageListLock);
	if (Replaced) {
		ImageFree(Data);
		return FALSE;
	}

	if (Data->Pixbuf != NULL) {
		if (GTK_IS_IMAGE(Data->Image)) {
			gtk_image_set_from_pixbuf(Data->Image, Data->Pixbuf);
		}
		g_object_unref(Data->Pixbuf);
	}
	ImageFree(Data);

	return FALSE;
}

void *ImageFile(void *GotData) {
	// This function is responsible for filling a GtkImage with either a
	// downloaded image or a channel/regular placeholder

	// Faulty image or no image as default
	bool PlaceHolder = true;
	ImageData *Data = GotData;

	// If image name exists
	if (Data->Path != NULL) {
		// Get actual size and format of image
		GdkPixbufFormat *Format =
			gdk_pixbuf_get_file_info(Data->Path, &Data->RealWidth,
				&Data->RealHeight);
		char LocalPath[strlen(Data->Path) + 5];

		// If the image is faulty, read gif version
		if (Data->RealWidth == -1) {
			sprintf(LocalPath, "%s.gif", Data->Path);
			Format =
				gdk_pixbuf_get_file_info(LocalPath, &Data->RealWidth,
					&Data->RealHeight);
		} else {
			sprintf(LocalPath, "%s", Data->Path);
		}
		if (Data->RealWidth != -1 && Format != NULL) {
			// Get format string
			char *Name = gdk_pixbuf_format_get_name(Format);

			// No failed images or faulty ones at this point
			PlaceHolder = false;

			if (strcmp(Name, "gif") != 0) {
				// If file is not gif

				// Prepare size/circling/rounding
				GdkPixbuf *Pixbuf =
					ImagePixbuf(Data->RealWidth, Data->RealHeight, Data->Width,
						Data->Height, Data->Channel, Data->Transform, true,
						LocalPath);

				if (Pixbuf != NULL) {
					// Load pixbuf, if not corrupt
					Data->Pixbuf = Pixbuf;
					g_idle_add_full(G_PRIORITY_LOW, ImageSetPixbuf, Data, NULL);
				} else if (!Data->Retry) {
					// Corrupt pixbuf, retry it once
					Data->Retry = true;
					ImageGet(Data);
				} else {
					// Corrupt pixbuf, no more retries
					PlaceHolder = true;
				}
			} else {
				// If file is gif
				// Actual path of image
				free(Data->Path);
				Data->Path = malloc(strlen(LocalPath) + 1);
				if (Data->Path != NULL) {
					// Get image data
					sprintf(Data->Path, "%s", LocalPath);
					ImageGetAnimation(Data);
				} else {
					PlaceHolder = true;
				}
			}
			free(Name);
		}
	}

	// If image does not exist or is faulty
	if (PlaceHolder) {
		// Calculate minimum square
		int Minimum = (Data->Width < Data->Height) ? Data->Width : Data->Height;

		// Switch icon depending on whether it is for a channel or not
		GdkPixbuf *Loaded = ImageMissing;
		if (Data->Channel) {
			Loaded = ImageUser;
		}

		// Scale and set icon
		Data->Pixbuf = gdk_pixbuf_scale_simple(Loaded, Minimum, Minimum,
				GDK_INTERP_BILINEAR);
		g_idle_add_full(G_PRIORITY_LOW, ImageSetPixbuf, Data, NULL);
	}
	return NULL;
}

void *ImageGet(void *GotData) {
	// This function is responsible for binding image download and display
	ImageData *Data = (ImageData *) GotData;

	// Free original path if retrying
	if (Data->Retry) {
		free(Data->Path);
	}

	// Download image, set path
	Data->Path = ImageDownload(Data->Url, Data->Retry);

	// Display image
	ImageFile(Data);
	return NULL;
}

void ImageUrl(char *Url, GtkImage *Image, long Width, long Height, bool Channel,
	bool Transform) {
	// This function is responsible for clearing the image and then sending the
	// data for download/filling

	// Clear image
	gtk_image_clear(Image);

	// Create and fill data
	ImageData *Data = ImageAlloc(Image, Width, Height, Channel, Transform);
	if (Data == NULL) {
		return;
	}

	if (Url != NULL) {
		Data->Url = malloc(strlen(Url) + 1);
		if (Data->Url != NULL) {
			sprintf(Data->Url, "%s", Url);
		}
	}

	// Download and fill, if image exists, otherwise only fill
	pthread_t Thread;
	if (Data->Url != NULL) {
		pthread_create(&Thread, NULL, ImageGet, Data);
	} else {
		pthread_create(&Thread, NULL, ImageFile, Data);
	}
	pthread_detach(Thread);
}

void ImageSetTypes(GdkPixbufFormat *Format, GSList *Formats) {
	// This is a helper function for filling image types supported by GTK
	int Index = g_slist_index(Formats, Format);
	ImageTypes[Index] = gdk_pixbuf_format_get_name(Format);
}

char *ImageConvertPlace[] = { "ConvertLimit", NULL };
char *ImageAnimationPlace[] = { "AnimationLimit", NULL };

void ImageUpdateMax() {
	// This function is responsible for updating max values of limiters

	// Get limits
	double ConvertLimit = JsonDoubleObject(GlobalPreferences, ImageConvertPlace,
			10);
	double AnimationLimit = JsonDoubleObject(GlobalPreferences,
			ImageAnimationPlace, 1);

	// Update max values of limiters
	LimitMax(ImageConvertLimit, ConvertLimit);
	LimitMax(ImageAnimationLimit, AnimationLimit);
}

void ImageStart(void) {
	// This function is responsible for initing everything used by the file

	// Get screen, icon theme and placeholders at 1024 resolution
	GdkScreen *Screen = gdk_screen_get_default();
	GtkIconTheme *IconTheme = gtk_icon_theme_get_for_screen(Screen);
	ImageUser = gtk_icon_theme_load_icon(IconTheme, "user", 1024,
			GTK_ICON_LOOKUP_FORCE_SIZE, NULL);
	if (ImageUser == NULL) {
		ImageUser = gtk_icon_theme_load_icon(IconTheme, "avatar-default",
				1024, GTK_ICON_LOOKUP_FORCE_SIZE, NULL);
	}
	ImageMissing = gtk_icon_theme_load_icon(IconTheme, "image-missing", 1024,
			GTK_ICON_LOOKUP_FORCE_SIZE, NULL);

	// Get limits
	double ConvertLimit =
		JsonDoubleObject(GlobalPreferences, ImageConvertPlace, 10);
	double AnimationLimit = JsonDoubleObject(GlobalPreferences,
			ImageAnimationPlace, 1);

	// Create limiters
	ImageConvertLimit = LimitCreate((int) ConvertLimit);
	ImageAnimationLimit = LimitCreate((int) AnimationLimit);
	ImageWandLimit = LimitCreate(-1);

	// Create lock for ImageListCount
	pthread_mutex_init(&ImageListLock, NULL);

	// Get list of image formats supported by GTK
	GSList *Formats = gdk_pixbuf_get_formats();

	// Create list containing formats
	ImageTypesLength = g_slist_length(Formats);
	ImageTypes = malloc(sizeof(char *) * (ImageTypesLength + 1));

	// Fill list, free GSList container
	g_slist_foreach(Formats, (GFunc) ImageSetTypes, Formats);
	ImageTypes[ImageTypesLength] = NULL;
	g_slist_free(Formats);

	// Start ImageMagick and Curl
	MagickWandGenesis();
	curl_global_init(CURL_GLOBAL_ALL);
}

void ImageStop(void) {
	// This function is responsible for cleaning up everything used by the file

	// Destroy limiters
	LimitDestroy(ImageAnimationLimit);
	LimitDestroy(ImageConvertLimit);
	LimitDestroy(ImageWandLimit);

	// Free image types supported by GTK
	int Index = 0;
	while (ImageTypes[Index] != NULL) {
		free(ImageTypes[Index]);
		++Index;
	}
	free(ImageTypes);

	// Stop ImageMagick, Curl
	MagickWandTerminus();
	curl_global_cleanup();

	// Free placeholders
	g_object_unref(ImageUser);
	g_object_unref(ImageMissing);

	// Destroy lock
	pthread_mutex_destroy(&ImageListLock);
}

void ImageIncreaseCount(void) {
	// Increase ListCount

	pthread_mutex_lock(&ImageListLock);
	++ImageListCount;
	pthread_mutex_unlock(&ImageListLock);
}

// Everything under this line is removable after full C conversion
static PyObject *ImageUpdateMaxPython(PyObject *Py_UNUSED(
		Self), PyObject *Py_UNUSED(Args)) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	ImageUpdateMax();

	// Acquire lock
	PyEval_RestoreThread(Save);

	return PyLong_FromLong(0);
}

// Function names and other python data
static PyMethodDef ImageMethods[] = {
	{"UpdateMax", ImageUpdateMaxPython, METH_VARARGS, "Update Max"},
	{NULL, NULL, 0, NULL}
};

static struct PyModuleDef ImageModule = {
	PyModuleDef_HEAD_INIT, "Image", "Image module", -1, ImageMethods, 0, 0, 0, 0
};

// The function name has to end with the module name
PyMODINIT_FUNC PyInit_Image(void) {
	return PyModule_Create(&ImageModule);
}
