/******************************************************************************\
* LBRY-GTK                                                                     *
* Copyright (C) 2021-2023 MorsMortium and Other Contributors                   *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU General Public License as published by         *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                *
* GNU General Public License for more details.                                 *
*                                                                              *
* You should have received a copy of the GNU General Public License            *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
\******************************************************************************/

// This file gets and sets the settings/preferences

#include <Python.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <jansson.h>
#include <pthread.h>

#include "Header/Request.h"
#include "Header/Json.h"
#include "Header/Global.h"
#include "Header/Preferences.h"

// Lock so we won't accidentally import settings
pthread_mutex_t PreferencesLock;

json_t *PreferencesUpdate(char *DefaultPath, json_t *OldObject) {
	// This function will update any information in given json object
	// from given default file

	// Load the file
	json_error_t Error;
	json_t *Defaults = json_load_file(DefaultPath, 0, &Error);
	if (Defaults == NULL) {
		json_t *ErrorMessage = json_object();

		// Return an error message in failure
		json_object_set_new(ErrorMessage, "error", json_string(Error.text));
		return ErrorMessage;
	}

	// Create a copy of defaults
	json_t *Return = json_deep_copy(Defaults);

	// Update the object
	json_object_update_existing(Return, OldObject);

	// Update objects within object
	const char *Key;
	json_t *Value;
	json_object_foreach(Defaults, Key, Value) {
		// Check if value is object
		if (json_is_object(Value)) {
			json_object_update_existing(Value, json_object_get(OldObject, Key));

			// Set the value to copied object
			json_object_set(Return, Key, Value);
		}
	}
	json_decref(OldObject);
	json_decref(Defaults);

	// Return the new values object
	return Return;
}

json_t *PreferencesImport(char *ConfigPath, char *DefaultPath) {
	// This function will be used to import old settings

	// Server holder
	char *Server;

	// For array foreach loops and OldKeys object
	void *Tmp;
	const char *Key;
	json_t *Value, *OldKeys = json_object();

	// Get the old session file
	char SessionPath[strlen(ConfigPath) + 15];
	sprintf(SessionPath, "%sSession.json", ConfigPath);

	// Start by getting the old sessions file
	json_error_t Error;
	json_t *Session = json_load_file(SessionPath, 0, &Error);
	if (Session != NULL) {
		Server = (char *) json_string_value(json_object_get(Session, "Server"));

		// Move keybinds to its own object
		json_object_foreach_safe(Session, Tmp, Key, Value) {
			// Keys start with KB prefix
			if (Key[0] == 'K' && Key[1] == 'B') {
				json_object_set(OldKeys, Key + 2, Value);
				json_object_del(Session, Key);
			}
		}
	} else {
		// If session file doesn't exist take defaults
		json_t *Defaults = PreferencesUpdate(DefaultPath, json_object());
		if (json_object_get(Defaults, "error") != NULL) {
			return Defaults;
		}
		Session = json_object_get(Defaults, "Session");
		OldKeys = json_object_get(Defaults, "Keyboard");
		Server = (char *) json_string_value(json_object_get(Session, "Server"));
		json_incref(Session);
		json_incref(OldKeys);
		json_decref(Defaults);
	}

	// Create a cool new object to hold preferences
	json_t *Preferences = json_object();

	// Check for any settings in preferences
	json_t *Result = RequestJson(Server,
			"{ \"method\" : \"preference_get\", \"params\" : {} }");

	// Now add Session to the object
	json_object_set_new(Preferences, "Session", Session);

	// If Result errored out due to server not running return just Session
	if (json_object_get(Result, "error") != NULL) {
		// TODO: Why am I updating Preferences with Result if Result
		// has invalid value?
		json_object_del(Result, "error");
		json_object_update_missing(Preferences, Result);
		json_object_set_new(Preferences, "Keyboard", OldKeys);
		json_decref(Result);
		return Preferences;
	}

	// Start getting values we might want and need

	// Uppest objects of old preferences
	json_t *Shared = json_object_get(Result, "shared");
	json_t *Local = json_object_get(Result, "local");

	// If object actually exists then do some tedious checking
	if (Shared != NULL) {
		// Value exists for 100% if shared exists
		json_t *ValueShared = json_object_get(Shared, "value");

		// ShowMature
		json_t *Sets = json_object_get(ValueShared, "settings");
		if (json_object_get(Sets, "show_mature") != NULL) {
			json_object_set(Preferences, "ShowMature",
				json_object_get(Sets, "show_mature"));
		}

		// Following
		if (json_object_get(ValueShared, "following") != NULL) {
			json_object_set(Preferences, "Following",
				json_object_get(ValueShared, "following"));
		}

		// Tags
		if (json_object_get(ValueShared, "tags") != NULL) {
			json_object_set(Preferences, "TagsShared",
				json_object_get(ValueShared, "tags"));
		}

		// All of LBRY-GTK specific preferences
		if (json_object_get(ValueShared, "LBRY-GTK") != NULL) {
			json_object_update(Preferences,
				json_object_get(ValueShared, "LBRY-GTK"));

			// We also change to new naming while doing this
			json_object_set(Preferences, "ChannelsNot",
				json_object_get(Preferences, "NotChannels"));
			json_object_set(Preferences, "TagsNot",
				json_object_get(Preferences, "NotTags"));

			// Delete old names
			json_object_del(Preferences, "NotChannels");
			json_object_del(Preferences, "NotTags");
		}
	}

	// Local tags separately
	if (Local != NULL) {
		json_t *ValueLocal = json_object_get(Local, "value");
		if (json_object_get(ValueLocal, "tags") != NULL) {
			json_object_set(Preferences, "TagsLocal",
				json_object_get(ValueLocal, "tags"));
		}
	}
	json_decref(Result);

	// Here we add default LBRY-GTK preferences
	json_t *Updated = PreferencesUpdate(DefaultPath, Preferences);
	if (json_object_get(Updated, "error") != NULL) {
		return Updated;
	}

	// Get default keyboard
	json_t *DefaultKeyboard = json_object_get(Updated, "Keyboard");

	// Add keyboard values to defaults
	json_t *NewKeys = json_object();
	json_object_foreach(DefaultKeyboard, Key, Value) {
		// Some keys might not be in use anymore
		if (json_object_get(OldKeys, Key) != NULL) {
			json_object_set(NewKeys, Key, json_object_get(OldKeys, Key));
		} else {
			json_object_set(NewKeys, Key, Value);
		}
	}
	json_decref(OldKeys);

	// Add the new set of keys to keyboard object
	json_object_set_new(Updated, "Keyboard", NewKeys);

	// We want to save preferences to config path
	char FilePath[strlen(ConfigPath) + 20];
	sprintf(FilePath, "%sPreferences.json", ConfigPath);

	// Finally dump to a file
	json_dump_file(Updated, FilePath, JSON_INDENT(4) | JSON_SORT_KEYS);

	// Return resulting object for further inspection
	return Updated;
}

json_t *PreferencesGet(void) {
	// This function gets the settings/preferences into one json block
	// This is more convenient way of getting settings/preferences as
	// the lock is used here without having to worry about corruptions

	// Lock the lock or wait for lock
	pthread_mutex_lock(&PreferencesLock);

	// Wrapper block
	json_t *Preferences = json_object();
	json_object_set(Preferences, "Preferences", GlobalPreferences);
	json_object_set(Preferences, "settings", GlobalSettings);

	// Here we unlock the lock
	pthread_mutex_unlock(&PreferencesLock);

	return Preferences;
}

int PreferencesSync(char *ConfigPath, char *DefaultPath) {
	// This function syncs the settings/preferences to global variables

	// Some variables used in checking
	int RC = 0;
	char *Server;

	// Lock the lock or wait for lock
	pthread_mutex_lock(&PreferencesLock);

	// We want to get preferences from config path
	char FilePath[strlen(ConfigPath) + 20];
	sprintf(FilePath, "%sPreferences.json", ConfigPath);

	// Get preferences first
	json_error_t Error;
	json_t *PreferenceGet = json_load_file(FilePath, 0, &Error);
	if (PreferenceGet == NULL) {
		// If -1 the file is empty and no other errors occured
		if (Error.line != -1) {
			remove(FilePath);
		}

		// Default server url
		Server = "http://localhost:5279";

		// Import Preferences if failure or just make an object
		if (DefaultPath != NULL) {
			PreferenceGet = PreferencesImport(ConfigPath, DefaultPath);
		} else {
			RC += 1;
			PreferenceGet = json_object();
		}
	} else {
		// We get the server url from session
		Server = (char *)
			json_string_value(json_object_get(json_object_get(PreferenceGet,
				"Session"), "Server"));
	}

	// Get settings now
	json_t *SettingGet = RequestJson(Server,
			"{ \"method\" : \"settings_get\", \"params\" : {} }");

	// Check for error and add new settings to global if okay
	if (json_object_get(SettingGet, "error") != NULL) {
		RC += 2;
		json_decref(SettingGet);
	} else {
		if (GlobalSettings) {
			json_decref(GlobalSettings);
		}
		GlobalSettings = SettingGet;
	}

	// Add client preferences to global variable
	if (GlobalPreferences) {
		json_decref(GlobalPreferences);
	}
	GlobalPreferences = PreferenceGet;

	// Here we unlock the lock
	pthread_mutex_unlock(&PreferencesLock);

	// Return value is 0-3 based on which failed if any or all
	return RC;
}

int PreferencesSetClient(char *ConfigPath, json_t *Preferences) {
	// This function sets the settings for client

	if (Preferences == NULL) {
		return 8;
	}

	// We save preferences to given config path
	char FilePath[strlen(ConfigPath) + 20];
	sprintf(FilePath, "%sPreferences.json", ConfigPath);

	// Dump to a file
	json_dump_file(Preferences, FilePath, JSON_INDENT(4) | JSON_SORT_KEYS);

	return 0;
}

int PreferencesSetLBRY(char *Server, json_t *Settings) {
	// This function sets the settings for lbrynet

	// If Settings is not actually set, we bail
	if (Settings == NULL) {
		return 4;
	}

	// Set settings
	const char *Key;
	json_t *Value;
	json_object_foreach(Settings, Key, Value) {
		// Array and null will be skipped
		if (json_is_null(Value) || json_is_array(Value) != 0) {
			continue;
		}

		// Temporary params object for key value pair
		json_t *Params = json_object();
		json_object_set_new(Params, "key", json_string(Key));
		json_object_set(Params, "value", Value);

		// Get the value as a string
		char *ParamStr = json_dumps(Params, 0);
		json_decref(Params);

		// Make a request
		char JsonRequest[strlen(ParamStr) + 85];
		sprintf(JsonRequest, "{ \"method\" : \"settings_set\", "
			"\"params\" : %s }", ParamStr);

		// Free dumped Value
		free(ParamStr);

		// Save key value pair
		json_t *Result = RequestJson(Server, JsonRequest);
		if (json_object_get(Result, "error") != NULL) {
			json_decref(Settings);
			return 4;
		}
		json_decref(Result);
	}

	return 0;
}

int PreferencesSet(char *ConfigPath, json_t *Settings) {
	// This function sets the preferences/settings

	// TODO: The return values of functions used here could follow
	// some bitflag enum IF error checking is at importance.
	// Currently the values are static integers.

	// Lock the lock or wait for lock
	pthread_mutex_lock(&PreferencesLock);

	int RC = 0;

	// Get server url
	char *Server =
		(char *) json_string_value(json_object_get(json_object_get(
				json_object_get(
					Settings, "Preferences"), "Session"), "Server"));

	// Set both client and lbrynet settings from given settings block
	RC += PreferencesSetClient(ConfigPath, json_object_get(Settings,
			"Preferences"));
	RC += PreferencesSetLBRY(Server, json_object_get(Settings, "settings"));
	json_decref(Settings);

	// Here we unlock the lock
	pthread_mutex_unlock(&PreferencesLock);

	// Sync the settings with global variables
	RC += PreferencesSync(ConfigPath, NULL);

	return RC;
}

void PreferencesStart(char *FilePath, char *DefaultPath) {
	// Inits mutex

	pthread_mutex_init(&PreferencesLock, NULL);
	PreferencesSync(FilePath, DefaultPath);
}

void PreferencesStop(void) {
	// Destroy mutex

	pthread_mutex_destroy(&PreferencesLock);
}

// Everything under this line is removable after full C conversion
static PyObject *PreferencesUpdatePython(PyObject *Py_UNUSED(
		Self), PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Get arguments and pass it to the pythonless function
	char *FilePath;
	PyObject *Unicode;
	if (!PyArg_ParseTuple(Args, "sU", &FilePath, &Unicode)) {
		return NULL;
	}

	char *Text = (char *) PyUnicode_AsUTF8(Unicode);

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	// Convert Text into json
	json_error_t Error;
	json_t *Object = json_loads(Text, 0, &Error);

	json_t *JsonData = PreferencesUpdate(FilePath, Object);

	// Acquire lock
	PyEval_RestoreThread(Save);

	return JsonToString(JsonData);
}

static PyObject *PreferencesGetPython(PyObject *Py_UNUSED(
		Self), PyObject *Py_UNUSED(Args)) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	json_t *JsonData = PreferencesGet();

	// Acquire lock
	PyEval_RestoreThread(Save);

	return JsonToString(JsonData);
}

static PyObject *PreferencesSetPython(PyObject *Py_UNUSED(Self),
	PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Get arguments and pass it to the pythonless function
	char *FilePath;
	PyObject *Unicode;
	if (!PyArg_ParseTuple(Args, "sU", &FilePath, &Unicode)) {
		return NULL;
	}

	char *Text = (char *) PyUnicode_AsUTF8(Unicode);

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	// Convert Text into json
	json_error_t Error;
	json_t *Settings = json_loads(Text, 0, &Error);

	int RC = PreferencesSet(FilePath, Settings);

	// Acquire lock
	PyEval_RestoreThread(Save);

	return (RC) ? PyLong_FromLong(1) : JsonToString(PreferencesGet());
}

// Function names and other python data
static PyMethodDef PreferencesMethods[] = {
	{"Update", PreferencesUpdatePython, METH_VARARGS, "Update json object"},
	{"Set", PreferencesSetPython, METH_VARARGS, "Preferences/settings set"},
	{"Get", (PyCFunction) PreferencesGetPython, METH_NOARGS,
	 "Preferences/settings get"},
	{NULL, NULL, 0, NULL}
};

static struct PyModuleDef PreferencesModule = {
	PyModuleDef_HEAD_INIT, "Preferences", "Preferences module", -1,
	PreferencesMethods, 0, 0, 0, 0
};

// The function name has to end with the module name
PyMODINIT_FUNC PyInit_Preferences(void) {
	return PyModule_Create(&PreferencesModule);
}
