_VERSION = 0.7.2
VERSION = $(_VERSION)-`git rev-parse --short HEAD`

ifeq ($(OS), Windows_NT)
LDOS = -export-all-symbols -ldbghelp
else
LDOS = -z,relro,-z,now
endif
