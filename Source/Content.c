/******************************************************************************\
* LBRY-GTK                                                                     *
* Copyright (C) 2021-2023 MorsMortium and Other Contributors                   *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU General Public License as published by         *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                *
* GNU General Public License for more details.                                 *
*                                                                              *
* You should have received a copy of the GNU General Public License            *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
\******************************************************************************/

// This file deals with content publishing and getting

#include <Python.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <jansson.h>

#include "Header/Request.h"
#include "Header/Filter.h"
#include "Header/Url.h"
#include "Header/Json.h"
#include "Header/Content.h"

// Array names for easier inspection
static const char *ContentIDArrays[] =
{ "channel_ids", "not_channel_ids", "claim_ids", NULL };

void ContentRandomString(int Size, char Out[Size + 1]) {
	// Inserts random characters into given buffer

	// We want only ASCII characters per LBRY SDK documentation
	const char ASCII[] =
		"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-0123456789";

	// Iterate amount of times to get random string of same length
	int Index;
	for (Index = 0; Index < Size; Index++) {
		Out[Index] = ASCII[rand() % (sizeof ASCII - 1)];
	}

	// Terminate string
	Out[Size] = '\0';
}

json_t *ContentPublish(bool NoThumbnailAndFile, json_t *Params, char *Server) {
	// This function will upload file

	// If no name specified (name must always be present)
	if (strlen(json_string_value(json_object_get(Params, "name"))) < 1) {
		char Name[71];
		ContentRandomString(70, Name);
		json_object_set_new(Params, "name", json_string(Name));
	}

	// Make new thumbnail claim and make it the thumbnail
	// True when url doesn't exist but there is a thumbnail file
	if (NoThumbnailAndFile) {
		// Create json block
		json_error_t Error;
		json_t *ThumbnailParams = json_loads("{ \"name\" : \"\","
				"\"bid\" : \"0.0001\", \"validate_file\" : false,"
				"\"optimize_file\" : false, \"preview\" : false,"
				"\"blocking\" : false }", 0, &Error);

		// Add thumbnail as the file to be uploaded
		json_object_set_new(ThumbnailParams, "file_path",
			json_object_get(Params, "thumbnail_file"));

		// Publish new thumbnail if file given
		json_t *JsonData = ContentPublish(false, ThumbnailParams, Server);
		if (json_object_get(JsonData, "error") != NULL) {
			return JsonData;
		}

		// The outputs have all the information
		json_t *Out = json_array_get(json_object_get(JsonData, "outputs"), 0);

		// Get thumbnail url
		const char *UrlStr =
			json_string_value(json_object_get(Out, "permanent_url"));

		// Make it into spee.ch link
		char SpeechUrl[130];
		sprintf(SpeechUrl, "https://spee.ch/%s", UrlStr + 7);

		// Reference to data is no longer needed
		json_decref(JsonData);

		// Add to the given parameters json object
		json_object_set_new(Params, "thumbnail_url", json_string(SpeechUrl));
	}

	// Delete any invalid parameters
	json_object_del(Params, "thumbnail_file");

	// Dump resulting parameters object
	char *ParamStr = json_dumps(Params, 0);
	json_decref(Params);

	// Make the request
	char JsonRequest[strlen(ParamStr) + 50];
	sprintf(JsonRequest, "{ \"method\" : \"publish\", \"params\" : %s }",
		ParamStr);

	// Free dumped string
	free(ParamStr);

	// Error handling is done in the other end
	return RequestJson(Server, JsonRequest);
}

json_t *ContentSearch(json_t *Params, int PageSize, int Page, char *Server) {
	// This function searches for content on LBRY

	// Check the string is not URL but id
	int Item = 0;
	while (ContentIDArrays[Item] != NULL) {
		json_t *Array = json_object_get(Params, ContentIDArrays[Item++]);

		// Continue to the next one if object is not found
		if (Array == NULL) {
			continue;
		}

		// Loop over each element in array
		json_t *Value;
		size_t Index;
		json_array_foreach(Array, Index, Value) {
			const char *ValueStr = json_string_value(Value);

			// Cut is the index of where to cut the new string from
			int Cut = 0;
			for (int Place = 0; Place < (int) strlen(ValueStr); Place++) {
				if (ValueStr[Place] == '#') {
					Cut = Place + 1;
					break;
				}
			}

			// Place the new value to array
			json_array_set_new(Array, Index, json_string(ValueStr + Cut));
		}
	}

	// Check for default or empty values
	JsonClear(Params);

	// Set all the values that will always be present
	json_object_set_new(Params, "page_size", json_integer(PageSize));
	json_object_set_new(Params, "page", json_integer(Page));
	json_object_set_new(Params, "no_totals", json_true());

	// Dump resulting parameters object
	char *ParamStr = json_dumps(Params, 0);

	// Make the request
	char Request[strlen(ParamStr) + 55];
	sprintf(Request, "{\"method\": \"claim_search\", \"params\": %s }",
		ParamStr);

	// Free dumped string
	free(ParamStr);

	// Get result
	json_t *Result = RequestJson(Server, Request);
	if (json_object_get(Result, "error") != NULL) {
		return Result;
	}

	// URL things happen here then
	UrlThumbnail(Result, Server);

	// Get parse stuff
	return FilterPublications(Result);
}

json_t *ContentGet(char *Uri, char *FileName, char *DownloadDirectory,
	int Timeout, bool SaveFile, char *WalletID, char *Server) {
	// This function will get the content from the network

	// Make the params object
	json_t *Params = json_object();

	// Add all present items to the object
	if (strcmp(Uri, "") != 0) {
		json_object_set_new(Params, "uri", json_string(Uri));
	}
	if (strcmp(FileName, "") != 0) {
		json_object_set_new(Params, "file_name", json_string(FileName));
	}
	if (strcmp(DownloadDirectory, "") != 0) {
		json_object_set_new(Params, "download_directory",
			json_string(DownloadDirectory));
	}
	if (Timeout != -1) {
		json_object_set_new(Params, "timeout", json_integer(Timeout));
	}
	if (SaveFile != false) {
		json_object_set_new(Params, "save_file", json_boolean(SaveFile));
	}
	if (strcmp(WalletID, "") != 0) {
		json_object_set_new(Params, "wallet_id", json_string(WalletID));
	}

	// Dump resulting parameters object
	char *ParamStr = json_dumps(Params, 0);
	json_decref(Params);

	// Make the request
	char JsonRequest[strlen(ParamStr) + 45];
	sprintf(JsonRequest, "{ \"method\" : \"get\", \"params\" : %s }", ParamStr);

	// Free dumped string
	free(ParamStr);

	// Get result
	json_t *Result = RequestJson(Server, JsonRequest);

	// Error handling is done in the other end
	return Result;
}

// Everything under this line is removable after full C conversion
static PyObject *ContentPublishPython(PyObject *Py_UNUSED(Self),
	PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Get arguments and pass it to the pythonless function
	char *Server;
	bool NoThumbnail;
	PyObject *Unicode;
	if (!PyArg_ParseTuple(Args, "psU", &NoThumbnail, &Server, &Unicode)) {
		return NULL;
	}

	char *Text = (char *) PyUnicode_AsUTF8(Unicode);

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	// Convert Text into json
	json_error_t Error;
	json_t *Params = json_loads(Text, 0, &Error);

	json_t *JsonData = ContentPublish(NoThumbnail, Params, Server);

	// Acquire lock
	PyEval_RestoreThread(Save);

	return JsonToString(JsonData);
}

static PyObject *ContentGetPython(PyObject *Py_UNUSED(Self), PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Get arguments and pass it to the pythonless function
	char *Uri, *FileName, *DownloadDirectory, *WalletID = "", *Server;
	bool SaveFile;
	int Timeout = -1;
	if (!PyArg_ParseTuple(Args, "sssps", &Uri, &FileName, &DownloadDirectory,
		&SaveFile, &Server)) {
		return NULL;
	}

	// Workaround for this bug that leaves Uri untouched by PyArg_ParseTuple?
	Uri = (char *) PyUnicode_AsUTF8(PyTuple_GetItem(Args, 0));

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	json_t *JsonData = ContentGet(Uri, FileName, DownloadDirectory,
			Timeout, SaveFile, WalletID, Server);

	// Acquire lock
	PyEval_RestoreThread(Save);

	return JsonToString(JsonData);
}

// Function names and other python data
static PyMethodDef ContentMethods[] = {
	{"Publish", ContentPublishPython, METH_VARARGS, "Publish new"},
	{"Get", ContentGetPython, METH_VARARGS, "Get content"},
	{NULL, NULL, 0, NULL}
};

static struct PyModuleDef ContentModule = {
	PyModuleDef_HEAD_INIT, "Content", "Content module", -1, ContentMethods, 0,
	0, 0, 0
};

// The function name has to end with the module name
PyMODINIT_FUNC PyInit_Content(void) {
	return PyModule_Create(&ContentModule);
}
