/******************************************************************************\
* LBRY-GTK                                                                     *
* Copyright (C) 2021-2023 MorsMortium and Other Contributors                   *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU General Public License as published by         *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                *
* GNU General Public License for more details.                                 *
*                                                                              *
* You should have received a copy of the GNU General Public License            *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
\******************************************************************************/

// Fill is responsible for textbuffer's text and items such as images

#include <Python.h>
#include <gtk/gtk.h>
#include <jansson.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <ctype.h>
#include <math.h>

#include "Header/Image.h"
#include "Header/Parse.h"
#include "Header/Execute.h"
#include "Header/Url.h"
#include "Header/Places.h"
#include "Header/Global.h"
#include "Header/Threads.h"
#include "Header/Tabs.h"
#include "Header/Markdown.h"
#include "Header/Fill.h"

// Items to keep in tag checking
const char *FillKeptTags[] =
{ "b", "big", "i", "s", "sub", "sup", "small", "tt", "u", NULL };

// Tables to check HTML character references
// Always wrapped in refrence start (&) and end (;) characters
const char *FillReferences[] = { "amp", "lt", "gt", "quot", "apos" };
const char FillCharacters[] = { '&', '<', '>', '"', '\'' };

static void FillFreeTag(FillTag *TagInfo, int TagAmount) {
	// Free everything in tag info

	for (int Index = 0; Index < TagAmount; Index++) {
		if (TagInfo[Index].Name != NULL) {
			free(TagInfo[Index].Name);
		}
		if (TagInfo[Index].Option != NULL) {
			free(TagInfo[Index].Option);
		}
	}

	// Free the struct array
	free(TagInfo);
}

static void *FillFreeData(void *Args) {
	// Frees data in data struct

	FillData *Data = (FillData *) Args;

	// Image thread uses lock so we need to check for that
	// to not accidentally attempt to access free'd memory
	if (Data->IsThreaded == true) {
		pthread_mutex_lock(&Data->ThreadLock);
		pthread_mutex_unlock(&Data->ThreadLock);
		pthread_mutex_destroy(&Data->ThreadLock);
	}

	// Free taginfos
	FillFreeTag(Data->TagInfos, Data->TagAmount);

	// Unreference manually referenced objects
	for (int Index = 0; Index < Data->RotateMax; Index++) {
		if (Data[Index].Item != NULL && GTK_IS_WIDGET(Data[Index].Item)) {
			g_object_unref((GObject *) Data[Index].Item);
		}
	}

	// Free linktags pointer array
	if (Data->LinkTags != NULL) {
		free(Data->LinkTags);
	}

	// Free data path
	if (Data->Path != NULL) {
		free(Data->Path);
	}

	// Free the structs array
	free(Data);

	return NULL;
}

static int FillNewTag(FillTag **TagInfo, int OldSize) {
	// Allocate memory for new items in taginfo array

	FillTag *TemporaryTagInfo = realloc(*TagInfo,
			(OldSize + 1) * sizeof *TemporaryTagInfo);
	if (TemporaryTagInfo == NULL) {
		FillFreeTag(*TagInfo, OldSize);
		return 0;
	}

	// Default values
	TemporaryTagInfo[OldSize].Place = -1;
	TemporaryTagInfo[OldSize].EndTag = false;
	TemporaryTagInfo[OldSize].PairInfo = NULL;
	TemporaryTagInfo[OldSize].Option = NULL;

	// Reference to new place in memory
	*TagInfo = TemporaryTagInfo;

	return OldSize + 1;
}

static int FillNewData(FillData **Data, int OldSize) {
	// Allocate memory for new items in data array

	FillData *TemporaryData = realloc(*Data,
			(OldSize + 1) * sizeof *TemporaryData);
	if (TemporaryData == NULL) {
		// Panic and free everything
		FillFreeData(*Data);

		return -1;
	}
	TemporaryData[OldSize].IsThreaded = false;

	// Reference to new place in memory
	*Data = TemporaryData;

	return OldSize + 1;
}

FillData *FillDataAlloc(bool IfComment, bool IfResolve, char *Path,
	TabsData *TabData, PyObject *AddPage) {
	// Allocate memory for FillData

	FillData *TextData = calloc(1, sizeof *TextData);
	if (TextData == NULL) {
		return NULL;
	}

	TextData->IfComment = IfComment;
	TextData->IfResolve = IfResolve;
	TextData->TabData = TabData;

	// Python stuff REMOVABLE SOON
	TextData->AddPage = AddPage;

	// At this point we can also allocate memory for path
	// Since FillData is calloced, the Path will be NULL if it's empty
	if (Path != NULL && Path[0] != '\0') {
		TextData->Path = malloc(strlen(Path) + 1);
		if (TextData->Path == NULL) {
			free(TextData);
			return NULL;
		}
		strcpy(TextData->Path, Path);
	}

	return TextData;
}

gboolean FillOnOverlayDestroy(G_GNUC_UNUSED GtkTextView *TextView,
	gpointer Args) {
	// Calls the memory freeing function to free data that
	// otherwise would be lost

	FillData *Data = (FillData *) Args;

	// Disconnect all signals connected to objects to prevent
	// free'd memory being read accidentally
	for (int Index = 0; Index < Data->RotateMax; Index++) {
		if (strcmp(Data[Index].TagInfo->Name, "a") == 0) {
			g_object_disconnect(Data[Index].Object, "any_signal", FillLinkClick,
				&Data[Index], NULL);
		} else if (strcmp(Data[Index].TagInfo->Name, "blockquote") == 0) {
			g_object_disconnect(Data[Index].Object, "any_signal", FillQuoteDraw,
				&Data[Index], NULL);
		} else if (strcmp(Data[Index].TagInfo->Name, "hr") == 0) {
			g_object_disconnect(Data[Index].Object, "any_signal", FillHRDraw,
				&Data[Index], NULL);
		}
	}

	pthread_t Thread;
	pthread_create(&Thread, NULL, FillFreeData, Data);
	pthread_detach(Thread);

	return FALSE;
}

gboolean FillHRDraw(GtkWidget *TextView, G_GNUC_UNUSED cairo_t *CR,
	gpointer Args) {
	// Draw a HR line

	FillData *Data = (FillData *) Args;

	// Index of our line
	int Place = Data->TagInfo->Place;

	GtkTextIter Iter;
	GdkRectangle Location;
	GtkTextBuffer *TextBuffer = gtk_text_view_get_buffer(
		(GtkTextView *) TextView);

	// Get the location in buffer
	gtk_text_buffer_get_iter_at_offset(TextBuffer, &Iter, Place);
	gtk_text_view_get_iter_location((GtkTextView *) TextView, &Iter,
		&Location);

	if (GTK_IS_WIDGET(Data->Item)) {
		// Set margin and new minimum size
		gtk_widget_set_margin_top(Data->Item,
			Location.y + Location.height);
		gtk_widget_set_size_request(Data->Item,
			gtk_widget_get_allocated_width(TextView), -1);
	}

	return FALSE;
}

gboolean FillQuoteDraw(GtkWidget *TextView, G_GNUC_UNUSED cairo_t *CR,
	gpointer Args) {
	// Draw quote separator

	FillData *Data = (FillData *) Args;

	// Some items from tag info
	int Place = Data->TagInfo->Place;
	int Pair = Data->TagInfo->PairInfo->Place;

	GtkTextBuffer *TextBuffer = gtk_text_view_get_buffer(
		(GtkTextView *) TextView);

	// The start/end iters to reference a place in buffer
	GtkTextIter Start, End;

	// Rectangle locations
	GdkRectangle StartLocation, EndLocation;

	gtk_text_buffer_get_iter_at_offset(TextBuffer, &Start, Place);
	gtk_text_buffer_get_iter_at_offset(TextBuffer, &End, Pair);

	// Some attributes
	int MaxHeight = gtk_widget_get_allocated_height(TextView);
	gtk_text_view_get_iter_location((GtkTextView *) TextView, &Start,
		&StartLocation);
	gtk_text_view_get_iter_location((GtkTextView *) TextView, &End,
		&EndLocation);

	int Margin = MaxHeight - EndLocation.height - EndLocation.y;

	// Set the margin and show widget again
	if (GTK_IS_WIDGET(Data->Item)) {
		gtk_widget_set_margin_top(Data->Item, StartLocation.y);
		if (Margin > 0) {
			gtk_widget_set_margin_bottom(Data->Item, Margin);
		}
		gtk_widget_show(Data->Item);
	}

	return FALSE;
}

gboolean FillLinkClick(G_GNUC_UNUSED GtkTextTag *Tag, GtkWidget *Widget,
	GdkEvent *Event, G_GNUC_UNUSED GtkTextIter *Iter, FillData *Data) {
	// Signal for link tags

	// Check the type of the button press event and if one
	GdkEventType Type = Event->type;

	// If the type of event is press event, add name to widget
	if (Type == GDK_BUTTON_PRESS) {
		g_object_set((GObject *) Widget, "name",
			Data->TagInfo->Option, NULL);
		return FALSE;
	}

	// Only with release do we want to execute rest of the code
	if (Type != GDK_BUTTON_RELEASE) {
		return FALSE;
	}

	// Check the tag we last pressed is still the same after
	// button release
	char *WidgetName;
	g_object_get((GObject *) Widget, "name", &WidgetName, NULL);
	if (strcmp(WidgetName, Data->TagInfo->Option) != 0) {
		return FALSE;
	}

	char *LinkCommand =
		(char *) json_string_value(json_object_get(GlobalPreferences,
			"LinkCommand"));

	// Get the index where to cut the string according to number of digits
	int LinkLength = (int) strlen(Data->TagInfo->Option) - 2;
	for (int Count = Data->LinkTagIndex; Count >= 10; Count /= 10) {
		LinkLength--;
	}

	// Copy the string cutting rotate -number from end of it
	char LinkName[LinkLength + 1];
	strncpy(LinkName, Data->TagInfo->Option, LinkLength);
	LinkName[LinkLength] = '\0';

	char *Server =
		(char *) json_string_value(json_object_get(json_object_get(
				GlobalPreferences,
				"Session"), "Server"));

	// Check if the link is an acceptable url unless IfResolve is set
	if (Data->Data->IfResolve || Data->IfResolve) {
		char *Url[1];
		Url[0] = LinkName;
		json_t *JsonList = UrlGet(Url, 1, Server);

		// Type is string and we are certain we want no resolving
		if (json_typeof(json_array_get(JsonList, 0)) == JSON_STRING) {
			Data->IfResolve = false;
		}
		json_decref(JsonList);
	}

	// Markdown files are opened starting from here
	if (strcmp(LinkName + LinkLength - 3, ".md") == 0 &&
		LinkName[0] == '.') {
		// Compose the path string
		char FilePath[strlen(Data->Data->Path) + LinkLength + 1];
		sprintf(FilePath, "%s%s", Data->Data->Path, LinkName);

		// Get the size of the file
		FILE *File = fopen(FilePath, "rb");
		fseek(File, 0, SEEK_END);
		size_t FileSize = ftell(File);
		fseek(File, 0, SEEK_SET);

		// Read the file in to a buffer
		char FileText[FileSize + 1];
		size_t ReadBytes = fread(FileText, FileSize, 1, File);
		fclose(File);
		if (ReadBytes != 1) {
			return FALSE;
		}
		FileText[FileSize] = '\0';

		// Free the old text and get new text
		free(Data->Data->Parent->Text);
		Data->Data->Parent->Text = ParseMarkdown(FileText);

		// Call MarkdownFill to show the new page
		g_idle_add((GSourceFunc) MarkdownFill, Data->Data->Parent);
	} else if (!(Data->Data->IfResolve & Data->IfResolve)) {
		// This is to open links in external programs
		ExecuteFile(LinkName, LinkCommand);
	} else if (Event->button.button == GDK_BUTTON_PRIMARY) {
		// If left button is pressed

		ThreadsMakeExecute(Data->TabData, ThreadsGetPublication, NULL, LinkName,
			NULL, NULL);
	} else if (Event->button.button == GDK_BUTTON_MIDDLE) {
		// If middle button is pressed
		// Get GIL state
		PyGILState_STATE State = PyGILState_Ensure();

		// Create base arguments
		PyObject *BaseArguments = PyList_New(1);
		PyObject *None = Py_None;
		Py_INCREF(None);
		PyList_SetItem(BaseArguments, 0, None);
		PyList_SetItem(BaseArguments, 1,
			PyUnicode_FromString(LinkName));

		// Create list for function and data, fill it
		PyObject *FinalList = PyList_New(2);
		PyList_SetItem(FinalList, 0, PyUnicode_FromString("Publication"));
		PyList_SetItem(FinalList, 1, BaseArguments);

		// Create arguments with dummy GTK values, call function
		PyObject *Arguments = Py_BuildValue("ssO", ".", "", FinalList);
		Py_DECREF(FinalList);
		PyObject *Call = PyObject_Call(Data->AddPage, Arguments, NULL);
		if (Call) {
			Py_DECREF(Call);
		} else {
			PyErr_Print();
		}

		// Free arguments, release GIL
		Py_DECREF(Arguments);
		PyGILState_Release(State);
	}

	// Reset the widget name here
	g_object_set((GObject *) Widget, "name", "", NULL);

	return FALSE;
}

static void FillFindEnclosed(FillTag *TagInfo, int TagAmount) {
	// Finds all enclosed tags in array of tags

	// Temporary tags that weren't enclosed yet
	char *StartTags[TagAmount];

	char *Name;
	for (int Index = 0; Index < TagAmount; Index++) {
		Name = TagInfo[Index].Name;

		// Determine if the tag is end or start tag and give value
		StartTags[Index] = (Name[0] == '/') ? NULL : Name;

		// Check each cached tag and if the current tag is end tag to one
		for (int Iter = Index - 1; Iter > -1 && Name[0] == '/'; Iter--) {
			if (StartTags[Iter] != NULL && strcmp(StartTags[Iter],
				Name + 1) == 0) {
				// No need to check same value anymore
				StartTags[Iter] = NULL;

				// Passing references to pairs
				TagInfo[Iter].PairInfo = &TagInfo[Index];
				TagInfo[Index].PairInfo = &TagInfo[Iter];
				TagInfo[Index].EndTag = true;
				break;
			}
		}
	}
}

static void FillReplace(char *Input, char *Out) {
	// Replace any character reference with the corresponding character

	// The character references are checked in a loop
	int Symbol = 0, FromStart = 0, WriteIndex = 0;
	while (Input[Symbol] != '\0') {
		// Add one character to the output string
		Out[WriteIndex] = Input[Symbol];

		// Find the end of character reference (;)
		if (FromStart && (Input[Symbol] == ';')) {
			// Create temporary buffer
			char TempBuffer[FromStart + 1];

			// Add the comparable string to this buffer
			snprintf(TempBuffer, FromStart, "%s",
				((Symbol - FromStart + 1) + Input));

			// Check if it matches any of the known cases
			for (int Index = 0; Index < 5; Index++) {
				if (strcmp(TempBuffer, FillReferences[Index]) == 0) {
					WriteIndex -= FromStart;
					Out[WriteIndex] = FillCharacters[Index];
				}
			}

			// Reset FromStart
			FromStart = 0;
		}

		// Increase the index from start character onward
		if (FromStart || (Input[Symbol] == '&')) {
			FromStart++;
		}

		// Increment the variables used in this loop
		WriteIndex++;
		Symbol++;
	}

	// This cuts any remaining and unwanted characters
	Out[WriteIndex] = '\0';
}

static int FillGetOffset(char *Text) {
	// Get the difference in length between unstripped and stripped strings

	// Get stripped version of the string
	char Out[strlen(Text) + 1];
	FillReplace(Text, Out);

	int Offset = (int) (strlen(Text) - strlen(Out));

	// Check for unicodes and only take one byte (char) from each
	unsigned char Temp[strlen(Out) + 1];
	strcpy((char *) Temp, Out);
	for (int Index = 1; Index < (int) strlen(Out); Index++) {
		// Checks for the start of the unicode character
		if (Temp[Index - 1] >= 0xC0) {
			// Break if normal ascii or start of next unicode char
			while (Temp[Index] >= 0x80 && Temp[Index] < 0xC0) {
				Index++;
				Offset++;
			}
			Index--;
		}
	}

	// Return the offset
	return Offset;
}

static void FillKept(FillTag *TagInfo, int TagAmount, char *Out) {
	// Add any tags to text that will be interpreted by gtk textbuffer

	int Offset = 0;
	for (int TagIndex = 0; TagIndex < TagAmount; TagIndex++) {
		char *Name = TagInfo[TagIndex].Name;
		bool Found = false;
		for (int Kept = 0; FillKeptTags[Kept] != NULL &&
			Found == false; Kept++) {
			// Found will tell if the tag is one of those we want to
			// keep or not also counting start and end tags
			Found = (strcmp(Name, FillKeptTags[Kept]) == 0 ||
				((strcmp(Name + 1, FillKeptTags[Kept]) == 0) &&
				Name[0] == '/'));
		}

		// If found, add the tag back to text buffer
		if (Found && TagInfo[TagIndex].PairInfo != NULL) {
			char Temporary[strlen(Out) + 10];
			int Index = TagInfo[TagIndex].Place + Offset +
				TagInfo[TagIndex].Offset;
			Offset += strlen(Name) + 2;

			// Add a clean version of this tag to text buffer
			strcpy(Temporary, Out);
			Temporary[Index] = '\0';
			sprintf(Temporary + Index, "<%s>%s", Name,
				Out + Index);

			// New out string
			strcpy(Out, Temporary);
		}
	}
}

FillTag *FillTagsGet(char *Text, char *Out, int *TagAmount) {
	// Get all tags in given text and give back clean text

	*TagAmount = -1;
	int OldCount = -1;
	int OutWrite = 0;

	// Allocate memory for first variable in array
	// This will be reused in the first iteration of our loop
	FillTag *TagInfo = malloc(sizeof *TagInfo);
	if (TagInfo == NULL) {
		return NULL;
	}

	size_t TextLength = strlen(Text);
	int Amount = 0;

	// Check for tags in the text
	for (int Count = 0; Count < (int) TextLength; Count++) {
		// If not a tag start, write to ordinary text
		if (Text[Count] != '<') {
			Out[OutWrite++] = Text[Count];
			continue;
		}

		// Get new item for our list
		Amount = FillNewTag(&TagInfo, Amount);
		if (Amount == 0) {
			return NULL;
		}

		OldCount = ++Count;

		// Whitespaces away
		while (isspace(Text[Count])) {
			Count++;
		}

		int TagIndex = 0, OptionIndex = -1;
		char Symbol = Text[Count];

		// Buffers to hold tag and options present in tag
		char Tag[TextLength - Count];
		char Option[TextLength - Count];
		while (Symbol != '>' && Symbol != '\0') {
			// As long as we haven't started the option
			// counting, we continue adding to the tag
			if (Symbol != ' ' && (OptionIndex == -1 || TagIndex == 0)) {
				Tag[TagIndex++] = Symbol;
			} else {
				OptionIndex += (OptionIndex == -1 && TagIndex > 0);
			}

			// If the tag has some option field (i.e. link)
			// it will be discovered here
			if (OptionIndex > -1 && Symbol == '"') {
				Symbol = Text[++Count];
				while (Symbol != '"' && Symbol != '\0') {
					Option[OptionIndex++] = Symbol;
					Symbol = Text[++Count];
				}
			}

			Symbol = Text[++Count];

			// If for some reason there's a tag inside a tag
			// we get a clean exit with string end symbol
			if (Symbol == '<') {
				Symbol = '\0';
			}
		}

		// Check the symbol is not exactly the wrong one
		if (Symbol != '\0') {
			// Apply items to struct
			Tag[TagIndex] = '\0';
			TagInfo[Amount - 1].Name =
				(char *) malloc(strlen(Tag) + 1);
			if (TagInfo[Amount - 1].Name == NULL) {
				FillFreeTag(TagInfo, Amount);
				return NULL;
			}
			strcpy(TagInfo[Amount - 1].Name, Tag);

			TagInfo[Amount - 1].Place = OutWrite;
			TagInfo[Amount - 1].Index = Amount - 1;

			// Temporary addition to count offset in buffer
			Out[OutWrite] = '\0';
			TagInfo[Amount - 1].Offset =
				(OutWrite) ? FillGetOffset(Out) : 0;
			TagInfo[Amount - 1].Place -= TagInfo[Amount - 1].Offset;
		} else {
			// We hit the end of wrong mark
			Count = OldCount;
			Out[OutWrite++] = Text[Count];
			Amount--;
		}

		// If option was found, we add it here
		if (OptionIndex > 0 && Symbol != '\0') {
			Option[OptionIndex] = '\0';
			TagInfo[Amount - 1].Option =
				(char *) malloc(strlen(Option) + 10);
			if (TagInfo[Amount - 1].Option == NULL) {
				FillFreeTag(TagInfo, Amount);
				return NULL;
			}
			FillReplace(Option, TagInfo[Amount - 1].Option);
		}
	}

	// Store the amount of tags here
	*TagAmount = Amount;
	Out[OutWrite] = '\0';

	// Add information about if the tag is/has end tag
	FillFindEnclosed(TagInfo, *TagAmount);

	return TagInfo;
}

gboolean FillImageUpdate(gpointer GotData) {
	// Make the image into a buffer

	FillData *Data = (FillData *) GotData;

	char *Option = Data->TagInfo->Option;

	// Make new image widget for our image
	GtkWidget *Image = gtk_image_new();
	gtk_widget_set_size_request(Image,
		(Data->ImageWidth > 0) ? Data->ImageWidth : 100,
		(Data->ImageHeight > 0) ? Data->ImageHeight : 100);

	// Either get image file or image url
	if (Option[0] == '.') {
		ImageData *DataImage = ImageAlloc((GtkImage *) Image,
				Data->ImageWidth, Data->ImageHeight,
				false, false);
		if (DataImage == NULL) {
			pthread_mutex_unlock(&Data->ThreadLock);
			return FALSE;
		}

		// We need to set a path for the image data
		DataImage->Path = (char *) malloc(strlen(Data->Data->Path) +
				strlen(Option) + 1);
		if (DataImage->Path == NULL) {
			ImageFree(DataImage);
			pthread_mutex_unlock(&Data->ThreadLock);
			return FALSE;
		}
		sprintf(DataImage->Path, "%s%s", Data->Data->Path, Option + 2);

		// Make the actual call
		ImageFile(DataImage);
	} else {
		ImageUrl(Option, (GtkImage *) Image, Data->ImageWidth,
			Data->ImageHeight, false, false);
	}

	// Add to the text buffer we have
	GtkTextIter Start, End;
	GtkTextBuffer *TextBuffer = gtk_text_view_get_buffer(
		(GtkTextView *) Data->Item);

	// Get iters around the empty space we added earlier
	gtk_text_buffer_get_iter_at_offset(TextBuffer, &Start,
		Data->TagInfo->Place);
	gtk_text_buffer_get_iter_at_offset(TextBuffer, &End,
		Data->TagInfo->Place + 1);

	// Delete the empty space added earlier
	gtk_text_buffer_delete(TextBuffer, &Start, &End);

	// Get anchor and add image to anchor
	GtkTextChildAnchor *Anchor = gtk_text_buffer_create_child_anchor(
		TextBuffer, &Start);
	gtk_text_view_add_child_at_anchor((GtkTextView *) Data->Item, Image,
		Anchor);

	gtk_widget_show_all(Data->Item);

	// Set the item to null so text view won't be unreferenced
	// by destroy signal later on
	g_object_unref((GObject *) Data->Item);
	Data->Item = NULL;

	pthread_mutex_unlock(&Data->ThreadLock);

	return FALSE;
}

static void *FillImageThread(void *GotData) {
	// Thread for image setting

	FillData *Data = (FillData *) GotData;

	int Resolution[] = { -1, -1 };
	char *Path;
	char *Option = Data->TagInfo->Option;

	// Threaded work requires a lock
	pthread_mutex_init(&Data->ThreadLock, NULL);
	pthread_mutex_lock(&Data->ThreadLock);
	Data->IsThreaded = true;

	// Check for what type of link we got
	if (Option[0] == '.') {
		// Allocate memory for our variable in this case
		Path = malloc(strlen(Option) + strlen(Data->Data->Path) - 1);
		if (Path == NULL) {
			pthread_mutex_unlock(&Data->ThreadLock);
			return NULL;
		}

		sprintf(Path, "%s%s", Data->Data->Path, Option + 2);
	} else {
		// Image download allocates memory for us
		// or returns NULL if no image was downloaded
		Path = ImageDownload(Option, false);
	}

	bool IfComment = Data->Data->IfComment;

	// Used values in the checking
	int ImageType;
	double MaxImageWidth, MaxImageHeight, ImageScale;
	ImageType = json_integer_value(json_object_get(GlobalPreferences,
			(IfComment) ? "CommentImageType" : "ImageType"));
	ImageScale = json_real_value(json_object_get(GlobalPreferences,
			(IfComment) ? "CommentImageScale" : "ImageScale"));
	MaxImageWidth = json_real_value(json_object_get(GlobalPreferences,
			(IfComment) ? "CommentImageWidth" : "ImageWidth"));
	MaxImageHeight = json_real_value(json_object_get(GlobalPreferences,
			(IfComment) ? "CommentImageHeight" : "ImageHeight"));

	// Get image resolution
	if (Path != NULL) {
		ImageResolution(Path, Resolution);
		free(Path);
	}

	// Get the width and height for our image
	if (Resolution[0] == -1) {
		Data->ImageWidth = 100;
		Data->ImageHeight = 100;
	} else if (ImageType == 0) {
		Data->ImageWidth = Resolution[0];
		Data->ImageHeight = Resolution[1];
	} else if (ImageType == 2) {
		Data->ImageWidth = Resolution[0] * ImageScale;
		Data->ImageHeight = Resolution[1] * ImageScale;
	} else {
		unsigned int ImageWidth = Resolution[0];
		unsigned int ImageHeight = Resolution[1];

		double WidthRatio = ImageWidth / MaxImageWidth;
		double HeightRatio = ImageHeight / MaxImageHeight;

		double Ratio = ceil(
			(WidthRatio < HeightRatio) ? HeightRatio : WidthRatio);

		Data->ImageWidth = ImageWidth / Ratio;
		Data->ImageHeight = ImageHeight / Ratio;
	}

	// Now to the GTK stuff and to the actual buffer
	g_idle_add(FillImageUpdate, Data);

	return NULL;
}

static int FillInsertA(GtkTextBuffer *TextBuffer, GtkTextTagTable *TagTable,
	GtkTextIter Start, GtkTextIter End, GtkTextTag **TextTags,
	FillData **GotData, char *Option, int LinkTagIndex, int Rotate) {
	// Create a link tag into the buffer

	FillData *Data = *GotData;

	// Add an identifying number to link end
	sprintf(Option + strlen(Option), " %d", LinkTagIndex);

	// Check for identifying link and add it if needed
	GtkTextTag *TextTag = gtk_text_tag_table_lookup(TagTable, Option);
	if (TextTag == NULL) {
		TextTag = gtk_text_buffer_create_tag(TextBuffer, Option, NULL);

		// First link tag in buffer has double underline
		if (LinkTagIndex == 0) {
			g_object_set((GObject *) TextTag, "underline",
				PANGO_UNDERLINE_DOUBLE, NULL);
		}
	}

	// Apply the link tag
	gtk_text_buffer_apply_tag(TextBuffer, TextTag,
		&Start, &End);
	gtk_text_buffer_apply_tag_by_name(TextBuffer, "Link",
		&Start, &End);

	// Reallocate data
	int RC = FillNewData(&Data, Rotate);
	if (RC != -1) {
		Data[Rotate].LinkTagIndex = LinkTagIndex;
		Data[0].LinkTags[LinkTagIndex] = Option;
		TextTags[LinkTagIndex] = TextTag;
		*GotData = Data;
	}

	return RC;
}

static int FillInsertQuote(GtkTextView *TextView, GtkTextBuffer *TextBuffer,
	GtkTextTagTable *TagTable, GtkTextIter Start, GtkTextIter End,
	FillData **GotData, int Rotate) {
	// Make text quoted text

	FillData *Data = *GotData;

	// Get new separator
	GtkWidget *Separator = gtk_separator_new(GTK_ORIENTATION_VERTICAL);

	// Set horizontal alignment for separator
	gtk_widget_set_halign(Separator, GTK_ALIGN_START);

	gtk_widget_set_no_show_all(Separator, TRUE);
	gtk_widget_hide(Separator);

	gtk_overlay_add_overlay((GtkOverlay *) gtk_widget_get_parent(
			gtk_widget_get_parent((GtkWidget *) TextView)), Separator);

	int Level = 11, Margin = 0;
	char Quote[20];
	strcpy(Quote, "Quote");

	// Find if quote tag already exists
	while (Level > 1 && Margin == 0) {
		Level--;

		// Start from 10th down
		sprintf(Quote + 5, "%d", Level);
		GtkTextTag *TextTag = gtk_text_tag_table_lookup(TagTable, Quote);
		if (TextTag != NULL && gtk_text_iter_has_tag(&Start, TextTag)) {
			// If level is 10, we take -1
			Margin = (Level - (Level / 10)) * 30;
		}
	}

	// If we got a margin, we'll look into adding a margin
	if (Margin != 0) {
		// If level is 10 at this point, the Quote is 10th
		// quote and all the information is already okay
		if (Level != 10) {
			sprintf(Quote + 5, "%d", Level + 1);
		}
		gtk_widget_set_margin_start(Separator, Margin);
	}

	// Apply the new tag name
	gtk_text_buffer_apply_tag_by_name(TextBuffer, Quote, &Start, &End);

	// Reallocate data
	int RC = FillNewData(&Data, Rotate);
	if (RC != -1) {
		g_object_ref((GObject *) Separator);
		Data[Rotate].Item = Separator;
		*GotData = Data;
	}

	return RC;
}

static int FillInsertImg(GtkTextView *TextView, GtkTextBuffer *TextBuffer,
	GtkTextIter Start, FillData **GotData, int Rotate) {
	// Ready a spot for image in buffer

	FillData *Data = *GotData;

	// Insert empty space as placeholder for image
	gtk_text_buffer_insert(TextBuffer, &Start, " ", 1);

	// Reallocate data
	int RC = FillNewData(&Data, Rotate);
	if (RC != -1) {
		Data[Rotate].Item = (GtkWidget *) TextView;
		*GotData = Data;
	}

	return RC;
}

static int FillInsertHR(GtkTextView *TextView, FillData **GotData, int Rotate) {
	// Make a line in buffer

	FillData *Data = *GotData;

	// Get new separator
	GtkWidget *Separator = gtk_separator_new(GTK_ORIENTATION_HORIZONTAL);

	// Set horizontal alignment for separator
	gtk_widget_set_valign(Separator, GTK_ALIGN_START);

	gtk_overlay_add_overlay(
		(GtkOverlay *) gtk_widget_get_parent(
			gtk_widget_get_parent((GtkWidget *) TextView)),
		Separator);

	// Reallocate data
	int RC = FillNewData(&Data, Rotate);
	if (RC != -1) {
		g_object_ref((GObject *) Separator);
		Data[Rotate].Item = Separator;
		*GotData = Data;
	}

	return RC;
}

static void FillTextBuffer(GtkTextView *TextView, FillData **GotData,
	char *Text) {
	// Fill the text buffer and add references to pieces of text

	FillData *Data = *GotData;

	// Get text buffer
	GtkTextBuffer *TextBuffer = gtk_text_view_get_buffer(TextView);

	// Get tag table
	GtkTextTagTable *TagTable = gtk_text_buffer_get_tag_table(TextBuffer);

	// Initial text into the buffer
	gtk_text_buffer_set_text(TextBuffer, "", 0);

	// The start/end iters to reference a place in buffer
	GtkTextIter Start, End;

	// Insert text as markup
	gtk_text_buffer_get_start_iter(TextBuffer, &Start);
	gtk_text_buffer_insert_markup(TextBuffer, &Start, Text, -1);

	// Empty buffer and data set to null means this is the end
	if (Text[0] == '\0' && Data == NULL) {
		return;
	}

	// Now onto some variables we need in tag checking

	int TagAmount = Data->TagAmount;
	FillTag *TagInfo = Data->TagInfos;

	char *Name, *Option;
	int Pair, Place, RC, LinkTagIndex = 0, Rotate = 0, Offset = 0;

	// Temporary array of link tags to connect signals
	GtkTextTag *TextTags[TagAmount];

	// Allocate memory for the pointers to links in taginfos
	// This will freed before destroy signal in python version
	Data->LinkTags = malloc(TagAmount * sizeof(char *));
	if (Data->LinkTags == NULL) {
		FillFreeData(Data);
		*GotData = NULL;
		return;
	}

	// Now check for tags that we are really curious about
	for (int TagIndex = 0; TagIndex < TagAmount; TagIndex++) {
		// Images add one character to buffer, so it'll offset slightly
		TagInfo[TagIndex].Place += Offset;

		Name = TagInfo[TagIndex].Name;
		Option = TagInfo[TagIndex].Option;
		Place = TagInfo[TagIndex].Place;
		Pair = -1, RC = 0;

		// Save some execution time by sorting out
		// those that are end tags
		if (TagInfo[TagIndex].EndTag) {
			continue;
		}

		// Populate Pair with actual index in text
		if (TagInfo[TagIndex].PairInfo != NULL) {
			Pair = TagInfo[TagIndex].PairInfo->Place + Offset;
		}

		// Get start and end iters
		gtk_text_buffer_get_iter_at_offset(TextBuffer, &Start, Place);
		gtk_text_buffer_get_iter_at_offset(TextBuffer, &End, Pair);

		if (strcmp(Name, "a") == 0 && Pair != -1 && Option != NULL) {
			// Link tag - make a clickable link
			// If the next tag is img tag, we need to add one to
			// end iter as it then covers the img tag as well
			if (strcmp(TagInfo[TagIndex + 1].Name, "img") == 0 &&
				TagInfo[TagIndex + 1].Place + Offset - Place) {
				gtk_text_buffer_get_iter_at_offset(TextBuffer,
					&End, Pair + 1);
			}

			// In the case of no inside tag text, add option as text
			if (Place == Pair) {
				gtk_text_buffer_insert(TextBuffer, &End,
					Option, -1);

				// End and Start iters are offset by the length of
				// Option, therefore we need to get the same place
				// for our Start iter again
				gtk_text_buffer_get_iter_at_offset(TextBuffer,
					&Start, Place);
				Offset += strlen(Option);
			}

			RC = FillInsertA(TextBuffer, TagTable, Start, End,
					TextTags, &Data, Option, LinkTagIndex,
					Rotate);
			Data[Rotate].Item = NULL;
			Data[Rotate].IfResolve = 0;
			LinkTagIndex++;
		} else if (strcmp(Name, "blockquote") == 0 && Pair != -1) {
			// Blockquote - quoted text
			RC = FillInsertQuote(TextView, TextBuffer, TagTable,
					Start, End, &Data, Rotate);
		} else if (strcmp(Name, "img") == 0 && Option != NULL) {
			// Image - Image in text buffer
			RC = FillInsertImg(TextView, TextBuffer, Start,
					&Data, Rotate);
			Offset++;
		} else if (strcmp(Name, "hr") == 0 && Pair == -1) {
			// HR - Draws a line in the buffer
			RC = FillInsertHR(TextView, &Data, Rotate);
		}

		// Failure to reallocate
		if (RC == -1) {
			return;
		}

		// Pointer to tag in tag array
		if (RC) {
			Data[Rotate].TagInfo = &TagInfo[TagIndex];
			Rotate++;
		}
	}

	// Yet add tag index max to data struct
	Data[0].LinkTagIndexMax = LinkTagIndex;
	Data[0].RotateMax = Rotate;

	// Variables used in aid for connecting signals
	char *Signal;
	GObject *Object;
	GSourceFunc Function;
	FillTag *TagPointer;

	// Count link tags again
	LinkTagIndex = 0;

	for (int Index = 0; Index < Rotate; Index++) {
		// Point to first element in array to access some common variables
		Data[Index].Data = &Data[0];

		// Retrieve the pointer
		TagPointer = Data[Index].TagInfo;

		if (TagPointer->Option != NULL && TagPointer->PairInfo != NULL) {
			// Link tags or "a" tags
			Object = (GObject *) TextTags[LinkTagIndex++];
			Function = G_SOURCE_FUNC(FillLinkClick);
			Signal = "signal::event";
		} else if (TagPointer->PairInfo != NULL) {
			// Quote tags aka blockquote
			Object = (GObject *) TextView;
			Function = G_SOURCE_FUNC(FillQuoteDraw);
			Signal = "signal::draw";
		} else if (TagPointer->Option != NULL) {
			// Image idles
			// Creates a thread for image and detaches it

			// Increase reference count for used object
			g_object_ref((GObject *) Data[Index].Item);

			pthread_t Thread;
			pthread_create(&Thread, NULL, FillImageThread,
				&Data[Index]);
			pthread_detach(Thread);
			continue;
		} else {
			// HR tags
			Object = (GObject *) TextView;
			Function = G_SOURCE_FUNC(FillHRDraw);
			Signal = "signal::draw";
		}

		// Add GObject to data so we can disconnect all signals
		Data[Index].Object = Object;

		// Finally connect the signal to the object
		g_object_connect(Object, Signal, Function, &Data[Index], NULL);
	}

	// Reference back to first pointer
	*GotData = Data;
}

void FillMarkdown(GtkTextView *TextView, char *Text, FillData **GotData) {
	// Fill the text buffer in textview container with given text

	// The text being empty we may just exit at this point
	if (Text[0] == '\0' || GotData == NULL) {
		FillData *EmptyData = NULL;
		FillTextBuffer(TextView, &EmptyData, Text);
		return;
	}
	FillData *Data = *GotData;

	// Default json file path for backup purposes
	char DefaultPath[strlen(PlacesJson) + 25];
	sprintf(DefaultPath, "%sDefaultSettings.json", PlacesJson);

	int TagAmount;
	char Out[strlen(Text) + 10];

	// Get the tags from text and text without tags
	FillTag *TagInfo = FillTagsGet(Text, Out, &TagAmount);
	if (TagInfo == NULL) {
		return;
	}

	Data->TagInfos = &TagInfo[0];
	Data->TagAmount = TagAmount;

	// Add some tags back to string in a clean form
	FillKept(TagInfo, TagAmount, Out);

	// Newline to the end of the string for some reason?
	strcat(Out, "\n");

	// Fill text buffer with all the elements we need
	FillTextBuffer(TextView, &Data, Out);

	// Result back to pointer
	*GotData = Data;
}
