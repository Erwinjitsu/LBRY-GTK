/******************************************************************************\
* LBRY-GTK                                                                     *
* Copyright (C) 2021-2023 MorsMortium and Other Contributors                   *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU General Public License as published by         *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                *
* GNU General Public License for more details.                                 *
*                                                                              *
* You should have received a copy of the GNU General Public License            *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
\******************************************************************************/

// This file is responsible for moving around in listviews and flowboxes

#define PY_SSIZE_T_CLEAN
#include <Python.h>
#include <stdbool.h>
#include <gtk/gtk.h>
#include <stdio.h>
#include <pygobject.h>

#include "Header/Move.h"

void MoveFlowBoxLeftRight(GtkFlowBox *FlowBox, int Step) {
	// This function is responsible for moving left or right in a flowbox

	// Get every children and selected children
	GList *SelectedChildren = gtk_flow_box_get_selected_children(FlowBox);
	GList *Children = gtk_container_get_children((GtkContainer *) FlowBox);

	// Get number of children
	int Length = g_list_length(Children);

	// If there are no children selected
	if (g_list_length(SelectedChildren) == 0) {
		// If there are children, select first
		if (Length != 0) {
			gtk_flow_box_select_child(FlowBox, g_list_nth_data(Children, 0));
		}

		// Free up memory
		g_list_free(SelectedChildren);
		g_list_free(Children);
		return;
	}

	// Get index of currently selected children in every children plus step
	gpointer CurrentChild = g_list_nth_data(SelectedChildren, 0);
	int Index = g_list_index(Children, CurrentChild) + Step;

	// If index is within range of every children, select it
	if (-1 < Index && Index < Length) {
		gtk_flow_box_select_child(FlowBox, g_list_nth_data(Children, Index));
	}

	// Free up memory
	g_list_free(SelectedChildren);
	g_list_free(Children);
}

void MoveAllocationTest(gpointer Widget, gpointer Columns) {
	// This function is responsible for checking if widget is in top line in a
	// flowbox and increasing Columns, if so

	// Get location of widget
	GtkAllocation Allocation;
	gtk_widget_get_allocation((GtkWidget *) Widget, &Allocation);

	// If y coordinate is 0, the widget is on top, part of the first row
	if (Allocation.y == 0) {
		((int *) Columns)[0]++;
	}
}

void MoveFlowBoxUpDown(GtkFlowBox *FlowBox, int Direction) {
	// This function is responsible for moving up or down in a flowbox

	// Focus on FlowBox
	gtk_widget_grab_focus((GtkWidget *) FlowBox);

	// Get every children
	GList *Children = gtk_container_get_children((GtkContainer *) FlowBox);

	// Get number of children in a row
	int Columns = 0;
	g_list_foreach(Children, (GFunc) MoveAllocationTest, &Columns);
	g_list_free(Children);

	// Move selection sideways with full row
	MoveFlowBoxLeftRight(FlowBox, Direction * Columns);
}

void MoveListViewUpDown(GtkTreeView *TreeView, int Direction) {
	// This function is responsible for moving up or down in a listview

	// Get selected lines
	GtkTreeSelection *Selection = gtk_tree_view_get_selection(TreeView);

	// If there are no selected lines, select first and return
	if (gtk_tree_selection_count_selected_rows(Selection) == 0) {
		gtk_tree_selection_select_path(Selection, gtk_tree_path_new_first());
		return;
	}

	// Get first selected line
	GList *Rows = gtk_tree_selection_get_selected_rows(Selection, NULL);
	GtkTreePath *Path = g_list_nth_data(Rows, 0);

	// Move line forward or backward depending on direction
	if (Direction == 1) {
		gtk_tree_path_next(Path);
	} else {
		gtk_tree_path_prev(Path);
	}

	// Select moved line
	gtk_tree_selection_select_path(Selection, Path);
}

// Everything under this line is removable after full C conversion
static PyObject *MoveListViewUpDownPython(PyObject *Py_UNUSED(
		Self), PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Fill LongPointer, Direction with sent data
	uintptr_t LongPointer;
	int Direction;
	if (!PyArg_ParseTuple(Args, "Li", &LongPointer, &Direction)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	// Pass arguments to the pythonless function
	MoveListViewUpDown((GtkTreeView *) LongPointer, Direction);

	// Acquire lock
	PyEval_RestoreThread(Save);

	return PyLong_FromLong(0);
}

static PyObject *MoveFlowBoxLeftRightPython(PyObject *Py_UNUSED(
		Self), PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Fill LongPointer, Step with sent data
	uintptr_t LongPointer;
	int Step;
	if (!PyArg_ParseTuple(Args, "Li", &LongPointer, &Step)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	// Pass arguments to the pythonless function
	MoveFlowBoxLeftRight((GtkFlowBox *) LongPointer, Step);

	// Acquire lock
	PyEval_RestoreThread(Save);

	return PyLong_FromLong(0);
}

static PyObject *MoveFlowBoxUpDownPython(PyObject *Py_UNUSED(
		Self), PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Fill LongPointer, Direction with sent data
	uintptr_t LongPointer;
	int Direction;
	if (!PyArg_ParseTuple(Args, "Li", &LongPointer, &Direction)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	// Pass arguments to the pythonless function
	MoveFlowBoxUpDown((GtkFlowBox *) LongPointer, Direction);

	// Acquire lock
	PyEval_RestoreThread(Save);

	return PyLong_FromLong(0);
}

// Function names and other python data
static PyMethodDef MoveMethods[] = {
	{"ListViewUpDown", MoveListViewUpDownPython, METH_VARARGS,
	 "Vertical listview"},
	{"FlowBoxLeftRight", MoveFlowBoxLeftRightPython, METH_VARARGS,
	 "Horizontal flowbox"},
	{"FlowBoxUpDown", MoveFlowBoxUpDownPython, METH_VARARGS,
	 "Vertical flowbox"},
	{NULL, NULL, 0, NULL}
};

static struct PyModuleDef MoveModule = {
	PyModuleDef_HEAD_INIT, "Move", "Move module", -1, MoveMethods, 0, 0, 0, 0
};

// The function name has to end with the module name
PyMODINIT_FUNC PyInit_Move(void) {
	return PyModule_Create(&MoveModule);
}
