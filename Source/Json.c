/******************************************************************************\
* LBRY-GTK                                                                     *
* Copyright (C) 2021-2023 MorsMortium and Other Contributors                   *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU General Public License as published by         *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                *
* GNU General Public License for more details.                                 *
*                                                                              *
* You should have received a copy of the GNU General Public License            *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
\******************************************************************************/

// This file is responsible for extra json operations

#include <Python.h>
#include <jansson.h>
#include <stdbool.h>
#include <stdio.h>

#include "Header/Json.h"

json_t *JsonObject(json_t *Json, char **Keys) {
	// This function is responsible for fetching an object from a json, by keys

	// Final object to return
	json_t *Value = Json;

	int Index = 0;
	bool Faulty = true;
	while (Keys[Index] != NULL) {
		// Go through every key

		Faulty = false;

		// If current json object is an actual object, otherwise return NULL
		if (Value != NULL && json_is_object(Value)) {
			// Get new object from current object and key
			json_t *NewValue = json_object_get(Value, Keys[Index]);

			// Set object as current
			Value = NewValue;
		} else {
			Faulty = true;
			break;
		}

		Index++;
	}

	// Return current object, or NULL, if not every key was processed
	if (Faulty) {
		return NULL;
	}
	return Value;
}

char *JsonString(json_t *Json, char *FallBack) {
	// This function is responsible for returning string value of json object
	// if it is a string, FallBack otherwise

	// If Json exists and is a string return value of it
	if (Json != NULL && json_is_string(Json)) {
		return (char *) json_string_value(Json);
	}
	return FallBack;
}

int JsonInt(json_t *Json, int FallBack) {
	// This function is responsible for returning int value of json object
	// if it is an int, FallBack otherwise

	if (Json != NULL) {
		// If Json exists and is an int return value of it
		if (json_is_integer(Json)) {
			return json_integer_value(Json);
		}

		// Also check if real, so that we can convert this to int
		if (json_is_real(Json)) {
			return (int) json_real_value(Json);
		}
	}

	return FallBack;
}

double JsonDouble(json_t *Json, double FallBack) {
	// This function is responsible for returning double value of json object
	// if it is a double, FallBack otherwise

	// If Json exists and is a double return value of it
	if (Json != NULL && json_is_real(Json)) {
		return json_real_value(Json);
	}
	return FallBack;
}

bool JsonBool(json_t *Json, bool FallBack) {
	// This function is responsible for returning bool value of json object
	// if it is a bool, FallBack otherwise

	// If Json exists and is a bool return value of it
	if (Json != NULL && json_is_boolean(Json)) {
		return json_boolean_value(Json);
	}
	return FallBack;
}

char *JsonStringObject(json_t *Json, char **Keys, char *FallBack) {
	// This function is responsible for getting json object from Json and Keys
	// then getting string value of it
	return JsonString(JsonObject(Json, Keys), FallBack);
}

int JsonIntObject(json_t *Json, char **Keys, int FallBack) {
	// This function is responsible for getting json object from Json and Keys
	// then getting int value of it
	return JsonInt(JsonObject(Json, Keys), FallBack);
}

double JsonDoubleObject(json_t *Json, char **Keys, double FallBack) {
	// This function is responsible for getting json object from Json and Keys
	// then getting double value of it
	return JsonDouble(JsonObject(Json, Keys), FallBack);
}

bool JsonBoolObject(json_t *Json, char **Keys, bool FallBack) {
	// This function is responsible for getting json object from Json and Keys
	// then getting bool value of it
	return JsonBool(JsonObject(Json, Keys), FallBack);
}

bool JsonInArray(json_t *Item, json_t *Array) {
	// Checks if json item is in given array

	json_t *Value;
	size_t Index;
	json_array_foreach(Array, Index, Value) {
		if (json_equal(Item, Value)) {
			return true;
		}
	}

	return false;
}

void JsonClear(json_t *Json) {
	// This function is responsible for cleaning unused json values
	void *TMP;
	const char *Key;
	json_t *Value;
	json_object_foreach_safe(Json, TMP, Key, Value) {
		// Check for array type and if empty
		if (json_typeof(Value) == JSON_ARRAY && json_array_size(Value) == 0) {
			json_object_del(Json, Key);
			continue;
		}

		// Check for string type and if empty
		if (json_typeof(Value) == JSON_STRING) {
			// Or if timestamp...
			if (strcmp(Key, "timestamp") == 0) {
				if (strcmp(json_string_value(Value), "-1") == 0) {
					json_object_del(Json, Key);
					continue;
				}
			} else if (json_string_value(Value)[0] == '\0') {
				json_object_del(Json, Key);
				continue;
			}
		}

		// Check for bool type and if false
		if (json_is_boolean(Value) && !json_boolean_value(Value)) {
			json_object_del(Json, Key);
		}
	}
}

// Everything under this line is removable after full C conversion
static PyObject *JsonArrayToArray(json_t *Array) {
	// Read json array to python array

	size_t Index;
	json_t *Item;
	PyObject *PythonArray = PyList_New(0);

	json_array_foreach(Array, Index, Item) {
		PyList_Append(PythonArray, JsonParseToPython(Item));
	}

	return PythonArray;
}

static PyObject *JsonObjectToDict(json_t *Object) {
	// Read json object to python dict

	const char *Key;
	json_t *Item;
	PyObject *PythonObject = PyDict_New();

	json_object_foreach(Object, Key, Item) {
		PyDict_SetItemString(PythonObject, Key, JsonParseToPython(Item));
	}

	return PythonObject;
}

PyObject *JsonParseToPython(json_t *Item) {
	// Read json item to python equivalent

	PyObject *PythonItem = NULL;

	if (json_is_object(Item)) {
		PythonItem = JsonObjectToDict(Item);
	}
	if (json_is_array(Item)) {
		PythonItem = JsonArrayToArray(Item);
	}
	if (json_is_string(Item)) {
		char *String = (char *) json_string_value(Item);
		PythonItem = PyUnicode_FromString(String);
	}
	if (json_is_real(Item)) {
		double Value = json_real_value(Item);
		PythonItem = PyFloat_FromDouble(Value);
	}
	if (json_is_integer(Item)) {
		int Value = json_integer_value(Item);
		PythonItem = PyLong_FromLong(Value);
	}
	if (json_is_boolean(Item)) {
		int Value = json_integer_value(Item);
		PythonItem = PyBool_FromLong(Value);
	}

	return PythonItem;
}

PyObject *JsonToString(json_t *Json) {
	// This function is responsible for converting c json object to python
	// string

	// Convert json to pyhon string object
	char *JsonString = json_dumps(Json, 0);
	json_decref(Json);
	PyObject *PythonJsonString = PyUnicode_FromString(JsonString);
	free(JsonString);
	return PythonJsonString;
}
