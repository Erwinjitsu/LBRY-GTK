/******************************************************************************\
* LBRY-GTK                                                                     *
* Copyright (C) 2021-2023 MorsMortium and Other Contributors                   *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU General Public License as published by         *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                *
* GNU General Public License for more details.                                 *
*                                                                              *
* You should have received a copy of the GNU General Public License            *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
\******************************************************************************/

// This file is responsible for controlling the Order widget

#define PY_SSIZE_T_CLEAN
#include <Python.h>
#include <stdbool.h>
#include <gtk/gtk.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>

#include "Header/Global.h"
#include "Header/Places.h"
#include "Header/Order.h"

// Types of orderings
char *OrderTypes[] = { "creation_height", "name", "trending_mixed" };

void OrderGet(OrderData *Data, char **Returned) {
	// This function is responsible for getting currently set value in a string
	// format

	// If ordering is disabled return
	if (!gtk_toggle_button_get_active((GtkToggleButton *) Data->UseOrdering)) {
		*Returned = NULL;
		return;
	}

	*Returned = malloc(OrderMaxGet);

	// Get ordering, and if it is descending
	char *Order =
		OrderTypes[gtk_combo_box_get_active((GtkComboBox *) Data->OrderBy)];
	if (gtk_combo_box_get_active((GtkComboBox *) Data->Direction) == 1) {
		// Add '^' to ordering
		sprintf(*Returned, "^%s", Order);
		return;
	}
	sprintf(*Returned, "%s", Order);
}

void OrderSet(OrderData *Data, char *Ordering) {
	// This function is responsible for setting value from a string

	// If "" is passed, disable ordering
	gtk_combo_box_set_active((GtkComboBox *) Data->Direction, 0);
	if (strlen(Ordering) == 0) {
		gtk_toggle_button_set_active((GtkToggleButton *) Data->UseOrdering,
			false);
		gtk_combo_box_set_active((GtkComboBox *) Data->OrderBy, 0);
		return;
	}

	// Enable ordering
	gtk_toggle_button_set_active((GtkToggleButton *) Data->UseOrdering, true);

	// If '^' means ascending ordering, set and remove it
	if (Ordering[0] == '^') {
		gtk_combo_box_set_active((GtkComboBox *) Data->Direction, 1);
		Ordering++;
	}

	// Find Ordering string from OrderTypes, set its Index
	for (int Index = 0; Index < 3; ++Index) {
		if (strcmp(Ordering, OrderTypes[Index]) == 0) {
			gtk_combo_box_set_active((GtkComboBox *) Data->OrderBy, Index);
			break;
		}
	}
}

void OrderPrint(OrderData *Data, char *Returned) {
	// This function is responsible for getting currently set value in a human
	// readable format, for titles

	// If ordering is disabled, return ""
	if (!gtk_toggle_button_get_active((GtkToggleButton *) Data->UseOrdering)) {
		return;
	}

	// Get strings from choosen options and make them lower case
	char *OrderString = gtk_combo_box_text_get_active_text((GtkComboBoxText *)
			Data->OrderBy);
	char *DirectionString = gtk_combo_box_text_get_active_text((GtkComboBoxText
			*)
			Data->Direction);
	OrderString[0] = tolower(OrderString[0]);
	DirectionString[0] = tolower(DirectionString[0]);

	// Add together the final string, and return it
	char String[15 + strlen(OrderString) + strlen(DirectionString)];
	sprintf(String, "Order by: %s, %s, ", OrderString, DirectionString);
	sprintf(Returned, "%s", String);
}

OrderData *OrderCreate(void) {
	// This function is responsible for creating the Order widget and putting it
	// into Box

	// Create a builder object and load the XML from file
	char WidgetFile[strlen(PlacesGlade) + 12];
	sprintf(WidgetFile, "%sOrder.glade", PlacesGlade);
	gtk_builder_add_from_file(GlobalBuilder, WidgetFile, NULL);
	gtk_builder_connect_signals(GlobalBuilder, NULL);

	// Allocate memory for data, abort if NULL
	OrderData *Data = malloc(sizeof(OrderData));
	if (Data == NULL) {
		abort();
	}

	// Set data fields
	Data->Order = (GtkWidget *) gtk_builder_get_object(GlobalBuilder, "Order");
	Data->UseOrdering =
		(GtkWidget *) gtk_builder_get_object(GlobalBuilder, "UseOrdering");
	Data->UseOrderingActive =
		(GtkWidget *) gtk_builder_get_object(GlobalBuilder,
			"UseOrderingActive");
	Data->OrderBy = (GtkWidget *) gtk_builder_get_object(GlobalBuilder,
			"OrderBy");
	Data->OrderByActive =
		(GtkWidget *) gtk_builder_get_object(GlobalBuilder, "OrderByActive");
	Data->Direction =
		(GtkWidget *) gtk_builder_get_object(GlobalBuilder, "Direction");
	Data->DirectionActive =
		(GtkWidget *) gtk_builder_get_object(GlobalBuilder, "DirectionActive");

	return Data;
}

// Everything under this line is removable after full C conversion
OrderData *OrderGetObject(PyObject *Args) {
	// This function is responsible for getting a OrderData python
	// object and returning the c version

	// Parse single argument of Object pointer, return Object
	uintptr_t LongPointer;
	if (!PyArg_ParseTuple(Args, "L", &LongPointer)) {
		return NULL;
	}
	return (OrderData *) LongPointer;
}

static PyObject *OrderGetPython(PyObject *Py_UNUSED(Self), PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	// Get Object, pass it to pythonless function, with string pointer,
	// and return what it returned
	char *Returned;
	OrderGet(OrderGetObject(Args), &Returned);

	// Acquire lock
	PyEval_RestoreThread(Save);

	PyObject *Return = PyUnicode_FromString((Returned) ? Returned : "");
	if (Returned) {
		free(Returned);
	}

	return Return;
}

static PyObject *OrderSetPython(PyObject *Py_UNUSED(Self), PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Fill LongPointer, Ordering
	char *Ordering;
	uintptr_t LongPointer;
	if (!PyArg_ParseTuple(Args, "Ls", &LongPointer, &Ordering)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	// Get Object, pass it to pythonless function, with Ordering
	OrderSet((OrderData *) LongPointer, Ordering);

	// Acquire lock
	PyEval_RestoreThread(Save);

	return PyLong_FromLong(0);
}

static PyObject *OrderPrintPython(PyObject *Py_UNUSED(Self), PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	// Get Object, pass it to pythonless function, with string pointer,
	// and return what it returned
	char Returned[OrderMaxPrint];
	memset(Returned, 0, sizeof Returned);
	OrderPrint(OrderGetObject(Args), Returned);

	// Acquire lock
	PyEval_RestoreThread(Save);

	return PyUnicode_FromString(Returned);
}

static PyObject *OrderCreatePython(PyObject *Py_UNUSED(
		Self), PyObject *Py_UNUSED(Args)) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	// Pass arguments to pythonless function, and return what it returned
	OrderData *Data = OrderCreate();

	// Acquire lock
	PyEval_RestoreThread(Save);

	PyObject *Dict = PyDict_New();

	PyObject *Object = PyLong_FromVoidPtr((void *) Data->Order);
	PyDict_SetItemString(Dict, "Order", Object);
	Py_DECREF(Object);

	Object = PyLong_FromVoidPtr((void *) Data->UseOrdering);
	PyDict_SetItemString(Dict, "UseOrdering", Object);
	Py_DECREF(Object);

	Object = PyLong_FromVoidPtr((void *) Data->UseOrderingActive);
	PyDict_SetItemString(Dict, "UseOrderingActive", Object);
	Py_DECREF(Object);

	Object = PyLong_FromVoidPtr((void *) Data->OrderBy);
	PyDict_SetItemString(Dict, "OrderBy", Object);
	Py_DECREF(Object);

	Object = PyLong_FromVoidPtr((void *) Data->OrderByActive);
	PyDict_SetItemString(Dict, "OrderByActive", Object);
	Py_DECREF(Object);

	Object = PyLong_FromVoidPtr((void *) Data->Direction);
	PyDict_SetItemString(Dict, "Direction", Object);
	Py_DECREF(Object);

	Object = PyLong_FromVoidPtr((void *) Data->DirectionActive);
	PyDict_SetItemString(Dict, "DirectionActive", Object);
	Py_DECREF(Object);

	Object = PyLong_FromVoidPtr((void *) Data);
	PyDict_SetItemString(Dict, "Pointer", Object);
	Py_DECREF(Object);

	return Dict;
}

// Function names and other python data
static PyMethodDef OrderMethods[] = {
	{"Create", OrderCreatePython, METH_VARARGS, "Order init"},
	{"Set", OrderSetPython, METH_VARARGS, "Set Order"},
	{"Get", OrderGetPython, METH_VARARGS, "Get Order"},
	{"Print", OrderPrintPython, METH_VARARGS, "Print Order"},
	{NULL, NULL, 0, NULL}
};

static struct PyModuleDef OrderModule = {
	PyModuleDef_HEAD_INIT, "Order", "Order module", -1, OrderMethods, 0, 0, 0, 0
};

// The function name has to end with the module name
PyMODINIT_FUNC PyInit_Order(void) {
	return PyModule_Create(&OrderModule);
}
