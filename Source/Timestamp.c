/******************************************************************************\
* LBRY-GTK                                                                     *
* Copyright (C) 2021-2023 MorsMortium and Other Contributors                   *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU General Public License as published by         *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                *
* GNU General Public License for more details.                                 *
*                                                                              *
* You should have received a copy of the GNU General Public License            *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
\******************************************************************************/

// This file/module converts timestamps into dates or tells how long ago
// something happened.

// For strptime to NOT throw any errors
#define _XOPEN_SOURCE 700

#include <Python.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "Header/Timestamp.h"

// Table of periods from year to second, in seconds
static unsigned int TimestampPeriods[] = {
	60 * 60 * 24 * 365, 60 * 60 * 24 * 30, 60 * 60 * 24 * 7, 60 * 60 * 24,
	60 * 60, 60, 1
};

// Table of names of periods
static char *TimestampNames[] = {
	"year", "month", "week", "day", "hour", "minute", "second"
};

// Conditions for leap year
static const int TimestampLeaps[] = { 400, 100, 4, 0 };

char *TimestampPassed(unsigned int Timestamp) {
	// This function is used to return how long ago something happened
	// Remember to free() once done with variable

	// If for some reason timestamp larger or exactly the same
	// return before further execution
	unsigned int CurrentTime = time(NULL);
	char *Buffer;
	if (CurrentTime <= Timestamp + 1) {
		// We'll still allocate memory for free to not fail
		Buffer = malloc(10);
		if (Buffer != NULL) {
			sprintf(Buffer, "Just now");
		}

		return Buffer;
	}

	// Now it's safe to calculate time that has passed
	unsigned int PastTime = CurrentTime - Timestamp;

	// Check each number until one smaller found
	// 7 is the number of items in Periods
	int TimesBigger = 0, Count;
	for (Count = 0; Count < 7; Count++) {
		// In case smaller, get how many times and break execution
		if (PastTime > TimestampPeriods[Count]) {
			TimesBigger = (int) PastTime / TimestampPeriods[Count];
			break;
		}
	}

	// Check if plural
	char *Plural = " ";
	if (TimesBigger > 1) {
		Plural = "s ";
	}

	// Make the returnable string
	Buffer = malloc(30);
	if (Buffer != NULL) {
		sprintf(Buffer, "%d %s%sago", TimesBigger, TimestampNames[Count],
			Plural);
	}

	return Buffer;
}

char *TimestampGet(unsigned int Timestamp) {
	// This function returns a date usually used as a tooltip
	// Remember to free() once done with variable

	// Get the calendar time
	time_t CalendarTime = Timestamp;

	// Gets localtime from given calendar time
	struct tm *LocalTime = localtime(&CalendarTime);

	// Allocate memory to return date string
	char *Buffer = malloc(20);
	if (Buffer != NULL) {
		strftime(Buffer, 50, "%Y-%m-%d %H:%M:%S", LocalTime);
	}

	return Buffer;
}

int TimestampMonthDays(int Year, int Month) {
	// This function returns the days in a month

	// Check for february alone
	if (Month == 2 && Year != 0) {
		// Check if leap year
		int Leap = 0;
		while (TimestampLeaps[Leap] != 0) {
			if (Year % TimestampLeaps[Leap] == 0) {
				break;
			}
			Leap++;
		}

		// If Leap is 0 or 2 we know it's 28 days
		return ((Leap | 2) == 2) ? 28 : 29;
	}

	// Nearly every second month is 30 days
	// Less than 8: even; more or equal to 8: odd
	return ((Month % 2 == 0) ^ (Month < 8)) ? 31 : 30;
}

// Everything under this line is removable after full C conversion
static PyObject *TimestampCurrentPython(PyObject *Py_UNUSED(
		Self), PyObject *Py_UNUSED(Args)) {
	// This function is to only give the current time for Python version.

	unsigned int Time = time(NULL);

	PyObject *TimeNow = PyLong_FromUnsignedLong(Time);

	return TimeNow;
}

static PyObject *TimestampPassedPython(PyObject *Py_UNUSED(
		Self), PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Get arguments and pass it to the pythonless function
	unsigned int Timestamp = 0;

	// For notifications (from Odysee) we need to check for string
	// TODO: Remove string checking once notifications are local
	if (!PyArg_ParseTuple(Args, "I", &Timestamp)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	char *TimePassed = TimestampPassed(Timestamp);

	// Acquire lock
	PyEval_RestoreThread(Save);

	PyObject *ReturnString = PyUnicode_FromString(TimePassed);
	free(TimePassed);

	return ReturnString;
}

static PyObject *TimestampGetPython(PyObject *Py_UNUSED(Self), PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Get arguments and pass it to the pythonless function
	unsigned int Timestamp = 0;
	if (!PyArg_ParseTuple(Args, "I", &Timestamp)) {
		return NULL;
	}

	char *Date = TimestampGet(Timestamp);
	PyObject *ReturnString = PyUnicode_FromString(Date);
	free(Date);

	return ReturnString;
}

// Function names and other python data
static PyMethodDef TimestampMethods[] = {
	{"Ago", TimestampPassedPython, METH_VARARGS, "Time ago"},
	{"GetDate", TimestampGetPython, METH_VARARGS,
	 "Get date out of given timestamp"},
	{"CurrentTime", TimestampCurrentPython, METH_VARARGS, "Current time"},
	{NULL, NULL, 0, NULL}
};

static struct PyModuleDef TimestampModule = {
	PyModuleDef_HEAD_INIT, "Timestamp", "Timestamp module", -1,
	TimestampMethods, 0, 0, 0, 0
};

// The function name has to end with the module name
PyMODINIT_FUNC PyInit_Timestamp(void) {
	return PyModule_Create(&TimestampModule);
}
