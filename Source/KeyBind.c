/******************************************************************************\
* LBRY-GTK                                                                     *
* Copyright (C) 2021-2023 MorsMortium and Other Contributors                   *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU General Public License as published by         *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                *
* GNU General Public License for more details.                                 *
*                                                                              *
* You should have received a copy of the GNU General Public License            *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
\******************************************************************************/

// KeyBind is a widget file in aid of settings

#include <Python.h>
#include <gtk/gtk.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "Header/Global.h"
#include "Header/Places.h"
#include "Header/GTKExtra.h"
#include "Header/KeyBind.h"

KeyBindData *KeyBindCreate(void) {
	// This function is responsible for creating a KeyBind widget

	// Allocate memory for data, abort if NULL
	KeyBindData *Data = malloc(sizeof(KeyBindData));
	if (Data == NULL) {
		abort();
	}

	// Get glade file name, load it in builder
	char WidgetFile[strlen(PlacesGlade) + 20];
	sprintf(WidgetFile, "%sKeyBind.glade", PlacesGlade);
	gtk_builder_add_from_file(GlobalBuilder, WidgetFile, NULL);

	// Set data fields
	Data->KeyBind = (GtkWidget *) gtk_builder_get_object(GlobalBuilder,
			"KeyBind");
	Data->Modifier = (GtkWidget *) gtk_builder_get_object(GlobalBuilder,
			"Modifier");
	Data->Character =
		(GtkWidget *) gtk_builder_get_object(GlobalBuilder, "Character");
	Data->Use = (GtkWidget *) gtk_builder_get_object(GlobalBuilder, "Use");

	// Connect signals with data
	gtk_builder_connect_signals_full(GlobalBuilder, GTKExtraConnect, Data);

	return Data;
}

void KeyBindSet(char *Text, KeyBindData *Data) {
	// This function sets keybind to a widget

	// Here are some useful variables used in this function!
	bool UseActive;
	int ModifierActive;
	char *Symbol;

	// Make necessary changes: if empty make widget empty, otherwise not
	if (Text[0] != '\0') {
		UseActive = true;
		ModifierActive = atoi((Text));
		Symbol = (Text + 2);
	} else {
		UseActive = false;
		ModifierActive = 0;
		Symbol = "";
	}

	// Do changes to widgets
	gtk_toggle_button_set_active((GtkToggleButton *) Data->Use, UseActive);
	gtk_combo_box_set_active((GtkComboBox *) Data->Modifier, ModifierActive);
	gtk_entry_set_text((GtkEntry *) Data->Character, Symbol);
}

void KeyBindGet(char *Out, KeyBindData *Data) {
	// This function gets the active keybind in a widget

	// Get the used character
	const char *Symbol = gtk_entry_get_text((GtkEntry *) Data->Character);

	// Check if there's any need for getting the string
	if (gtk_toggle_button_get_active((GtkToggleButton *) Data->Use) != TRUE ||
		Symbol[0] == '\0') {
		Out[0] = '\0';
	} else {
		int ModifierActive =
			gtk_combo_box_get_active((GtkComboBox *) Data->Modifier);
		sprintf(Out, "%d %s", ModifierActive, Symbol);
	}
}

unsigned int *KeyBindConvert(char *Text) {
	// This function converts string to key symbol and mask

	// Check first we have non-empty string
	if (Text[0] == '\0') {
		return 0;
	}

	// Reserve variable to hold values
	static unsigned int Values[2];

	// Get the keyvalue of character
	Values[0] = gdk_unicode_to_keyval(*(Text + 2));

	// Get the mask from possible masks used by the client
	int Index = atoi((Text));
	Values[1] = KeybindMasks[Index];

	return Values;
}

// Everything under this line is removable after full C conversion
static PyObject *KeyBindCreatePython(PyObject *Py_UNUSED(
		Self), PyObject *Py_UNUSED(Args)) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	// Pass object to pythonless function
	KeyBindData *Data = KeyBindCreate();

	// Acquire lock
	PyEval_RestoreThread(Save);

	PyObject *Dict = PyDict_New();

	PyObject *Object = PyLong_FromVoidPtr((void *) Data->KeyBind);
	PyDict_SetItemString(Dict, "KeyBind", Object);
	Py_DECREF(Object);

	Object = PyLong_FromVoidPtr((void *) Data->Modifier);
	PyDict_SetItemString(Dict, "Modifier", Object);
	Py_DECREF(Object);

	Object = PyLong_FromVoidPtr((void *) Data->Character);
	PyDict_SetItemString(Dict, "Character", Object);
	Py_DECREF(Object);

	Object = PyLong_FromVoidPtr((void *) Data->Use);
	PyDict_SetItemString(Dict, "Use", Object);
	Py_DECREF(Object);

	Object = PyLong_FromVoidPtr((void *) Data);
	PyDict_SetItemString(Dict, "Pointer", Object);
	Py_DECREF(Object);

	return Dict;
}

static PyObject *KeyBindSetPython(PyObject *Py_UNUSED(Self), PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0
	char *Text;
	uintptr_t LongPointer;
	if (!PyArg_ParseTuple(Args, "sL", &Text, &LongPointer)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	// Pass object to pythonless function with time struct
	KeyBindSet(Text, (KeyBindData *) LongPointer);

	// Acquire lock
	PyEval_RestoreThread(Save);

	return PyLong_FromLong(0);
}

static PyObject *KeyBindGetPython(PyObject *Py_UNUSED(Self), PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0
	uintptr_t LongPointer;
	if (!PyArg_ParseTuple(Args, "L", &LongPointer)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	char Out[5];

	// Pass object to pythonless function with time struct
	KeyBindGet(Out, (KeyBindData *) LongPointer);

	// Acquire lock
	PyEval_RestoreThread(Save);

	return PyUnicode_FromString(Out);
}

static PyObject *KeyBindConvertPython(PyObject *Py_UNUSED(Self),
	PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0
	char *Text;
	if (!PyArg_ParseTuple(Args, "s", &Text)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	// Pass object to pythonless function with time struct
	unsigned int *Return = KeyBindConvert(Text);

	// Acquire lock
	PyEval_RestoreThread(Save);

	if (Return == 0) {
		return PyLong_FromLong(0);
	}

	PyObject *Dict = PyDict_New();

	PyObject *Object = PyLong_FromLongLong(*(Return));
	PyDict_SetItemString(Dict, "Value", Object);
	Py_DECREF(Object);

	Object = PyLong_FromLongLong(*(Return + 1));
	PyDict_SetItemString(Dict, "Modifier", Object);
	Py_DECREF(Object);

	return Dict;
}

// Function names and other python data
static PyMethodDef KeyBindMethods[] = {
	{"Create", KeyBindCreatePython, METH_VARARGS, "Keybind init"},
	{"Set", KeyBindSetPython, METH_VARARGS, "Set keybind"},
	{"Get", KeyBindGetPython, METH_VARARGS, "Get keybind"},
	{"Convert", KeyBindConvertPython, METH_VARARGS, "Convert keybind"},
	{NULL, NULL, 0, NULL}
};

static struct PyModuleDef KeyBindModule = {
	PyModuleDef_HEAD_INIT, "KeyBind", "KeyBind module", -1, KeyBindMethods, 0,
	0, 0, 0
};

// The function name has to end with the module name
PyMODINIT_FUNC PyInit_KeyBind(void) {
	return PyModule_Create(&KeyBindModule);
}
