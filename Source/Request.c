/******************************************************************************\
* LBRY-GTK                                                                     *
* Copyright (C) 2021-2023 MorsMortium and Other Contributors                   *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU General Public License as published by         *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                *
* GNU General Public License for more details.                                 *
*                                                                              *
* You should have received a copy of the GNU General Public License            *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
\******************************************************************************/

// This file is responsible for dealing with curl requests

#include <curl/curl.h>
#include <jansson.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include "Header/Error.h"
#include "Header/Global.h"
#include "Header/Request.h"

static size_t RequestWrite(void *Text, size_t UNUSED(One), size_t Size,
	void *FullText) {
	// This function is responsible for copying libcurl buffer text into the
	// full version. Helper function for libcurl

	// Cast sent void data into string
	char **FullChar = (char **) FullText;
	if (FullChar[0] == NULL) {
		return 0;
	}

	// Get full size
	int NewSize = strlen(FullChar[0]) + Size + 1;

	// Resize string to full size
	char *Temporary = realloc(FullChar[0], NewSize);

	// If realloc could not find a long enough memory, exit
	if (Temporary == NULL) {
		return 0;
	}

	FullChar[0] = Temporary;

	// Copy buffer text into full version
	strncat(FullChar[0], Text, Size);

	// Return Size signaling no error
	return Size;
}

json_t *RequestMake(char *Server, char *JsonString, char *Parameters) {
	// This function gets the json response from url

	CURL *Curl = curl_easy_init();

	json_error_t ErrorJson;
	if (Curl == NULL) {
		return json_loads("{\"error\": \"no result\"}", 0, &ErrorJson);
	}

	// Allocate memory for json
	char *Json[] = { malloc(1) };

	// If malloc could not find a long enough memory, exit
	if (Json[0] == NULL) {
		curl_easy_cleanup(Curl);
		return json_loads("{\"error\": \"no result\"}", 0, &ErrorJson);
	}

	// Empty Json
	Json[0][0] = '\0';

	// Set url, sent json, data writing function and user data for request
	struct curl_slist *Header = NULL;
	Header = curl_slist_append(Header, "Accept: application/json");
	if (JsonString != NULL) {
		Header = curl_slist_append(Header,
				"Content-Type: application/json; charset: utf-8");
		curl_easy_setopt(Curl, CURLOPT_POSTFIELDS, JsonString);
	} else if (Parameters != NULL) {
		Header = curl_slist_append(Header,
				"Content-Type: application/x-www-form-urlencoded; charset: utf-8");
		curl_easy_setopt(Curl, CURLOPT_POSTFIELDS, Parameters);
	}
	curl_easy_setopt(Curl, CURLOPT_HTTPHEADER, Header);
	curl_easy_setopt(Curl, CURLOPT_URL, Server);
	curl_easy_setopt(Curl, CURLOPT_WRITEFUNCTION, RequestWrite);
	curl_easy_setopt(Curl, CURLOPT_WRITEDATA, Json);

	// Perform request and clean up curl, header
	CURLcode Return = curl_easy_perform(Curl);
	curl_slist_free_all(Header);
	curl_easy_cleanup(Curl);

	// If request failed, clean up json string then return default error
	if (Return != CURLE_OK) {
		free(Json[0]);
		return json_loads("{\"error\": \"no result\"}", 0, &ErrorJson);
	}

	json_t *JsonObject = json_loads(Json[0], 0, &ErrorJson);
	free(Json[0]);

	// If json string was malformed, return default error
	if (JsonObject == NULL) {
		return json_loads("{\"error\": \"no result\"}", 0, &ErrorJson);
	}

	// Get the result, if unsuccessful, clean up json object then
	// return error
	json_t *Result = json_object_get(JsonObject, "result");
	json_t *Success = json_object_get(JsonObject, "success");
	if (Result == NULL && Success == NULL) {
		json_t *ErrorBlock = json_object_get(JsonObject, "error");
		if (ErrorBlock == NULL) {
			// Return default if object has no error key
			json_decref(JsonObject);
			return json_loads("{\"error\": \"no result\"}", 0, &ErrorJson);
		}

		// Check for error code
		int Code = json_integer_value(json_object_get(ErrorBlock, "code"));
		if (Code == 0) {
			Code = -1;
		}

		// Get the message
		const char *Message =
			json_string_value(json_object_get(ErrorBlock, "message"));
		char *ErrorMsg = ErrorGet(Code, Message);

		// Make error block
		json_t *FinalError = json_object();
		json_object_set_new(FinalError, "error", json_string(ErrorMsg));
		free(ErrorMsg);

		json_decref(JsonObject);
		return FinalError;
	} else if (Success != NULL) {
		// This is Odysee specific, consider doing this differently
		if (json_boolean_value(Success)) {
			Result = json_object_get(JsonObject, "data");
		} else {
			return JsonObject;
		}
	}

	// Make result object self sufficient
	json_incref(Result);

	// Clean up root json object and return result
	json_decref(JsonObject);
	return Result;
}

json_t *RequestJson(char *Server, char *JsonString) {
	// This function checks the string is Json and sends it

	// If NULL we can just send the string off
	if (JsonString != NULL) {
		// Load the string
		json_error_t Error;
		json_t *CheckValid = json_loads(JsonString, 0, &Error);
		if (CheckValid == NULL) {
			json_t *ErrorMessage = json_object();

			// Return an error message in failure
			json_object_set_new(ErrorMessage, "error", json_string(Error.text));
			return ErrorMessage;
		}
		json_decref(CheckValid);
	}

	// Make the request
	return RequestMake(Server, JsonString, NULL);
}

bool RequestFile(char *Url, char *Path) {
	// This function is responsible for downloading a file from Url to Path

	// Init Curl, report failure
	CURL *Curl = curl_easy_init();
	if (Curl == NULL) {
		return false;
	}

	// Open file, clean up curl and report failure, if can not
	FILE *File = fopen(Path, "wb");
	if (File == NULL) {
		curl_easy_cleanup(Curl);
		return false;
	}

	// Set url, file, default writer, timeout, redirection following for curl
	curl_easy_setopt(Curl, CURLOPT_URL, Url);
	curl_easy_setopt(Curl, CURLOPT_WRITEDATA, File);
	curl_easy_setopt(Curl, CURLOPT_WRITEFUNCTION, NULL);
	curl_easy_setopt(Curl, CURLOPT_TIMEOUT, 10);
	curl_easy_setopt(Curl, CURLOPT_FOLLOWLOCATION, true);

	// Make request, close file, clean up curl, report success or failure
	CURLcode Return = curl_easy_perform(Curl);
	fclose(File);

	if (Return == CURLE_OK) {
		// Get response
		long Response;
		curl_easy_getinfo(Curl, CURLINFO_RESPONSE_CODE, &Response);
		curl_easy_cleanup(Curl);
		return Response == 200;
	}

	curl_easy_cleanup(Curl);
	return false;
}
